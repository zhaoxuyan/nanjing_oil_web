#!/bin/bash

# xuyan.zhao
docker run -itd --name=nanjing_web --network=host --restart=always \
  -v /DATACENTER1/fei.li/nanjing_oil_web:/DATACENTER1/nanjing_oil_web \
  -v /DATACENTER1/fei.li/nanjing_oil_vue:/DATACENTER1/nanjing_oil_vue \
  -v /DATACENTER1/xuyan.zhao/oil_nanjing/datacenter:/DATACENTER1/datacenter \
  -v /DATACENTER1/xuyan.zhao/oil_nanjing/oil_shanxi:/DATACENTER1/oil_shanxi \
  -v /DATACENTER1/xuyan.zhao/oil_nanjing/test_video:/DATACENTER1/test_video \
  -v /etc/localtime:/etc/localtime \
  -v /etc/timezone:/etc/timezone \
  nanjing_oil_web /DATACENTER1/nanjing_oil_web/boot.sh

# song.hou
docker run -itd --name=nanjing_web --network=host --restart=always \
  -v /DATACENTER1/fei.li/nanjing_oil_web:/DATACENTER1/nanjing_oil_web \
  -v /DATACENTER1/fei.li/nanjing_oil_vue:/DATACENTER1/nanjing_oil_vue \
  -v /DATACENTER1/song.hou/nanjing_project/datacenter:/DATACENTER1/datacenter \
  -v /DATACENTER1/song.hou/nanjing_project/oil_shanxi:/DATACENTER1/oil_shanxi \
  -v /DATACENTER1/song.hou/nanjing_project/test_video:/DATACENTER1/test_video \
  -v /etc/localtime:/etc/localtime \
  -v /etc/timezone:/etc/timezone \
  nanjing_oil_web /DATACENTER1/nanjing_oil_web/boot.sh

# 开发
docker run -itd --name=nanjing_web_dev --network=host --restart=always \
  -v /DATACENTER1/fei.li/nanjing_oil_web_dev:/DATACENTER1/nanjing_oil_web \
  -v /etc/localtime:/etc/localtime \
  -v /etc/timezone:/etc/timezone \
  nanjing_oil_web

# 部署
docker run -itd --name=nanjing_web --network=host --restart=always \
  -v /DATACENTER1/oil_nanjing/nanjing_oil_web:/DATACENTER1/nanjing_oil_web \
  -v /DATACENTER1/oil_nanjing/nanjing_oil_vue:/DATACENTER1/nanjing_oil_vue \
  -v /DATACENTER1/oil_nanjing/datacenter:/DATACENTER1/datacenter \
  -v /DATACENTER1/oil_nanjing/oil_shanxi:/DATACENTER1/oil_shanxi \
  -v /DATACENTER1/oil_nanjing/test_video:/DATACENTER1/test_video \
  -v /etc/localtime:/etc/localtime \
  -v /etc/timezone:/etc/timezone \
  nanjing_oil_web /DATACENTER1/nanjing_oil_web/boot.sh
