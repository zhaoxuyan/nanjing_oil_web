CREATE DEFINER = `www` @`%` PROCEDURE `insert_event`(
    IN p_channel_id int,
    IN p_event_code int,
    IN p_image_path varchar(200),
    IN p_video_path varchar(200),
    OUT oid int
) BEGIN
insert log_event(channel_id, event_code, image_path, video_path)
values (
        p_channel_id,
        p_event_code,
        p_image_path,
        p_video_path
    );
set @nid = last_insert_id(),
    @ndt = current_timestamp();
update log_event as a,
    config_scheduler as b,
    dict_category as c
set a.category_code = c.`code`
where a.id = @nid
    and a.channel_id = b.id
    and b.category_name = c.`name`;
update log_event as a,
    dict_event as b
set a.`level` = b.`level`
where a.id = @nid
    and a.event_code = b.`code`;
update log_event as a
set a.`timestamp` = @ndt
where a.id = @nid;
select @nid into oid;
END