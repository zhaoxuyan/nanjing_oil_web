-- 创建数据库
drop database if exists oil_nanjing;
create database oil_nanjing character
set 'utf8' collate 'utf8_general_ci';
-- 创建测试数据库
drop database if exists oil_nanjing_test;
create database oil_nanjing_test character
set 'utf8' collate 'utf8_general_ci';
-- 创建用户
drop user 'www' @'%';
create user 'www' @'%' identified by 'password';
flush privileges;
-- 授权
grant all privileges on oil_nanjing.* to 'www' @'%';
grant all privileges on oil_nanjing_test.* to 'www' @'%';
flush privileges;
-- 查询用户
select host,
    user,
    authentication_string
from mysql.user;
-- 查询授权
show grants for 'www' @'%';