CREATE DEFINER = `www` @`%` PROCEDURE `insert_crash`(
    IN p_channel_id int,
    IN p_crash_code int,
    IN p_image_path varchar(200),
    IN p_video_path varchar(200),
    OUT oid int
) BEGIN
insert log_crash(channel_id, crash_code, image_path, video_path)
values (
        p_channel_id,
        p_crash_code,
        p_image_path,
        p_video_path
    );
set @nid = last_insert_id(),
    @ndt = current_timestamp();
update log_crash as a
set a.`timestamp` = @ndt
where a.id = @nid;
select @nid into oid;
END