CREATE DEFINER = `www` @`%` PROCEDURE `insert_state`(
    IN p_channel_id int,
    IN p_state_code int,
    IN p_seconds int,
    OUT oid int
) BEGIN
insert log_state(channel_id, state_code, seconds)
values (p_channel_id, p_state_code, p_seconds);
set @nid = last_insert_id(),
    @ndt = current_timestamp();
update log_state as a,
    config_scheduler as b,
    dict_category as c
set a.category_code = c.`code`
where a.id = @nid
    and a.channel_id = b.id
    and b.category_name = c.`name`;
update log_state as a
set a.`end` = @ndt,
    a.`start` = date_sub(@ndt, interval p_seconds second)
where a.id = @nid;
select @nid into oid;
END