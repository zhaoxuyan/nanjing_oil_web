# Web 系统部署和升级手册

## 1. 准备材料

- 服务器的办公网 ip
- nanjing_oil_web_image.tar
- nanjing_mysql_image.tar
- oil_nanjing_web 后端代码
- dist.zip 前端代码

## 2. 部署步骤

### 2.1. mysql 数据库容器

```bash
# 加载镜像
docker load < nanjing_mysql_image.tar
docker tag image_id nanjing_mysql:latest
```

```bash
# 创建 nanjing_mysql 容器
docker run -itd --name nanjing_mysql -p 3306:3306 --restart=always \
  -e MYSQL_ROOT_PASSWORD=SWJTU@mysql123456 \
  nanjing_mysql
```

### 2.2. 配置文件

```bash
cp .env.template .env
```

```bash
APP_NAME=Nanjing Oil Web
SECRET_KEY=a_random_and_long_string
DB_SERVER=localhost
DB_PORT=3306
DB_USERNAME=www
DB_PASSWORD=password
DB_DATABASE=oil_nanjing
DB_DATABASE_TEST=
MAIL_SERVER=
MAIL_PORT=
MAIL_USE_SSL=
MAIL_USERNAME=
MAIL_PASSWORD=
MAIL_ADMINS=
```

### 2.3. Web 应用程序容器

```bash
# 加载镜像
docker load < nanjing_oil_web_image.tar
docker tag image_id nanjing_oil_web:latest
```

- `-v 宿主机路径:docker 内部路径`，宿主机路径根据实际情况配置，**docker 内部路径不可修改！**
- nanjing_oil_web 为 **Web 后端程序根目录**，该目录下包括一个 main.py 文件
- nanjing_oil_vue 为 **Web 前端程序路径**，请自行创建该目录，将 dist.zip 解压得到后的 **dist 文件夹** 放到该目录下
- datacenter 为图片视频文件夹根目录，影响 Web 程序图片视频访问功能
- oil_shanxi 为 C++ 程序根目录，该目录下包括一个 config 文件夹，影响可视化配置功能
- test_video 为测试视频目录，可根据需求考虑是否挂载

```bash
chmod +x boot.sh
# 创建 nanjing_web 容器
docker run -itd --name=nanjing_web --network=host --restart=always \
  -v /DATACENTER1/oil_nanjing/nanjing_oil_web:/DATACENTER1/nanjing_oil_web \
  -v /DATACENTER1/oil_nanjing/nanjing_oil_vue:/DATACENTER1/nanjing_oil_vue \
  -v /DATACENTER1/oil_nanjing/datacenter:/DATACENTER1/datacenter \
  -v /DATACENTER1/oil_nanjing/oil_shanxi:/DATACENTER1/oil_shanxi \
  -v /DATACENTER1/oil_nanjing/test_video:/DATACENTER1/test_video \
  -v /etc/localtime:/etc/localtime \
  -v /etc/timezone:/etc/timezone \
  nanjing_oil_web /DATACENTER1/nanjing_oil_web/boot.sh
```

### 2.4. 验证运行情况

```bash
netstat -tnl
```

- 能看到 `0.0.0.0:3306` 或 `:::3306` ，说明数据库服务正常
- 能看到 `127.0.0.1:8000` ，说明 Web 后端应用程序运行正常
- 能看到 `0.0.0.0:8080` ，说明 Web 后端 nginx 代理正常
- 能看到 `0.0.0.0:8888` ，说明 Web 前端界面 nginx 代理正常

如果以上服务均正常，则可以：

- 通过 `办公网ip:8888` 访问用户界面
- 通过 `办公网ip:8080` 访问后端管理界面

## 3. 升级步骤

### 3.1. Web 前端界面升级

1. 备份旧的 dist 文件夹，把新的 **dist 文件夹** 放到宿主机 Web 前端程序路径。

### 3.2. Web 后端程序升级

1. 停止 docker 容器。`docker stop nanjing_web`
2. 备份旧的 oil_nanjing_web 文件夹，把新的代码放到宿主机 Web 后端程序路径。
3. 启动 docker 容器。`docker start nanjing_web`
