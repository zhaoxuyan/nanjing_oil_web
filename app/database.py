# -*- coding: utf-8 -*-

from . import db
from .utils import read_json


def init_users():
    payload = read_json('data/models.json')
    from .models import User
    with db.get_app().app_context():
        items_ready_add = []
        for item in payload.get('users'):
            if User.query.filter(User.username == item.get('username')).count() == 0:
                user = User.from_dict(item)
                if user:
                    user.set_all_permissions(item.get('permission'))
                    items_ready_add.append(user)
            else:
                res = User.query.filter(User.username == item.get('username')).first()
                res = User.from_dict(item, res)
                res.set_all_permissions(item.get('permission'))
                db.session.commit()
        if items_ready_add:
            db.session.add_all(items_ready_add)
            db.session.commit()


def init_metadata():
    payload = read_json('data/beans/metadata.json')
    from .beans import Category, Event, State, Crash
    with db.get_app().app_context():
        items_ready_add = []
        for item in payload.get('categories'):
            if Category.query.filter(Category.code == item.get('code')).count() == 0:
                category = Category.from_dict(item)
                if category:
                    items_ready_add.append(category)
            else:
                res = Category.query.filter(Category.code == item.get('code')).first()
                res = Category.from_dict(item, res)
                db.session.commit()
        if items_ready_add:
            db.session.add_all(items_ready_add)
            db.session.commit()

        items_ready_add = []
        for item in payload.get('events'):
            if Event.query.filter(Event.code == item.get('code')).count() == 0:
                event = Event.from_dict(item)
                if event:
                    items_ready_add.append(event)
            else:
                res = Event.query.filter(Event.code == item.get('code')).first()
                res = Event.from_dict(item, res)
                db.session.commit()
        if items_ready_add:
            db.session.add_all(items_ready_add)
            db.session.commit()

        items_ready_add = []
        for item in payload.get('states'):
            if State.query.filter(State.code == item.get('code')).count() == 0:
                state = State.from_dict(item)
                if state:
                    items_ready_add.append(state)
            else:
                res = State.query.filter(State.code == item.get('code')).first()
                res = State.from_dict(item, res)
                db.session.commit()
        if items_ready_add:
            db.session.add_all(items_ready_add)
            db.session.commit()

        items_ready_add = []
        for item in payload.get('crashes'):
            if Crash.query.filter(Crash.code == item.get('code')).count() == 0:
                crash = Crash.from_dict(item)
                if crash:
                    items_ready_add.append(crash)
            else:
                res = Crash.query.filter(Crash.code == item.get('code')).first()
                res = Crash.from_dict(item, res)
                db.session.commit()
        if items_ready_add:
            db.session.add_all(items_ready_add)
            db.session.commit()
