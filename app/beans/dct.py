# -*- coding: utf-8 -*-

from .. import db
from ..models import PaginatedAPIMixin

from . import LEVELS


class Category(PaginatedAPIMixin, db.Model):
    __tablename__ = 'dict_category'

    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.Integer, index=True, unique=True)
    name = db.Column(db.String(20), index=True, unique=True)
    name_cn = db.Column(db.String(50))
    model_path = db.Column(db.String(200))
    model_config = db.Column(db.String(200))
    schedulers = db.relationship('Scheduler', backref='category', lazy='dynamic')

    def __repr__(self):
        return '<Category {} {}>'.format(self.code, self.name)

    def to_dict(self):
        """序列化object为dict

        Returns:
            dict: 转换结果

            {
                'code': self.code,
                'name': self.name,
                'name_cn': self.name_cn,
                'model_path': self.model_path,
                'model_config': self.model_config
            }
        """
        payload = {
            'code': self.code,
            'name': self.name,
            'name_cn': self.name_cn,
            'model_path': self.model_path,
            'model_config': self.model_config
        }
        return payload

    @staticmethod
    def from_dict(payload, obj=None):
        """从dict反序列化为object

        Args:
            payload (dict): payload
            obj (Category, optional): 已有的Category对象. Defaults to None.

        Returns:
            Category: 转换结果，当缺少必要字段时返回None
        """
        if obj is None:
            required_fields = ['code', 'name']
            for field in required_fields:
                if field not in payload:
                    return None
            obj = Category()
            obj.code = payload.get('code', 0)
            obj.name = payload.get('name', '')[:20]
        obj.name_cn = payload.get('name_cn', '')[:50]
        obj.model_path = payload.get('model_path', '')[:200]
        obj.model_config = payload.get('model_config', '')[:200]
        return obj


class Event(PaginatedAPIMixin, db.Model):
    __tablename__ = 'dict_event'

    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.Integer, index=True, unique=True)
    name_cn = db.Column(db.String(100))
    level = db.Column(db.Integer)
    event_logs = db.relationship('EventLog', backref='event', lazy='dynamic')

    def __repr__(self):
        return '<Event {} {}>'.format(self.code, self.name_cn)

    def to_dict(self):
        """序列化object为dict

        Returns:
            dict: 转换结果

            {
                'code': self.code,
                'name_cn': self.name_cn,
                'level': self.level
            }
        """
        payload = {
            'code': self.code,
            'name_cn': self.name_cn,
            'level': self.level
        }
        return payload

    @staticmethod
    def from_dict(payload, obj=None):
        """从dict反序列化为object

        Args:
            payload (dict): payload
            obj (Event, optional): 已有的Event对象. Defaults to None.

        Returns:
            Event: 转换结果，当缺少必要字段时返回None
        """
        if obj is None:
            required_fields = ['code']
            for field in required_fields:
                if field not in payload:
                    return None
            obj = Event()
            obj.code = payload.get('code', 0)
        obj.name_cn = payload.get('name_cn', '')[:100]
        obj.level = payload.get('level', list(LEVELS.keys())[0] if LEVELS else 0)
        return obj


class State(PaginatedAPIMixin, db.Model):
    __tablename__ = 'dict_state'

    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.Integer, index=True, unique=True)
    name_cn = db.Column(db.String(100))
    state_logs = db.relationship('StateLog', backref='state', lazy='dynamic')

    def __repr__(self):
        return '<State {} {}>'.format(self.code, self.name_cn)

    def to_dict(self):
        """序列化object为dict

        Returns:
            dict: 转换结果

            {
                'code': self.code,
                'name_cn': self.name_cn
            }
        """
        payload = {
            'code': self.code,
            'name_cn': self.name_cn
        }
        return payload

    @staticmethod
    def from_dict(payload, obj=None):
        """从dict反序列化为object

        Args:
            payload (dict): payload
            obj (State, optional): 已有的State对象. Defaults to None.

        Returns:
            State: 转换结果，当缺少必要字段时返回None
        """
        if obj is None:
            required_fields = ['code']
            for field in required_fields:
                if field not in payload:
                    return None
            obj = State()
            obj.code = payload.get('code', 0)
        obj.name_cn = payload.get('name_cn', '')[:100]
        return obj


class Crash(PaginatedAPIMixin, db.Model):
    __tablename__ = 'dict_crash'

    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.Integer, index=True, unique=True)
    name_cn = db.Column(db.String(100))
    crash_logs = db.relationship('CrashLog', backref='crash', lazy='dynamic')

    def __repr__(self):
        return '<Crash {} {}>'.format(self.code, self.name_cn)

    def to_dict(self):
        """序列化object为dict

        Returns:
            dict: 转换结果

            {
                'code': self.code,
                'name_cn': self.name_cn
            }
        """
        payload = {
            'code': self.code,
            'name_cn': self.name_cn
        }
        return payload

    @staticmethod
    def from_dict(payload, obj=None):
        """从dict反序列化为object

        Args:
            payload (dict): payload
            obj (Crash, optional): 已有的Crash对象. Defaults to None.

        Returns:
            Crash: 转换结果，当缺少必要字段时返回None
        """
        if obj is None:
            required_fields = ['code']
            for field in required_fields:
                if field not in payload:
                    return None
            obj = Crash()
            obj.code = payload.get('code', 0)
        obj.name_cn = payload.get('name_cn', '')[:100]
        return obj
