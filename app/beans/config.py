# -*- coding: utf-8 -*-

import os.path as osp

from .. import db
from ..models import PaginatedAPIMixin
from ..utils import save_screenshot

from . import TEMP_DIR, STATIC_DIR


class Camera(PaginatedAPIMixin, db.Model):
    __tablename__ = 'config_camera'

    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String(200), index=True, unique=True)
    schedulers = db.relationship('Scheduler', backref='camera', lazy='dynamic')

    def __repr__(self):
        return '<Camera {}>'.format(self.url)

    def capture_screenshot(self, image_dir=osp.join(TEMP_DIR, 'camera')):
        return save_screenshot(self.url, image_dir=image_dir)

    def to_dict(self):
        """序列化object为dict

        Returns:
            dict: 转换结果

            {
                'id': self.id,
                'url': self.url
            }
        """
        payload = {
            'id': self.id,
            'url': self.url
        }
        return payload

    @staticmethod
    def from_dict(payload, obj=None):
        """从dict反序列化为object

        Args:
            payload (dict): payload
            obj (Camera, optional): 已有的Camera对象. Defaults to None.

        Returns:
            Camera: 转换结果，当缺少必要字段时返回None
        """
        if obj is None:
            required_fields = ['url']
            for field in required_fields:
                if field not in payload:
                    return None
            obj = Camera()
            obj.url = payload.get('url', '')[:200]
        return obj


class Scheduler(PaginatedAPIMixin, db.Model):
    __tablename__ = 'config_scheduler'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), index=True, unique=True)
    category_name = db.Column(db.String(20), db.ForeignKey('dict_category.name'))
    start = db.Column(db.Integer)
    batch = db.Column(db.Integer)
    url = db.Column(db.String(200), db.ForeignKey('config_camera.url'))
    height = db.Column(db.Integer)
    width = db.Column(db.Integer)
    buffer = db.Column(db.Integer)
    image_path = db.Column(db.String(200))
    services = db.relationship('Service', backref='scheduler', lazy='dynamic')
    masks = db.relationship('Mask', backref='scheduler', lazy='dynamic')
    event_logs = db.relationship('EventLog', backref='scheduler', lazy='dynamic')
    state_logs = db.relationship('StateLog', backref='scheduler', lazy='dynamic')
    crash_logs = db.relationship('CrashLog', backref='scheduler', lazy='dynamic')

    def __repr__(self):
        return '<Scheduler {}>'.format(self.id)

    def capture_screenshot(self, image_dir=osp.join(STATIC_DIR, 'scheduler')):
        return save_screenshot(self.url, image_dir=image_dir)

    def to_dict(self):
        """序列化object为dict

        Returns:
            dict: 转换结果

            {
                'channel_id': self.id,
                'name': self.name,
                'category_name': self.category_name,
                'start': self.start,
                'batch': self.batch,
                'url': self.url,
                'height': self.height,
                'width': self.width,
                'buffer': self.buffer,
                'image_path': self.image_path
            }
        """
        payload = {
            'channel_id': self.id,
            'name': self.name,
            'category_name': self.category_name,
            'start': self.start,
            'batch': self.batch,
            'url': self.url,
            'height': self.height,
            'width': self.width,
            'buffer': self.buffer,
            'image_path': self.image_path
        }
        return payload

    @staticmethod
    def from_dict(payload, obj=None):
        """从dict反序列化为object

        Args:
            payload (dict): payload
            obj (Scheduler, optional): 已有的Scheduler对象. Defaults to None.

        Returns:
            Scheduler: 转换结果，当缺少必要字段时返回None
        """
        if obj is None:
            required_fields = ['name', 'category_name', 'url']
            for field in required_fields:
                if field not in payload:
                    return None
            obj = Scheduler()
            obj.name = payload.get('name', '')[:20]
            obj.category_name = payload.get('category_name', '')[:20]
            obj.url = payload.get('url', '')[:200]
        obj.start = payload.get('start', 0)
        obj.batch = payload.get('batch', 4)
        obj.height = payload.get('height', 720)
        obj.width = payload.get('width', 1280)
        obj.buffer = payload.get('buffer', 30)
        return obj


class Service(PaginatedAPIMixin, db.Model):
    __tablename__ = 'config_service'

    id = db.Column(db.Integer, primary_key=True)
    channel_id = db.Column(db.Integer, db.ForeignKey('config_scheduler.id'))
    name = db.Column(db.String(20), index=True)
    ip = db.Column(db.String(50))
    port = db.Column(db.Integer)

    def __repr__(self):
        return '<Service {}>'.format(self.id)

    def to_dict(self):
        """序列化object为dict

        Returns:
            dict: 转换结果

            {
                'service_id': self.id,
                'channel_id': self.channel_id,
                'name': self.name,
                'ip': self.ip,
                'port': self.port
            }
        """
        payload = {
            'service_id': self.id,
            'channel_id': self.channel_id,
            'name': self.name,
            'ip': self.ip,
            'port': self.port
        }
        return payload

    @staticmethod
    def from_dict(payload, obj=None):
        """从dict反序列化为object

        Args:
            payload (dict): payload
            obj (Service, optional): 已有的Service对象. Defaults to None.

        Returns:
            Service: 转换结果，当缺少必要字段时返回None
        """
        if obj is None:
            required_fields = ['channel_id', 'name']
            for field in required_fields:
                if field not in payload:
                    return None
            obj = Service()
            obj.channel_id = payload.get('channel_id', 0)
            obj.name = payload.get('name', '')[:20]
        obj.ip = payload.get('ip', '0.0.0.0')[:50]
        obj.port = payload.get('port', -1)
        return obj


class Mask(PaginatedAPIMixin, db.Model):
    __tablename__ = 'config_mask'

    id = db.Column(db.Integer, primary_key=True)
    channel_id = db.Column(db.Integer, db.ForeignKey('config_scheduler.id'))
    name = db.Column(db.String(100), index=True)
    x_min = db.Column(db.Integer)
    y_min = db.Column(db.Integer)
    x_max = db.Column(db.Integer)
    y_max = db.Column(db.Integer)

    def __repr__(self):
        return '<Mask {}>'.format(self.id)

    def to_dict(self):
        """序列化object为dict

        Returns:
            dict: 转换结果

            {
                'mask_id': self.id,
                'channel_id': self.channel_id,
                'name': self.name,
                'x_min': self.x_min,
                'y_min': self.y_min,
                'x_max': self.x_max,
                'y_max': self.y_max
            }
        """
        payload = {
            'mask_id': self.id,
            'channel_id': self.channel_id,
            'name': self.name,
            'x_min': self.x_min,
            'y_min': self.y_min,
            'x_max': self.x_max,
            'y_max': self.y_max
        }
        return payload

    @staticmethod
    def from_dict(payload, obj=None):
        """从dict反序列化为object

        Args:
            payload (dict): payload
            obj (Mask, optional): 已有的Mask对象. Defaults to None.

        Returns:
            Mask: 转换结果，当缺少必要字段时返回None
        """
        if obj is None:
            required_fields = ['channel_id', 'name']
            for field in required_fields:
                if field not in payload:
                    return None
            obj = Mask()
            obj.channel_id = payload.get('channel_id', 0)
            obj.name = payload.get('name', '')[:100]
        obj.x_min = payload.get('x_min', -1)
        obj.y_min = payload.get('y_min', -1)
        obj.x_max = payload.get('x_max', -1)
        obj.y_max = payload.get('y_max', -1)
        return obj
