# -*- coding: utf-8 -*-

from flask import jsonify

from ... import read_required
from ...api import token_auth
from .. import LEVELS, SERVICES
from . import bp


@bp.route('/level', methods=['GET'])
@token_auth.login_required
@read_required()
def get_levels():
    """获取所有违规等级

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 bean_api 模块有 R 权限。

    ## 响应示例

    ```json
    {
        "0": "合规",
        "1": "轻度违规",
        "2": "中度违规",
        "3": "严重违规"
    }
    ```

    @@@
    """
    return jsonify(LEVELS)


@bp.route('/service', methods=['GET'])
@token_auth.login_required
@read_required()
def get_services():
    """获取所有模型服务类型

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 bean_api 模块有 R 权限。

    ## 响应示例

    ```json
    {
        "detect": "目标检测",
        "smoke": "烟火检测"
    }
    ```

    @@@
    """
    return jsonify(SERVICES)
