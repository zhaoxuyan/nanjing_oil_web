# -*- coding: utf-8 -*-

from flask import request, jsonify

from ... import read_required, RECORDS_PER_PAGE, RECORDS_MAX_PER_PAGE
from ...api import token_auth
from .. import Category, Event, State, Crash
from . import bp


@bp.route('/category', methods=['GET'])
@token_auth.login_required
@read_required()
def get_categories():
    """获取场景定义

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 bean_api 模块有 R 权限。

    ## 查询字段

    | 字段     | 是否必须 | 类型 | 说明     |
    | -------- | -------- | ---- | -------- |
    | code     |          | int  |          |
    | name     |          | str  |          |
    | name_cn  |          | str  | 模糊查询 |
    | page     |          | int  | 默认：1  |
    | per_page |          | int  | 默认：25 |

    ## 响应示例

    ```json
    {
        "_links": {
            "next": null,
            "prev": null,
            "self": "/api/category?page=1&per_page=25"
        },
        "_meta": {
            "page": 1,
            "per_page": 25,
            "total_items": 3,
            "total_pages": 1
        },
        "items": [
            {
                "code": 1,
                "model_config": "",
                "model_path": "",
                "name": "unload",
                "name_cn": "卸油区"
            },
            {
                "code": 2,
                "model_config": "",
                "model_path": "",
                "name": "checkout",
                "name_cn": "收银台"
            },
            {
                "code": 3,
                "model_config": "",
                "model_path": "",
                "name": "refuel_side",
                "name_cn": "加油区"
            },
            {
                "code": 4,
                "model_config": "",
                "model_path": "",
                "name": "gate",
                "name_cn": "便利店大门"
            }
        ]
    }
    ```

    @@@
    """
    code = request.args.get('code', None, type=int)
    name = request.args.get('name', None, type=str)
    name_cn = request.args.get('name_cn', None, type=str)
    page = request.args.get('page', 1, type=int)
    per_page = request.args.get('per_page', RECORDS_PER_PAGE, type=int)
    per_page = min(per_page, RECORDS_MAX_PER_PAGE)
    custom_query = Category.query
    if code is not None:
        custom_query = custom_query.filter(Category.code == code)
    if name is not None:
        custom_query = custom_query.filter(Category.name == name)
    if name_cn is not None:
        custom_query = custom_query.filter(Category.name_cn.like('%' + name_cn + '%'))
    custom_query = custom_query.order_by(Category.id.asc())
    payload = Category.to_collection_dict(custom_query, page, per_page, 'bean_api.get_categories',
        code=code, name=name, name_cn=name_cn)  # NOQA
    return jsonify(payload)


@bp.route('/event', methods=['GET'])
@token_auth.login_required
@read_required()
def get_events():
    """获取事件定义

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 bean_api 模块有 R 权限。

    ## 查询字段

    | 字段     | 是否必须 | 类型 | 说明     |
    | -------- | -------- | ---- | -------- |
    | code     |          | int  |          |
    | name_cn  |          | str  | 模糊查询 |
    | level    |          | int  |          |
    | page     |          | int  | 默认：1  |
    | per_page |          | int  | 默认：25 |

    ## 响应示例

    ```json
        {
        "_links": {
            "next": null,
            "prev": null,
            "self": "/api/event?page=1&per_page=25"
        },
        "_meta": {
            "page": 1,
            "per_page": 25,
            "total_items": 14,
            "total_pages": 1
        },
        "items": [
            {
                "code": 100001,
                "level": 2,
                "name_cn": "卸油区出现非工作人员"
            },
            {
                "code": 100002,
                "level": 2,
                "name_cn": "油罐车静置时间过短就进行卸油作业"
            },
            {
                "code": 100003,
                "level": 2,
                "name_cn": "卸油作业期间未连接静电夹"
            },
            {
                "code": 100004,
                "level": 2,
                "name_cn": "卸油作业期间未放置消防器材"
            },
            {
                "code": 100005,
                "level": 2,
                "name_cn": "卸油作业期间工作人员离开时间过长"
            },
            {
                "code": 100006,
                "level": 1,
                "name_cn": "顾客服务时间过长"
            },
            {
                "code": 100007,
                "level": 2,
                "name_cn": "收银台内部出现非工作人员"
            },
            {
                "code": 100008,
                "level": 1,
                "name_cn": "顾客排队人数超限"
            },
            {
                "code": 100009,
                "level": 3,
                "name_cn": "加油区出现烟火"
            },
            {
                "code": 100010,
                "level": 0,
                "name_cn": "油罐车到达"
            },
            {
                "code": 100011,
                "level": 0,
                "name_cn": "油罐车离开"
            },
            {
                "code": 100012,
                "level": 0,
                "name_cn": "接油管已连接"
            },
            {
                "code": 100013,
                "level": 0,
                "name_cn": "消防器材已放置"
            },
            {
                "code": 100014,
                "level": 0,
                "name_cn": "静电夹已连接"
            },
            {
                "code": 100015,
                "level": 0,
                "name_cn": "顾客进入便利店"
            },
            { 
                "code": 100016,
                "level": 0,
                "name_cn": "卸油作业期间存在两名工作人员"
            }
        ]
    }
    ```

    @@@
    """
    code = request.args.get('code', None, type=int)
    name_cn = request.args.get('name_cn', None, type=str)
    level = request.args.get('level', None, type=int)
    page = request.args.get('page', 1, type=int)
    per_page = request.args.get('per_page', RECORDS_PER_PAGE, type=int)
    per_page = min(per_page, RECORDS_MAX_PER_PAGE)
    custom_query = Event.query
    if code is not None:
        custom_query = custom_query.filter(Event.code == code)
    if name_cn is not None:
        custom_query = custom_query.filter(Category.name_cn.like('%' + name_cn + '%'))
    if level is not None:
        custom_query = custom_query.filter(Event.level == level)
    custom_query = custom_query.order_by(Event.id.asc())
    payload = Event.to_collection_dict(custom_query, page, per_page, 'bean_api.get_events',
        code=code, name_cn=name_cn, level=level)  # NOQA
    return jsonify(payload)


@bp.route('/state', methods=['GET'])
@token_auth.login_required
@read_required()
def get_states():
    """获取状态定义

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 bean_api 模块有 R 权限。

    ## 查询字段

    | 字段     | 是否必须 | 类型 | 说明     |
    | -------- | -------- | ---- | -------- |
    | code     |          | int  |          |
    | name_cn  |          | str  | 模糊查询 |
    | page     |          | int  | 默认：1  |
    | per_page |          | int  | 默认：25 |

    ## 响应示例

    ```json
    {
        "_links": {
            "next": null,
            "prev": null,
            "self": "/api/state?page=1&per_page=25"
        },
        "_meta": {
            "page": 1,
            "per_page": 25,
            "total_items": 1,
            "total_pages": 1
        },
        "items": [
            {
                "code": 200001,
                "name_cn": "顾客出现"
            }
        ]
    }
    ```

    @@@
    """
    code = request.args.get('code', None, type=int)
    name_cn = request.args.get('name_cn', None, type=str)
    page = request.args.get('page', 1, type=int)
    per_page = request.args.get('per_page', RECORDS_PER_PAGE, type=int)
    per_page = min(per_page, RECORDS_MAX_PER_PAGE)
    custom_query = State.query
    if code is not None:
        custom_query = custom_query.filter(State.code == code)
    if name_cn is not None:
        custom_query = custom_query.filter(Category.name_cn.like('%' + name_cn + '%'))
    custom_query = custom_query.order_by(State.id.asc())
    payload = State.to_collection_dict(custom_query, page, per_page, 'bean_api.get_states',
        code=code, name_cn=name_cn)  # NOQA
    return jsonify(payload)


@bp.route('/crash', methods=['GET'])
@token_auth.login_required
@read_required()
def get_crashes():
    """获取异常定义

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 bean_api 模块有 R 权限。

    ## 查询字段

    | 字段     | 是否必须 | 类型 | 说明     |
    | -------- | -------- | ---- | -------- |
    | code     |          | int  |          |
    | name_cn  |          | str  | 模糊查询 |
    | page     |          | int  | 默认：1  |
    | per_page |          | int  | 默认：25 |

    ## 响应示例

    ```json
    {
        "_links": {
            "next": null,
            "prev": null,
            "self": "/api/crash?page=1&per_page=25"
        },
        "_meta": {
            "page": 1,
            "per_page": 25,
            "total_items": 4,
            "total_pages": 1
        },
        "items": [
            {
                "code": 300001,
                "name_cn": "视频流无法连接"
            },
            {
                "code": 300002,
                "name_cn": "视频画面巨幅改变"
            },
            {
                "code": 300003,
                "name_cn": "视频画面内容模糊"
            },
            {
                "code": 300004,
                "name_cn": "视频画面中出现遮挡物"
            }
        ]
    }
    ```

    @@@
    """
    code = request.args.get('code', None, type=int)
    name_cn = request.args.get('name_cn', None, type=str)
    custom_query = Crash.query
    if code is not None:
        custom_query = custom_query.filter(Crash.code == code)
    if name_cn is not None:
        custom_query = custom_query.filter(Category.name_cn.like('%' + name_cn + '%'))
    page = request.args.get('page', 1, type=int)
    per_page = request.args.get('per_page', RECORDS_PER_PAGE, type=int)
    per_page = min(per_page, RECORDS_MAX_PER_PAGE)
    custom_query = custom_query.order_by(Crash.id.asc())
    payload = Crash.to_collection_dict(custom_query, page, per_page, 'bean_api.get_crashes',
        code=code, name_cn=name_cn)  # NOQA
    return jsonify(payload)
