# -*- coding: utf-8 -*-

from flask import Blueprint
bp = Blueprint('bean_api', __name__)

from . import metadata  # NOQA
from . import dct  # NOQA
