# -*- coding: utf-8 -*-

# 临时文件夹
TEMP_DIR = 'tmp/beans'
# 静态资源文件夹
STATIC_DIR = 'app/static'

# 违规事件等级
LEVELS = {
    0: "合规",
    1: "轻度违规",
    2: "中度违规",
    3: "严重违规"
}
from .dct import Category  # NOQA
from .dct import Event  # NOQA
from .dct import State  # NOQA
from .dct import Crash  # NOQA

# 模型服务名
SERVICES = {
    'detect': '目标检测',
    'smoke': '烟火检测'
}
from .config import Camera  # NOQA
from .config import Scheduler  # NOQA
from .config import Service  # NOQA
from .config import Mask  # NOQA

from .log import EventLog  # NOQA
from .log import StateLog  # NOQA
from .log import CrashLog  # NOQA
