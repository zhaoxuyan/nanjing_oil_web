# -*- coding: utf-8 -*-

import os.path as osp
from datetime import datetime

from .. import db
from ..models import PaginatedAPIMixin
from ..utils import DATETIME_FORMATTER


class EventLog(PaginatedAPIMixin, db.Model):
    __tablename__ = 'log_event'

    id = db.Column(db.Integer, primary_key=True)
    channel_id = db.Column(db.Integer, db.ForeignKey('config_scheduler.id'))
    category_code = db.Column(db.Integer)
    event_code = db.Column(db.Integer, db.ForeignKey('dict_event.code'))
    level = db.Column(db.Integer)
    timestamp = db.Column(db.DateTime, default=datetime.now)
    image_path = db.Column(db.String(200))
    video_path = db.Column(db.String(200))

    def __repr__(self):
        return '<EventLog {}>'.format(self.id)

    def to_dict(self):
        """序列化object为dict

        Returns:
            dict: 转换结果

            {
                'log_id': self.id,
                'channel_id': self.channel_id,
                'category_code': self.category_code,
                'event_code': self.event_code,
                'level': self.level,
                'timestamp': self.timestamp,
                'image_path': self.image_path,
                'video_path': self.video_path
            }
        """
        payload = {
            'log_id': self.id,
            'channel_id': self.channel_id,
            'category_code': self.category_code,
            'event_code': self.event_code,
            'level': self.level,
            'timestamp': self.timestamp.strftime(DATETIME_FORMATTER) if self.timestamp else None,
            'image_path': osp.join('/image', self.image_path) if self.image_path and str.lower(
                self.image_path) != 'null' else None,
            'video_path': osp.join('/video', self.video_path) if self.video_path and str.lower(
                self.video_path) != 'null' else None
        }
        return payload

    @staticmethod
    def from_dict(payload, obj=None):
        """从json解析\n
        Args:\n
            payload dict 至少包含channel_id和event_code
        Returns:\n
            obj EventLog 根据payload解析得到的EventLog对象，如果payload中缺少必要的字段，将返回None
        """
        if obj is None:
            required_fields = ['channel_id', 'event_code']
            for field in required_fields:
                if field not in payload:
                    return None
            obj = EventLog()
            obj.channel_id = payload.get('channel_id', 0)
            obj.event_code = payload.get('event_code', 0)
        obj.category_code = payload.get('category_code', 0)
        obj.level = payload.get('level', 0)
        obj.timestamp = payload.get('timestamp', datetime.now())
        obj.image_path = payload.get('image_path', 'null')[:200]
        obj.video_path = payload.get('video_path', 'null')[:200]
        return obj


class StateLog(PaginatedAPIMixin, db.Model):
    __tablename__ = 'log_state'

    id = db.Column(db.Integer, primary_key=True)
    channel_id = db.Column(db.Integer, db.ForeignKey('config_scheduler.id'))
    category_code = db.Column(db.Integer)
    state_code = db.Column(db.Integer, db.ForeignKey('dict_state.code'))
    start = db.Column(db.DateTime, default=datetime.now)
    end = db.Column(db.DateTime, default=datetime.now)
    seconds = db.Column(db.Integer)

    def __repr__(self):
        return '<StateLog {}>'.format(self.id)

    def to_dict(self):
        """序列化object为dict

        Returns:
            dict: 转换结果

            {
                'log_id': self.id,
                'channel_id': self.channel_id,
                'category_code': self.category_code,
                'state_code': self.state_code,
                'start': self.start,
                'end': self.end,
                'seconds': self.seconds
            }
        """
        payload = {
            'log_id': self.id,
            'channel_id': self.channel_id,
            'category_code': self.category_code,
            'state_code': self.state_code,
            'start': self.start.strftime(DATETIME_FORMATTER) if self.start else None,
            'end': self.end.strftime(DATETIME_FORMATTER) if self.end else None,
            'seconds': self.seconds
        }
        return payload

    @staticmethod
    def from_dict(payload, obj=None):
        """从json解析\n
        Args:\n
            payload dict 至少包含channel_id和state_code
        Returns:\n
            obj StateLog 根据payload解析得到的StateLog对象，如果payload中缺少必要的字段，将返回None
        """
        if obj is None:
            required_fields = ['channel_id', 'state_code']
            for field in required_fields:
                if field not in payload:
                    return None
            obj = StateLog()
            obj.channel_id = payload.get('channel_id', 0)
            obj.state_code = payload.get('state_code', 0)
        obj.category_code = payload.get('category_code', 0)
        obj.start = payload.get('start', datetime.now())
        obj.end = payload.get('end', datetime.now())
        obj.seconds = payload.get('seconds', 0)
        return obj


class CrashLog(PaginatedAPIMixin, db.Model):
    __tablename__ = 'log_crash'

    id = db.Column(db.Integer, primary_key=True)
    channel_id = db.Column(db.Integer, db.ForeignKey('config_scheduler.id'))
    crash_code = db.Column(db.Integer, db.ForeignKey('dict_crash.code'))
    timestamp = db.Column(db.DateTime, default=datetime.now)
    image_path = db.Column(db.String(200))
    video_path = db.Column(db.String(200))

    def __repr__(self):
        return '<CrashLog {}>'.format(self.id)

    def to_dict(self):
        """序列化object为dict

        Returns:
            dict: 转换结果

            {
                'log_id': self.id,
                'channel_id': self.channel_id,
                'crash_code': self.crash_code,
                'timestamp': self.timestamp,
                'image_path': self.image_path,
                'video_path': self.video_path
            }
        """
        payload = {
            'log_id': self.id,
            'channel_id': self.channel_id,
            'crash_code': self.crash_code,
            'timestamp': self.timestamp.strftime(DATETIME_FORMATTER) if self.timestamp else None,
            'image_path': osp.join('/image', self.image_path) if self.image_path and str.lower(
                self.image_path) != 'null' else None,
            'video_path': osp.join('/video', self.video_path) if self.video_path and str.lower(
                self.video_path) != 'null' else None
        }
        return payload

    @staticmethod
    def from_dict(payload, obj=None):
        """从json解析\n
        Args:\n
            payload dict 至少包含channel_id和crash_code
        Returns:\n
            obj CrashLog 根据payload解析得到的CrashLog对象，如果payload中缺少必要的字段，将返回None
        """
        if obj is None:
            required_fields = ['channel_id', 'crash_code']
            for field in required_fields:
                if field not in payload:
                    return None
            obj = CrashLog()
            obj.channel_id = payload.get('channel_id', 0)
            obj.crash_code = payload.get('crash_code', 0)
        obj.timestamp = payload.get('timestamp', datetime.now())
        obj.image_path = payload.get('image_path', 'null')[:200]
        obj.video_path = payload.get('video_path', 'null')[:200]
        return obj
