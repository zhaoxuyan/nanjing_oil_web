# -*- coding: utf-8 -*-

from flask import Blueprint
bp = Blueprint('record', __name__)

# 临时文件夹
TEMP_DIR = 'tmp/record'

# 日志统计时间跨度
RECORD_DAY_SPAN = 62

# 定时计算时间跨度，要与 jobs.py 定时设置对应
UPDATE_HOUR_SPAN = 1

# 模型
RECODER_MODELS = {
    'count': '次数计数模型',
    'sum': '字段求和模型',
}

# 日志表
RECODER_TABLES = {
    'event_log': '事件日志',
    'state_log': '状态日志',
    'crash_log': '异常日志'
}

from . import routes  # NOQA
