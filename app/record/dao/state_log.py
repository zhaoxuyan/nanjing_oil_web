# -*- coding: utf-8 -*-

from sqlalchemy import func

from ...beans import StateLog


def query_state_log(start, end, channel_id=None, category_code=None, state_code=None):
    custom_query = StateLog.query.filter(StateLog.start >= start)
    custom_query = StateLog.query.filter(StateLog.start < end)
    if channel_id is not None:
        custom_query = custom_query.filter(StateLog.channel_id == channel_id)
    if category_code is not None:
        custom_query = custom_query.filter(StateLog.category_code == category_code)
    if state_code is not None:
        custom_query = custom_query.filter(StateLog.state_code == state_code)
    return custom_query.all()


def count_state_log(start, end, channel_id=None, category_code=None, state_code=None):
    custom_query = StateLog.query.filter(StateLog.start.between(start, end))
    if channel_id is not None:
        custom_query = custom_query.filter(StateLog.channel_id == channel_id)
    if category_code is not None:
        custom_query = custom_query.filter(StateLog.category_code == category_code)
    if state_code is not None:
        custom_query = custom_query.filter(StateLog.state_code == state_code)
    return custom_query.count()


def sum_state_log_seconds(start, end, channel_id=None, category_code=None, state_code=None):
    custom_query = StateLog.query.filter(StateLog.start.between(start, end))
    if channel_id is not None:
        custom_query = custom_query.filter(StateLog.channel_id == channel_id)
    if category_code is not None:
        custom_query = custom_query.filter(StateLog.category_code == category_code)
    if state_code is not None:
        custom_query = custom_query.filter(StateLog.state_code == state_code)
    custom_query = custom_query.with_entities(func.sum(StateLog.seconds))
    return int(custom_query.scalar() or 0)
