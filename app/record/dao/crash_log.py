# -*- coding: utf-8 -*-

from ...beans import CrashLog


def query_crash_log(start, end, channel_id=None, crash_code=None):
    custom_query = CrashLog.query.filter(CrashLog.timestamp >= start)
    custom_query = CrashLog.query.filter(CrashLog.timestamp < end)
    if channel_id is not None:
        custom_query = custom_query.filter(CrashLog.channel_id == channel_id)
    if crash_code is not None:
        custom_query = custom_query.filter(CrashLog.crash_code == crash_code)
    return custom_query.all()


def count_crash_log(start, end, channel_id=None, crash_code=None):
    custom_query = CrashLog.query.filter(CrashLog.timestamp.between(start, end))
    if channel_id is not None:
        custom_query = custom_query.filter(CrashLog.channel_id == channel_id)
    if crash_code is not None:
        custom_query = custom_query.filter(CrashLog.crash_code == crash_code)
    return custom_query.count()
