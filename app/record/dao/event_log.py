# -*- coding: utf-8 -*-

from sqlalchemy import func

from ...beans import EventLog


def query_event_log(start, end, channel_id=None, category_code=None, event_code=None, level=None):
    custom_query = EventLog.query.filter(EventLog.timestamp >= start)
    custom_query = EventLog.query.filter(EventLog.timestamp < end)
    if channel_id is not None:
        custom_query = custom_query.filter(EventLog.channel_id == channel_id)
    if category_code is not None:
        custom_query = custom_query.filter(EventLog.category_code == category_code)
    if event_code is not None:
        custom_query = custom_query.filter(EventLog.event_code == event_code)
    if level is not None:
        custom_query = custom_query.filter(EventLog.level == level)
    return custom_query.all()


def count_event_log(start, end, channel_id=None, category_code=None, event_code=None, level=None):
    custom_query = EventLog.query.filter(EventLog.timestamp.between(start, end))
    if channel_id is not None:
        custom_query = custom_query.filter(EventLog.channel_id == channel_id)
    if category_code is not None:
        custom_query = custom_query.filter(EventLog.category_code == category_code)
    if event_code is not None:
        custom_query = custom_query.filter(EventLog.event_code == event_code)
    if level is not None:
        custom_query = custom_query.filter(EventLog.level == level)
    return custom_query.count()


def sum_event_log_level(start, end, channel_id=None, category_code=None, event_code=None, level=None):
    custom_query = EventLog.query.filter(EventLog.timestamp.between(start, end))
    if channel_id is not None:
        custom_query = custom_query.filter(EventLog.channel_id == channel_id)
    if category_code is not None:
        custom_query = custom_query.filter(EventLog.category_code == category_code)
    if event_code is not None:
        custom_query = custom_query.filter(EventLog.event_code == event_code)
    if level is not None:
        custom_query = custom_query.filter(EventLog.level == level)
    custom_query = custom_query.with_entities(func.sum(EventLog.level))
    return int(custom_query.scalar() or 0)
