# -*- coding: utf-8 -*-

from datetime import datetime

from ...utils import hour_start, DATE_FORMATTER, TIME_FORMATTER


def get_date_time_str(dt):
    """设置dt为整小时后，获取日期字符串和时间字符串

    Args:
        dt (datetime): datetime对象

    Returns:
        tuple: date_str, time_str
    """
    if not isinstance(dt, datetime):
        return None, None
    dt = hour_start(dt)
    date_str = dt.strftime(DATE_FORMATTER)
    time_str = dt.strftime(TIME_FORMATTER)
    return date_str, time_str
