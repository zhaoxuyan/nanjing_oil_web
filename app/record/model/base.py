# -*- coding: utf-8 -*-

import os
from datetime import timedelta
from abc import ABCMeta, abstractmethod

from ...utils import hour_start, read_json, write_json
from .. import RECORD_DAY_SPAN, UPDATE_HOUR_SPAN, TEMP_DIR
from .math import keep_decimals, good_ratio, bad_ratio, good_count, bad_count
from .utils import get_date_time_str


class Recorder(metaclass=ABCMeta):
    """模型基类"""

    @abstractmethod
    def __init__(self, name, payload={}):
        """初始化属性

        Args:
            name (str): 用于标识
            payload (dict, optional): 数据. Defaults to {}.
        """
        self.name = name
        self.payload = payload
        self.filename = '.'.join([str(self.name), 'json'])

    @abstractmethod
    def init(self, start, end, default=0):
        """初始化键和值

        Args:
            start (datetime): 开始时刻
            end (datetime): 结束时刻
            default (int, optional): 默认值. Defaults to 0.

        Returns:
            tuple: start, end
        """
        start, end = hour_start(start), hour_start(end)
        s = start
        while s < end:
            date_str, time_str = get_date_time_str(s)
            if date_str not in self.payload:
                self.payload.setdefault(date_str, {})
            self.payload[date_str][time_str] = default
            s += timedelta(hours=UPDATE_HOUR_SPAN)
        return start, end

    @abstractmethod
    def update(self, start, default=0):
        """初始化键和值

        Args:
            start (datetime): 开始时刻
            default (int, optional): 默认值. Defaults to 0.

        Returns:
            tuple: date_str, time_str, start, end
        """
        date_str, time_str = get_date_time_str(start)
        if date_str not in self.payload:
            self.payload.setdefault(date_str, {})
        if time_str not in self.payload.get(date_str):
            self.payload[date_str].setdefault(time_str, default)
        # 删除超过保存时间跨度的数据
        date_str2, _ = get_date_time_str(start - timedelta(days=RECORD_DAY_SPAN + 1))
        if date_str2 in self.payload:
            self.payload.pop(date_str2)
        end = start + timedelta(hours=UPDATE_HOUR_SPAN)
        return date_str, time_str, start, end

    def read(self, json_dir=TEMP_DIR):
        """初始化读json

        Args:
            json_dir (str, optional): 路径. Defaults to TEMP_DIR.
        """
        if not os.path.exists(json_dir):
            os.makedirs(json_dir)
        self.payload = read_json(os.path.join(json_dir, self.filename))

    def write(self, json_dir=TEMP_DIR):
        """初始化写json

        Args:
            json_dir (str, optional): 路径. Defaults to TEMP_DIR.
        """
        if not os.path.exists(json_dir):
            os.makedirs(json_dir)
        write_json(self.payload, os.path.join(json_dir, self.filename), new_thread=True)

    def operation1(self, f, *args, **kwargs):
        """单目操作

        Args:
            f (function): 元素操作函数

        Returns:
            dict: payload
        """
        return {
            date_str: {
                time_str: keep_decimals(f(i, *args, **kwargs)) for time_str, i in zip(
                    self.payload.get(date_str).keys(),
                    self.payload.get(date_str).values())
            } for date_str in self.payload
        }

    def operation2(self, other, f):
        """双目操作

        Args:
            other (Recoder): 另一个Recorder
            f (function): 元素操作函数

        Returns:
            dict: payload
        """
        return {
            date_str: {
                time_str: keep_decimals(f(i, j)) for time_str, i, j in zip(
                    self.payload.get(date_str).keys(),
                    self.payload.get(date_str).values(),
                    other.payload.get(date_str).values())
            } for date_str in self.payload
        }

    def __add__(self, other):
        return self.add(other)

    def __iadd__(self, other):
        return self.add(other)

    def add(self, other):
        """Recorder相加

        Args:
            other (Recorder): 另一个Recorder

        Returns:
            Recorder: 运算结果
        """
        if isinstance(other, Recorder):
            self.payload = self.operation2(other, (lambda x, y: x + y))
        return self

    def __sub__(self, other):
        return self.sub(other)

    def __isub__(self, other):
        return self.sub(other)

    def sub(self, other):
        """Recorder相减

        Args:
            other (Recorder): 另一个Recorder

        Returns:
            Recorder: 运算结果
        """
        if isinstance(other, Recorder):
            self.payload = self.operation2(other, (lambda x, y: x - y))
        return self

    def __mul__(self, other):
        return self.mul(other)

    def __imul__(self, other):
        return self.mul(other)

    def mul(self, other):
        """Recorder相乘

        Args:
            other (Recorder|int|float): 另一个Recorder或整数或浮点数

        Returns:
            Recorder: 运算结果
        """
        if isinstance(other, Recorder):
            self.payload = self.operation2(other, (lambda x, y: x * y))
        if isinstance(other, int) or isinstance(other, float):
            self.payload = self.operation1((lambda x, y: x * y), other)
        return self

    def __truediv__(self, other):
        return self.div(other)

    def __itruediv__(self, other):
        return self.div(other)

    def div(self, other):
        """Recorder相除法

        Args:
            other (Recorder|int|float): 另一个Recorder或整数或浮点数

        Returns:
            Recorder: 运算结果
        """
        if isinstance(other, Recorder):
            self.payload = self.operation2(other, (lambda x, y: x / y if y != 0 else 0))
        if isinstance(other, int) or isinstance(other, float):
            self.payload = self.operation1((lambda x, y: x / y if y != 0 else 0), other)
        return self

    def __mod__(self, other):
        return self.mod(other)

    def __imod__(self, other):
        return self.mod(other)

    def mod(self, other):
        """Recorder取模

        Args:
            other (Recorder|int|float): 另一个Recorder或整数或浮点数

        Returns:
            Recorder: 运算结果
        """
        if isinstance(other, Recorder):
            self.payload = self.operation2(other, (lambda x, y: x % y if y != 0 else 0))
        if isinstance(other, int) or isinstance(other, float):
            self.payload = self.operation1((lambda x, y: x % y if y != 0 else 0), other)
        return self

    def __pow__(self, other):
        return self.pow(other)

    def __ipow__(self, other):
        return self.pow(other)

    def pow(self, other):
        """Recorder乘方

        Args:
            other (int|float): 整数或浮点数

        Returns:
            Recorder: 运算结果
        """
        if isinstance(other, int) or isinstance(other, float):
            self.payload = self.operation1((lambda x, y: x ** y), other)
        return self

    def bin(self, other):
        """Recorder二值化

        Args:
            other (int|float): 整数或浮点数

        Returns:
            Recorder: 运算结果
        """
        if isinstance(other, int) or isinstance(other, float):
            self.payload = self.operation1((lambda x, y: 1 if x >= y else 0), other)
        return self

    def between(self, lower=None, upper=None):
        """Recoder区间限制

        Args:
            lower (Recorder|int|float, optional): 下限值. Defaults to None.
            upper (Recorder|int|float, optional): 上限值. Defaults to None.

        Returns:
            Recorder: 运算结果
        """
        if lower is not None:
            if isinstance(lower, Recorder):
                self.payload = self.operation2(lower, (lambda x, y: y if x < y else x))
            if isinstance(lower, int) or isinstance(lower, float):
                self.payload = self.operation1((lambda x, y: y if x < y else x), lower)
        if upper is not None:
            if isinstance(upper, Recorder):
                self.payload = self.operation2(upper, (lambda x, y: y if x > y else x))
            if isinstance(upper, int) or isinstance(upper, float):
                self.payload = self.operation1((lambda x, y: y if x > y else x), upper)
        return self

    def get_good_ratio(self):
        """获得比率越大越好的评分

        Returns:
            Recorder: 运算结果
        """
        self.payload = self.operation1(good_ratio)
        return self

    def get_bad_ratio(self):
        """获得比率越大越差的评分

        Returns:
            Recorder: 运算结果
        """
        self.payload = self.operation1(bad_ratio)
        return self

    def get_good_count(self):
        """获得数字越大越好的评分

        Returns:
            Recorder: 运算结果
        """
        self.payload = self.operation1(good_count)
        return self

    def get_bad_count(self):
        """获得数字越大越差的评分

        Returns:
            Recorder: 运算结果
        """
        self.payload = self.operation1(bad_count)
        return self
