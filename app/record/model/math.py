# -*- coding: utf-8 -*-

import math


def keep_decimals(number, k=4):
    """保留小数位数

    Args:
        number (float): 浮点数
        k (int, optional): 保留小数位数. Defaults to 4.

    Returns:
        float: 保留指定小数位数后的浮点数
    """
    d = pow(10, k)
    return int(number * d) / d


def good_ratio(number, k=math.e):
    """比率越大越好的评分

    Args:
        number (float): 比率，定义域：[0, 1]
        k (float, optional): 函数参数. Defaults to math.e.

    Returns:
        float: 评分

    Examples:
        >>> [good_ratio(i / 10) for i in range(11)]
        [0.5575751009228052, 0.6628787871942436, 0.7431186346865297, 0.8042602086765339, 0.8508491814476317, 0.8863492878763759, 0.9133998425783243, 0.9340119641546875, 0.9497180951586544, 0.9616859340926459, 0.9708052499008353]
    """
    return (1 - math.exp(-k * (number + 0.3))) if 0 <= number <= 1 else 0


def bad_ratio(number, k=math.e):
    """比率越大越差的评分

    Args:
        number (float): 比率，定义域：[0, 1]
        k (float, optional): 函数参数. Defaults to math.e.

    Returns:
        float: 评分

    Examples:
        >>> [bad_ratio(i / 10) for i in range(11)]
        [0.9708052499008353, 0.9616859340926459, 0.9497180951586544, 0.9340119641546875, 0.9133998425783243, 0.8863492878763759, 0.8508491814476317, 0.804260208676534, 0.7431186346865297, 0.6628787871942436, 0.5575751009228052]
    """
    return (1 - math.exp(-k * (1 - number + 0.3))) if 0 <= number <= 1 else 0


def good_count(number, k=math.e):
    """数字越大越好的评分

    Args:
        number (int): 数字，定义域：[0, inf)
        k (float, optional): 函数参数. Defaults to math.e.

    Returns:
        float: 评分

    Examples:
        >>> [good_count(10 ** i) for i in range(6)]
        [0.6490211037743331, 0.7540900718083384, 0.8373131623667941, 0.8812492807930405, 0.9067057334735344, 0.9231927938630402]
    """
    return math.exp(-1 / math.log(k * (k + number))) if number >= 0 else 0


def bad_count(number, k=math.e):
    """数字越小越好的评分

    Args:
        number (int): 数字，定义域：[0, inf)
        k (float, optional): 函数参数. Defaults to math.e.

    Returns:
        float: 评分

    Examples:
        >>> [bad_count(10 ** i) for i in range(6)]
        [0.9953846144680537, 0.6498895823869164, 0.4088404004220619, 0.2910806981091822, 0.22550901514878488, 0.1840161277577422]
    """
    return min(1 / math.log10(k * (k + number)), 1) if number >= 0 else 0
