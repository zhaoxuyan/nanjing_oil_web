# -*- coding: utf-8 -*-

from .base import Recorder
from .utils import get_date_time_str


class CountRecorder(Recorder):
    def __init__(self, name, payload={}):
        """初始化次数计数模型

        Args:
            name (str): 用于标识
            payload (dict, optional): 数据. Defaults to {}.
        """
        super().__init__(name, payload)

    def init(self, start, end, query_dao, default=0, **kwargs):
        """初始化次数统计结果

        Args:
            start (datetime): 开始时刻
            end (datetime): 结束时刻
            query_dao (function): 查询函数
            default (int, optional): 默认值. Defaults to 0.
        """
        start, end = super().init(start, end, default)
        for r in query_dao(start, end, **kwargs):
            timestamp = getattr(r, 'timestamp') if hasattr(r, 'timestamp') else getattr(r, 'start')
            date_str, time_str = get_date_time_str(timestamp)
            self.payload[date_str][time_str] += 1

    def update(self, start, count_dao, default=0, **kwargs):
        """更新次数统计结果

        Args:
            start (datetime): 开始时刻
            count_dao (function): 计数函数
            default (int, optional): 默认值. Defaults to 0.
        """
        date_str, time_str, start, end = super().update(start, default)
        self.payload[date_str][time_str] += count_dao(start, end, **kwargs)

    def __add__(self, other):
        return super().__add__(other)

    def __iadd__(self, other):
        return super().__iadd__(other)

    def __sub__(self, other):
        return super().__sub__(other)

    def __isub__(self, other):
        return super().__isub__(other)

    def __mul__(self, other):
        return super().__mul__(other)

    def __imul__(self, other):
        return super().__imul__(other)

    def __truediv__(self, other):
        return super().__truediv__(other)

    def __itruediv__(self, other):
        return super().__itruediv__(other)

    def __mod__(self, other):
        return super().__mod__(other)

    def __imod__(self, other):
        return super().__imod__(other)

    def __pow__(self, other):
        return super().__pow__(other)

    def __ipow__(self, other):
        return super().__ipow__(other)
