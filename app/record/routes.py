# -*- coding: utf-8 -*-

import shutil
import os.path as osp
from datetime import datetime, timedelta

from flask import redirect, url_for
from flask_login import login_required

from .. import write_required
from . import bp, table, RECORD_DAY_SPAN


@bp.route('/init/event_log', methods=['GET'])
@login_required
@write_required()
def init_event_logs():
    """初始化事件日志统计"""
    shutil.rmtree(osp.join('tmp', 'event_log'), ignore_errors=True)
    end = datetime.now()
    start = end - timedelta(days=RECORD_DAY_SPAN)
    table.init_event_log(start, end)
    return redirect(url_for('main.index'))


@bp.route('/init/state_log', methods=['GET'])
@login_required
@write_required()
def init_state_logs():
    """初始化状态日志统计"""
    shutil.rmtree(osp.join('tmp', 'state_log'), ignore_errors=True)
    end = datetime.now()
    start = end - timedelta(days=RECORD_DAY_SPAN)
    table.init_state_log(start, end)
    return redirect(url_for('main.index'))


@bp.route('/init/crash_log', methods=['GET'])
@login_required
@write_required()
def init_crash_logs():
    """初始化异常日志统计"""
    shutil.rmtree(osp.join('tmp', 'crash_log'), ignore_errors=True)
    end = datetime.now()
    start = end - timedelta(days=RECORD_DAY_SPAN)
    table.init_crash_log(start, end)
    return redirect(url_for('main.index'))
