# -*- coding: utf-8 -*-

import copy
import os.path as osp
from datetime import datetime, timedelta

from flask import current_app

from ... import db, scheduler
from ...beans import Scheduler
from ...utils import hour_start
from .. import TEMP_DIR
from ..dao import query_event_log, count_event_log, sum_event_log_level
from ..model import CountRecorder, SumRecorder
from . import category_codes, event_codes, levels


def init_event_log(start, end):
    with db.get_app().test_request_context():
        current_app.logger.info('start init event_log all')
        cr = CountRecorder('all')
        cr.init(start, end, query_event_log)
        cr.write(osp.join(TEMP_DIR, 'event_log/count'))
        sr = SumRecorder('all')
        sr.init(start, end, query_event_log, 'level')
        sr.write(osp.join(TEMP_DIR, 'event_log/sum'))

        current_app.logger.info('start init event_log levels')
        cr_not_0 = CountRecorder('1_2_3', copy.deepcopy(cr.payload))
        sr_not_0 = SumRecorder('1_2_3', copy.deepcopy(sr.payload))
        for i in levels:
            cr = CountRecorder(i)
            cr.init(start, end, query_event_log, level=i)
            cr.write(osp.join(TEMP_DIR, 'event_log/count/level'))
            sr = SumRecorder(i)
            sr.init(start, end, query_event_log, 'level', level=i)
            sr.write(osp.join(TEMP_DIR, 'event_log/sum/level'))
            if i == 0:
                cr_not_0 -= cr
                cr_not_0.write(osp.join(TEMP_DIR, osp.join('event_log/count/level')))
                sr_not_0 -= sr
                sr_not_0.write(osp.join(TEMP_DIR, osp.join('event_log/sum/level')))

        current_app.logger.info('start init event_log event_codes')
        for i in event_codes:
            cr = CountRecorder(i)
            cr.init(start, end, query_event_log, event_code=i)
            cr.write(osp.join(TEMP_DIR, 'event_log/count/event_code'))
            sr = SumRecorder(i)
            sr.init(start, end, query_event_log, 'level', event_code=i)
            sr.write(osp.join(TEMP_DIR, 'event_log/sum/event_code'))

        current_app.logger.info('start init event_log category_codes')
        for i in category_codes:
            cr = CountRecorder(i)
            cr.init(start, end, query_event_log, category_code=i)
            cr.write(osp.join(TEMP_DIR, 'event_log/count/category_code'))
            sr = SumRecorder(i)
            sr.init(start, end, query_event_log, 'level', category_code=i)
            sr.write(osp.join(TEMP_DIR, 'event_log/sum/category_code'))
            cr_not_0 = CountRecorder('1_2_3', copy.deepcopy(cr.payload))
            sr_not_0 = SumRecorder('1_2_3', copy.deepcopy(sr.payload))
            for j in event_codes:
                cr = CountRecorder(j)
                cr.init(start, end, query_event_log, category_code=i, event_code=j)
                cr.write(osp.join(TEMP_DIR, osp.join('event_log/count/category_code', str(i), 'event_code')))
                sr = SumRecorder(j)
                sr.init(start, end, query_event_log, 'level', category_code=i, event_code=j)
                sr.write(osp.join(TEMP_DIR, osp.join('event_log/sum/category_code', str(i), 'event_code')))
            for j in levels:
                cr = CountRecorder(j)
                cr.init(start, end, query_event_log, category_code=i, level=j)
                cr.write(osp.join(TEMP_DIR, osp.join('event_log/count/category_code', str(i), 'level')))
                sr = SumRecorder(j)
                sr.init(start, end, query_event_log, 'level', category_code=i, level=j)
                sr.write(osp.join(TEMP_DIR, osp.join('event_log/sum/category_code', str(i), 'level')))
                if j == 0:
                    cr_not_0 -= cr
                    cr_not_0.write(osp.join(TEMP_DIR, osp.join('event_log/count/category_code', str(i), 'level')))
                    sr_not_0 -= sr
                    sr_not_0.write(osp.join(TEMP_DIR, osp.join('event_log/sum/category_code', str(i), 'level')))

        current_app.logger.info('start init event_log channel_ids')
        channel_ids = [s.id for s in Scheduler.query.all()]
        for i in channel_ids:
            cr = CountRecorder(i)
            cr.init(start, end, query_event_log, channel_id=i)
            cr.write(osp.join(TEMP_DIR, 'event_log/count/channel_id'))
            sr = SumRecorder(i)
            sr.init(start, end, query_event_log, 'level', channel_id=i)
            sr.write(osp.join(TEMP_DIR, 'event_log/sum/channel_id'))
            cr_not_0 = CountRecorder('1_2_3', copy.deepcopy(cr.payload))
            sr_not_0 = SumRecorder('1_2_3', copy.deepcopy(sr.payload))
            for j in event_codes:
                cr = CountRecorder(j)
                cr.init(start, end, query_event_log, channel_id=i, event_code=j)
                cr.write(osp.join(TEMP_DIR, osp.join('event_log/count/channel_id', str(i), 'event_code')))
                sr = SumRecorder(j)
                sr.init(start, end, query_event_log, 'level', channel_id=i, event_code=j)
                sr.write(osp.join(TEMP_DIR, osp.join('event_log/sum/channel_id', str(i), 'event_code')))
            for j in levels:
                cr = CountRecorder(j)
                cr.init(start, end, query_event_log, channel_id=i, level=j)
                cr.write(osp.join(TEMP_DIR, osp.join('event_log/count/channel_id', str(i), 'level')))
                sr = SumRecorder(j)
                sr.init(start, end, query_event_log, 'level', channel_id=i, level=j)
                sr.write(osp.join(TEMP_DIR, osp.join('event_log/sum/channel_id', str(i), 'level')))
                if j == 0:
                    cr_not_0 -= cr
                    cr_not_0.write(osp.join(TEMP_DIR, osp.join('event_log/count/channel_id', str(i), 'level')))
                    sr_not_0 -= sr
                    sr_not_0.write(osp.join(TEMP_DIR, osp.join('event_log/sum/channel_id', str(i), 'level')))

        current_app.logger.info('finish init event_log')


def update_event_log():
    start = hour_start(datetime.now() - timedelta(minutes=1))
    with scheduler.app.app_context():
        current_app.logger.info('start update event_log all')
        cr = CountRecorder('all')
        cr.read(osp.join(TEMP_DIR, 'event_log/count'))
        cr.update(start, count_event_log)
        cr.write(osp.join(TEMP_DIR, 'event_log/count'))
        sr = SumRecorder('all')
        sr.read(osp.join(TEMP_DIR, 'event_log/sum'))
        sr.update(start, sum_event_log_level)
        sr.write(osp.join(TEMP_DIR, 'event_log/sum'))

        current_app.logger.info('start update event_log levels')
        cr_not_0 = CountRecorder('1_2_3', copy.deepcopy(cr.payload))
        sr_not_0 = SumRecorder('1_2_3', copy.deepcopy(sr.payload))
        for i in levels:
            cr = CountRecorder(i)
            cr.read(osp.join(TEMP_DIR, 'event_log/count/level'))
            cr.update(start, count_event_log, level=i)
            cr.write(osp.join(TEMP_DIR, 'event_log/count/level'))
            sr = SumRecorder(i)
            sr.read(osp.join(TEMP_DIR, 'event_log/sum/level'))
            sr.update(start, sum_event_log_level, level=i)
            sr.write(osp.join(TEMP_DIR, 'event_log/sum/level'))
            if i == 0:
                cr_not_0 -= cr
                cr_not_0.write(osp.join(TEMP_DIR, osp.join('event_log/count/level')))
                sr_not_0 -= sr
                sr_not_0.write(osp.join(TEMP_DIR, osp.join('event_log/sum/level')))

        current_app.logger.info('start update event_log event_codes')
        for i in event_codes:
            cr = CountRecorder(i)
            cr.read(osp.join(TEMP_DIR, 'event_log/count/event_code'))
            cr.update(start, count_event_log, event_code=i)
            cr.write(osp.join(TEMP_DIR, 'event_log/count/event_code'))
            sr = SumRecorder(i)
            sr.read(osp.join(TEMP_DIR, 'event_log/sum/event_code'))
            sr.update(start, sum_event_log_level, event_code=i)
            sr.write(osp.join(TEMP_DIR, 'event_log/sum/event_code'))

        current_app.logger.info('start update event_log category_codes')
        for i in category_codes:
            cr = CountRecorder(i)
            cr.read(osp.join(TEMP_DIR, 'event_log/count/category_code'))
            cr.update(start, count_event_log, category_code=i)
            cr.write(osp.join(TEMP_DIR, 'event_log/count/category_code'))
            sr = SumRecorder(i)
            sr.read(osp.join(TEMP_DIR, 'event_log/sum/category_code'))
            sr.update(start, sum_event_log_level, category_code=i)
            sr.write(osp.join(TEMP_DIR, 'event_log/sum/category_code'))
            cr_not_0 = CountRecorder('1_2_3', copy.deepcopy(cr.payload))
            sr_not_0 = SumRecorder('1_2_3', copy.deepcopy(sr.payload))
            for j in event_codes:
                cr = CountRecorder(j)
                cr.read(osp.join(TEMP_DIR, osp.join('event_log/count/category_code', str(i), 'event_code')))
                cr.update(start, count_event_log, category_code=i, event_code=j)
                cr.write(osp.join(TEMP_DIR, osp.join('event_log/count/category_code', str(i), 'event_code')))
                sr = SumRecorder(j)
                sr.read(osp.join(TEMP_DIR, osp.join('event_log/sum/category_code', str(i), 'event_code')))
                sr.update(start, sum_event_log_level, category_code=i, event_code=j)
                sr.write(osp.join(TEMP_DIR, osp.join('event_log/sum/category_code', str(i), 'event_code')))
            for j in levels:
                cr = CountRecorder(j)
                cr.read(osp.join(TEMP_DIR, osp.join('event_log/count/category_code', str(i), 'level')))
                cr.update(start, count_event_log, category_code=i, level=j)
                cr.write(osp.join(TEMP_DIR, osp.join('event_log/count/category_code', str(i), 'level')))
                sr = SumRecorder(j)
                sr.read(osp.join(TEMP_DIR, osp.join('event_log/sum/category_code', str(i), 'level')))
                sr.update(start, sum_event_log_level, category_code=i, level=j)
                sr.write(osp.join(TEMP_DIR, osp.join('event_log/sum/category_code', str(i), 'level')))
                if j == 0:
                    cr_not_0 -= cr
                    cr_not_0.write(osp.join(TEMP_DIR, osp.join('event_log/count/category_code', str(i), 'level')))
                    sr_not_0 -= sr
                    sr_not_0.write(osp.join(TEMP_DIR, osp.join('event_log/sum/category_code', str(i), 'level')))

        current_app.logger.info('start update event_log channel_ids')
        channel_ids = [s.id for s in Scheduler.query.all()]
        for i in channel_ids:
            cr = CountRecorder(i)
            cr.read(osp.join(TEMP_DIR, 'event_log/count/channel_id'))
            cr.update(start, count_event_log, channel_id=i)
            cr.write(osp.join(TEMP_DIR, 'event_log/count/channel_id'))
            sr = SumRecorder(i)
            sr.read(osp.join(TEMP_DIR, 'event_log/sum/channel_id'))
            sr.update(start, sum_event_log_level, channel_id=i)
            sr.write(osp.join(TEMP_DIR, 'event_log/sum/channel_id'))
            cr_not_0 = CountRecorder('1_2_3', copy.deepcopy(cr.payload))
            sr_not_0 = SumRecorder('1_2_3', copy.deepcopy(sr.payload))
            for j in event_codes:
                cr = CountRecorder(j)
                cr.read(osp.join(TEMP_DIR, osp.join('event_log/count/channel_id', str(i), 'event_code')))
                cr.update(start, count_event_log, channel_id=i, event_code=j)
                cr.write(osp.join(TEMP_DIR, osp.join('event_log/count/channel_id', str(i), 'event_code')))
                sr = SumRecorder(j)
                sr.read(osp.join(TEMP_DIR, osp.join('event_log/sum/channel_id', str(i), 'event_code')))
                sr.update(start, sum_event_log_level, channel_id=i, event_code=j)
                sr.write(osp.join(TEMP_DIR, osp.join('event_log/sum/channel_id', str(i), 'event_code')))
            for j in levels:
                cr = CountRecorder(j)
                cr.read(osp.join(TEMP_DIR, osp.join('event_log/count/channel_id', str(i), 'level')))
                cr.update(start, count_event_log, channel_id=i, level=j)
                cr.write(osp.join(TEMP_DIR, osp.join('event_log/count/channel_id', str(i), 'level')))
                sr = SumRecorder(j)
                sr.read(osp.join(TEMP_DIR, osp.join('event_log/sum/channel_id', str(i), 'level')))
                sr.update(start, sum_event_log_level, channel_id=i, level=j)
                sr.write(osp.join(TEMP_DIR, osp.join('event_log/sum/channel_id', str(i), 'level')))
                if j == 0:
                    cr_not_0 -= cr
                    cr_not_0.write(osp.join(TEMP_DIR, osp.join('event_log/count/channel_id', str(i), 'level')))
                    sr_not_0 -= sr
                    sr_not_0.write(osp.join(TEMP_DIR, osp.join('event_log/sum/channel_id', str(i), 'level')))

        current_app.logger.info('finish update event_log')
