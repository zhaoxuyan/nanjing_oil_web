# -*- coding: utf-8 -*-

import os.path as osp
from datetime import datetime, timedelta

from flask import current_app

from ... import db, scheduler
from ...beans import Scheduler
from ...utils import hour_start
from .. import TEMP_DIR
from ..dao import query_crash_log, count_crash_log
from ..model import CountRecorder
from . import crash_codes


def init_crash_log(start, end):
    with db.get_app().test_request_context():
        current_app.logger.info('start init crash_log all')
        cr = CountRecorder('all')
        cr.init(start, end, query_crash_log)
        cr.write(osp.join(TEMP_DIR, 'crash_log/count'))

        current_app.logger.info('start init crash_log crash_codes')
        for i in crash_codes:
            cr = CountRecorder(i)
            cr.init(start, end, query_crash_log, crash_code=i)
            cr.write(osp.join(TEMP_DIR, 'crash_log/count/crash_code'))

        current_app.logger.info('start init crash_log channel_ids')
        channel_ids = [s.id for s in Scheduler.query.all()]
        for i in channel_ids:
            cr = CountRecorder(i)
            cr.init(start, end, query_crash_log, channel_id=i)
            cr.write(osp.join(TEMP_DIR, 'crash_log/count/channel_id'))
            for j in crash_codes:
                cr = CountRecorder(j)
                cr.init(start, end, query_crash_log, channel_id=i, crash_code=j)
                cr.write(osp.join(TEMP_DIR, osp.join('crash_log/count/channel_id', str(i), 'crash_code')))

        current_app.logger.info('finish init crash_log')


def update_crash_log():
    start = hour_start(datetime.now() - timedelta(minutes=1))
    with scheduler.app.app_context():
        current_app.logger.info('start update crash_log all')
        cr = CountRecorder('all')
        cr.read(osp.join(TEMP_DIR, 'crash_log/count'))
        cr.update(start, count_crash_log)
        cr.write(osp.join(TEMP_DIR, 'crash_log/count'))

        current_app.logger.info('start update crash_log crash_codes')
        for i in crash_codes:
            cr = CountRecorder(i)
            cr.read(osp.join(TEMP_DIR, 'crash_log/count/crash_code'))
            cr.update(start, count_crash_log, crash_code=i)
            cr.write(osp.join(TEMP_DIR, 'crash_log/count/crash_code'))

        current_app.logger.info('start update crash_log channel_ids')
        channel_ids = [s.id for s in Scheduler.query.all()]
        for i in channel_ids:
            cr = CountRecorder(i)
            cr.read(osp.join(TEMP_DIR, 'crash_log/count/channel_id'))
            cr.update(start, count_crash_log, channel_id=i)
            cr.write(osp.join(TEMP_DIR, 'crash_log/count/channel_id'))
            for j in crash_codes:
                cr = CountRecorder(j)
                cr.read(osp.join(TEMP_DIR, osp.join('crash_log/count/channel_id', str(i), 'crash_code')))
                cr.update(start, count_crash_log, channel_id=i, crash_code=j)
                cr.write(osp.join(TEMP_DIR, osp.join('crash_log/count/channel_id', str(i), 'crash_code')))

        current_app.logger.info('finish update crash_log')
