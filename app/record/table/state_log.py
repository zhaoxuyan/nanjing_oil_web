# -*- coding: utf-8 -*-

import os.path as osp
from datetime import datetime, timedelta

from flask import current_app

from ... import db, scheduler
from ...beans import Scheduler
from ...utils import hour_start
from .. import TEMP_DIR
from ..dao import query_state_log, count_state_log, sum_state_log_seconds
from ..model import CountRecorder, SumRecorder
from . import category_codes, state_codes


def init_state_log(start, end):
    with db.get_app().test_request_context():
        current_app.logger.info('start init state_log all')
        cr = CountRecorder('all')
        cr.init(start, end, query_state_log)
        cr.write(osp.join(TEMP_DIR, 'state_log/count'))
        sr = SumRecorder('all')
        sr.init(start, end, query_state_log, 'seconds')
        sr.write(osp.join(TEMP_DIR, 'state_log/sum'))

        current_app.logger.info('start init state_log state_codes')
        for i in state_codes:
            cr = CountRecorder(i)
            cr.init(start, end, query_state_log, state_code=i)
            cr.write(osp.join(TEMP_DIR, 'state_log/count/state_code'))
            sr = SumRecorder(i)
            sr.init(start, end, query_state_log, 'seconds', state_code=i)
            sr.write(osp.join(TEMP_DIR, 'state_log/sum/state_code'))

        current_app.logger.info('start init state_log category_codes')
        for i in category_codes:
            cr = CountRecorder(i)
            cr.init(start, end, query_state_log, category_code=i)
            cr.write(osp.join(TEMP_DIR, 'state_log/count/category_code'))
            sr = SumRecorder(i)
            sr.init(start, end, query_state_log, 'seconds', category_code=i)
            sr.write(osp.join(TEMP_DIR, 'state_log/sum/category_code'))
            for j in state_codes:
                cr = CountRecorder(j)
                cr.init(start, end, query_state_log, category_code=i, state_code=j)
                cr.write(osp.join(TEMP_DIR, osp.join('state_log/count/category_code', str(i), 'state_code')))
                sr = SumRecorder(j)
                sr.init(start, end, query_state_log, 'seconds', category_code=i, state_code=j)
                sr.write(osp.join(TEMP_DIR, osp.join('state_log/sum/category_code', str(i), 'state_code')))

        current_app.logger.info('start init state_log channel_ids')
        channel_ids = [s.id for s in Scheduler.query.all()]
        for i in channel_ids:
            cr = CountRecorder(i)
            cr.init(start, end, query_state_log, channel_id=i)
            cr.write(osp.join(TEMP_DIR, 'state_log/count/channel_id'))
            sr = SumRecorder(i)
            sr.init(start, end, query_state_log, 'seconds', channel_id=i)
            sr.write(osp.join(TEMP_DIR, 'state_log/sum/channel_id'))
            for j in state_codes:
                cr = CountRecorder(j)
                cr.init(start, end, query_state_log, channel_id=i, state_code=j)
                cr.write(osp.join(TEMP_DIR, osp.join('state_log/count/channel_id', str(i), 'state_code')))
                sr = SumRecorder(j)
                sr.init(start, end, query_state_log, 'seconds', channel_id=i, state_code=j)
                sr.write(osp.join(TEMP_DIR, osp.join('state_log/sum/channel_id', str(i), 'state_code')))

        current_app.logger.info('finish init state_log')


def update_state_log():
    start = hour_start(datetime.now() - timedelta(minutes=1))
    with scheduler.app.app_context():
        current_app.logger.info('start update state_log all')
        cr = CountRecorder('all')
        cr.read(osp.join(TEMP_DIR, 'state_log/count'))
        cr.update(start, count_state_log)
        cr.write(osp.join(TEMP_DIR, 'state_log/count'))
        sr = SumRecorder('all')
        sr.read(osp.join(TEMP_DIR, 'state_log/sum'))
        sr.update(start, sum_state_log_seconds)
        sr.write(osp.join(TEMP_DIR, 'state_log/sum'))

        current_app.logger.info('start update state_log state_codes')
        for i in state_codes:
            cr = CountRecorder(i)
            cr.read(osp.join(TEMP_DIR, 'state_log/count/state_code'))
            cr.update(start, count_state_log, state_code=i)
            cr.write(osp.join(TEMP_DIR, 'state_log/count/state_code'))
            sr = SumRecorder(i)
            sr.read(osp.join(TEMP_DIR, 'state_log/sum/state_code'))
            sr.update(start, sum_state_log_seconds, state_code=i)
            sr.write(osp.join(TEMP_DIR, 'state_log/sum/state_code'))

        current_app.logger.info('start update state_log category_codes')
        for i in category_codes:
            cr = CountRecorder(i)
            cr.read(osp.join(TEMP_DIR, 'state_log/count/category_code'))
            cr.update(start, count_state_log, category_code=i)
            cr.write(osp.join(TEMP_DIR, 'state_log/count/category_code'))
            sr = SumRecorder(i)
            sr.read(osp.join(TEMP_DIR, 'state_log/sum/category_code'))
            sr.update(start, sum_state_log_seconds, category_code=i)
            sr.write(osp.join(TEMP_DIR, 'state_log/sum/category_code'))
            for j in state_codes:
                cr = CountRecorder(j)
                cr.read(osp.join(TEMP_DIR, osp.join('state_log/count/category_code', str(i), 'state_code')))
                cr.update(start, count_state_log,  category_code=i, state_code=j)
                cr.write(osp.join(TEMP_DIR, osp.join('state_log/count/category_code', str(i), 'state_code')))
                sr = SumRecorder(j)
                sr.read(osp.join(TEMP_DIR, osp.join('state_log/sum/category_code', str(i), 'state_code')))
                sr.update(start, sum_state_log_seconds, category_code=i, state_code=j)
                sr.write(osp.join(TEMP_DIR, osp.join('state_log/sum/category_code', str(i), 'state_code')))

        current_app.logger.info('start update state_log channel_ids')
        channel_ids = [s.id for s in Scheduler.query.all()]
        for i in channel_ids:
            cr = CountRecorder(i)
            cr.read(osp.join(TEMP_DIR, 'state_log/count/channel_id'))
            cr.update(start, count_state_log, channel_id=i)
            cr.write(osp.join(TEMP_DIR, 'state_log/count/channel_id'))
            sr = SumRecorder(i)
            sr.read(osp.join(TEMP_DIR, 'state_log/sum/channel_id'))
            sr.update(start, sum_state_log_seconds, channel_id=i)
            sr.write(osp.join(TEMP_DIR, 'state_log/sum/channel_id'))
            for j in state_codes:
                cr = CountRecorder(j)
                cr.read(osp.join(TEMP_DIR, osp.join('state_log/count/channel_id', str(i), 'state_code')))
                cr.update(start, count_state_log,  channel_id=i, state_code=j)
                cr.write(osp.join(TEMP_DIR, osp.join('state_log/count/channel_id', str(i), 'state_code')))
                sr = SumRecorder(j)
                sr.read(osp.join(TEMP_DIR, osp.join('state_log/sum/channel_id', str(i), 'state_code')))
                sr.update(start, sum_state_log_seconds, channel_id=i, state_code=j)
                sr.write(osp.join(TEMP_DIR, osp.join('state_log/sum/channel_id', str(i), 'state_code')))

        current_app.logger.info('finish update state_log')
