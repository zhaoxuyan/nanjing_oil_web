# -*- coding: utf-8 -*-

from ...beans import LEVELS  # NOQA
levels = [i for i in LEVELS]

from ...utils import read_json  # NOQA
payload = read_json('data/beans/metadata.json')
category_codes = [item.get('code') for item in payload.get('categories')]
event_codes = [item.get('code') for item in payload.get('events')]
state_codes = [item.get('code') for item in payload.get('states')]
crash_codes = [item.get('code') for item in payload.get('crashes')]

from .event_log import init_event_log, update_event_log  # NOQA
from .state_log import init_state_log, update_state_log  # NOQA
from .crash_log import init_crash_log, update_crash_log  # NOQA
