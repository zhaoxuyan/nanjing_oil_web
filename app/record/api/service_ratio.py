# -*- coding: utf-8 -*-

import os.path as osp
from datetime import datetime, timedelta

from flask import request, jsonify
from flask_babel import lazy_gettext as _l

from ... import read_required
from ...api import token_auth, bad_request
from ...utils import read_json, date_str_2_dt, day_start, DATE_FORMATTER
from .. import TEMP_DIR
from ..model import CountRecorder, SumRecorder, keep_decimals
from . import bp


@bp.route('/service_number', methods=['GET'])
@token_auth.login_required
@read_required()
def get_service_number():
    """获取服务人次

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 record_api 模块有 R 权限。

    ## 查询字段

    | 字段 | 是否必须 | 类型 | 说明                                        |
    | ---- | -------- | ---- | ------------------------------------------- |
    | date |          | str  | 形如：`YYYY-mm-dd`，默认：当天              |
    | span |          | str  | 日期跨度，大于等于 0 且不超过 31，默认：0   |
    | sum  |          | int  | 是否求和，可选值为：0，1，默认：0（不求和） |

    ## 响应示例

    ```json
    {
        "00:00:00": 0.0,
        "01:00:00": 0.0,
        "02:00:00": 0.0,
        "03:00:00": 0.0,
        "04:00:00": 0.0,
        "05:00:00": 0.0,
        "06:00:00": 0.0,
        "07:00:00": 0.0,
        "08:00:00": 0.0,
        "09:00:00": 0.0,
        "10:00:00": 0.0,
        "11:00:00": 0.0,
        "12:00:00": 0.0,
        "13:00:00": 0.0,
        "14:00:00": 0.0,
        "15:00:00": 0.0,
        "16:00:00": 0.0,
        "17:00:00": 0.0,
        "18:00:00": 0.0,
        "19:00:00": 0.0,
        "20:00:00": 0.0,
        "21:00:00": 0.0,
        "22:00:00": 0.0,
        "23:00:00": 0.0
    }
    ```

    ```json
    {
        "2020-09-01": 0.0,
        "2020-09-02": 0.0,
        "2020-09-03": 0.0,
        "2020-09-04": 0.0,
        "2020-09-05": 0.0,
        "2020-09-06": 0.0
    }
    ```

    ```json
    {
        "sum": 0.0
    }
    ```

    @@@
    """
    cr1 = CountRecorder(200001)
    cr1.read(osp.join(TEMP_DIR, osp.join('state_log/count/channel_id', str(1), 'state_code')))
    cr2 = CountRecorder(100015)
    cr2.read(osp.join(TEMP_DIR, 'event_log/count/event_code'))
    payload = cr1.between(lower=None, upper=cr2).payload

    date_str = request.args.get('date', None, type=str)
    day_span = request.args.get('span', 0, type=int)
    is_sum = request.args.get('sum', 0, type=int)

    dt = None
    if date_str:
        dt = date_str_2_dt(date_str)
    if dt is None:
        dt = day_start(datetime.now())

    result_json = {}
    if day_span == 0:
        result_json = payload.get(dt.strftime(DATE_FORMATTER)) or {}
    elif day_span - 0:
        day_span = 31 if day_span - 31 > 0 else day_span
        for i in range(day_span):
            key = dt.strftime(DATE_FORMATTER)
            result_json[key] = sum((payload.get(key) or {}).values())
            dt -= timedelta(days=1)
    else:
        return bad_request(_l('%(obj)s is invalid!', obj=day_span))

    if is_sum:
        result_json = {'sum': sum(result_json.values())}
    return jsonify(result_json)


@bp.route('/entering_number', methods=['GET'])
@token_auth.login_required
@read_required()
def get_entering_number():
    """获取进店人次

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 record_api 模块有 R 权限。

    ## 查询字段

    | 字段 | 是否必须 | 类型 | 说明                                        |
    | ---- | -------- | ---- | ------------------------------------------- |
    | date |          | str  | 形如：`YYYY-mm-dd`，默认：当天              |
    | span |          | str  | 日期跨度，大于等于 0 且不超过 31，默认：0   |
    | sum  |          | int  | 是否求和，可选值为：0，1，默认：0（不求和） |

    ## 响应示例

    ```json
    {
        "00:00:00": 170,
        "01:00:00": 154,
        "02:00:00": 134,
        "03:00:00": 155,
        "04:00:00": 176,
        "05:00:00": 167,
        "06:00:00": 166,
        "07:00:00": 177,
        "08:00:00": 146,
        "09:00:00": 133,
        "10:00:00": 159,
        "11:00:00": 164,
        "12:00:00": 175,
        "13:00:00": 168,
        "14:00:00": 175,
        "15:00:00": 153,
        "16:00:00": 7,
        "17:00:00": 0,
        "18:00:00": 0,
        "19:00:00": 0,
        "20:00:00": 0,
        "21:00:00": 0,
        "22:00:00": 0,
        "23:00:00": 0
    }
    ```

    ```json
    {
        "2020-08-31": 27,
        "2020-09-01": 2240,
        "2020-09-02": 3921,
        "2020-09-03": 3883,
        "2020-09-04": 3927,
        "2020-09-05": 2579
    }
    ```

    ```json
    {
        "sum": 16577
    }
    ```

    @@@
    """
    cr2 = CountRecorder(100015)
    cr2.read(osp.join(TEMP_DIR, 'event_log/count/event_code'))
    payload = cr2.payload

    date_str = request.args.get('date', None, type=str)
    day_span = request.args.get('span', 0, type=int)
    is_sum = request.args.get('sum', 0, type=int)

    dt = None
    if date_str:
        dt = date_str_2_dt(date_str)
    if dt is None:
        dt = day_start(datetime.now())

    result_json = {}
    if day_span == 0:
        result_json = payload.get(dt.strftime(DATE_FORMATTER)) or {}
    elif day_span - 0:
        day_span = 31 if day_span - 31 > 0 else day_span
        for i in range(day_span):
            key = dt.strftime(DATE_FORMATTER)
            result_json[key] = sum((payload.get(key) or {}).values())
            dt -= timedelta(days=1)
    else:
        return bad_request(_l('%(obj)s is invalid!', obj=day_span))

    if is_sum:
        result_json = {'sum': sum(result_json.values())}
    return jsonify(result_json)


@bp.route('/service_ratio', methods=['GET'])
@token_auth.login_required
@read_required()
def get_service_ratio():
    """获取进店服务率

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 record_api 模块有 R 权限。

    ## 查询字段

    | 字段 | 是否必须 | 类型 | 说明                                        |
    | ---- | -------- | ---- | ------------------------------------------- |
    | date |          | str  | 形如：`YYYY-mm-dd`，默认：当天              |
    | span |          | str  | 日期跨度，大于等于 0 且不超过 31，默认：0   |
    | avg  |          | int  | 是否平均，可选值为：0，1，默认：0（不平均） |

    ## 响应示例

    ```json
    {
        "00:00:00": 0.0,
        "01:00:00": 0.0,
        "02:00:00": 0.0,
        "03:00:00": 0.0,
        "04:00:00": 0.0,
        "05:00:00": 0.0,
        "06:00:00": 0.0,
        "07:00:00": 0.0,
        "08:00:00": 0.0,
        "09:00:00": 0.0,
        "10:00:00": 0.0,
        "11:00:00": 0.0,
        "12:00:00": 0.0,
        "13:00:00": 0.0,
        "14:00:00": 0.0,
        "15:00:00": 0.0,
        "16:00:00": 0.0,
        "17:00:00": 0.0,
        "18:00:00": 0.0,
        "19:00:00": 0.0,
        "20:00:00": 0.0,
        "21:00:00": 0.0,
        "22:00:00": 0.0,
        "23:00:00": 0.0
    }
    ```

    ```json
    {
        "2020-09-01": 0.0,
        "2020-09-02": 0.0,
        "2020-09-03": 0.0,
        "2020-09-04": 0.0,
        "2020-09-05": 0.0,
        "2020-09-06": 0.0
    }
    ```

    ```json
    {
        "avg": 0.0
    }
    ```

    @@@
    """
    cr1 = CountRecorder(200001)
    cr1.read(osp.join(TEMP_DIR, osp.join('state_log/count/channel_id', str(1), 'state_code')))
    cr2 = CountRecorder(100015)
    cr2.read(osp.join(TEMP_DIR, 'event_log/count/event_code'))
    payload = (cr1 / cr2).between(0, 1).payload

    date_str = request.args.get('date', None, type=str)
    day_span = request.args.get('span', 0, type=int)
    is_avg = request.args.get('avg', 0, type=int)

    dt = None
    if date_str:
        dt = date_str_2_dt(date_str)
    if dt is None:
        dt = day_start(datetime.now())

    result_json = {}
    if day_span == 0:
        result_json = payload.get(dt.strftime(DATE_FORMATTER)) or {}
    elif day_span - 0:
        day_span = 31 if day_span - 31 > 0 else day_span
        for i in range(day_span):
            key = dt.strftime(DATE_FORMATTER)
            result_json[key] = keep_decimals(sum((payload.get(key) or {}).values()) / (len(payload.get(key) or {}) or 1))
            dt -= timedelta(days=1)
    else:
        return bad_request(_l('%(obj)s is invalid!', obj=day_span))

    if is_avg:
        result_json = {'avg': keep_decimals(sum(result_json.values()) / (len(result_json) or 1))}
    return jsonify(result_json)


@bp.route('/service_avg_time', methods=['GET'])
@token_auth.login_required
@read_required()
def get_service_avg_time():
    """获取平均服务时间

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 record_api 模块有 R 权限。

    ## 查询字段

    | 字段 | 是否必须 | 类型 | 说明                                        |
    | ---- | -------- | ---- | ------------------------------------------- |
    | date |          | str  | 形如：`YYYY-mm-dd`，默认：当天              |
    | span |          | str  | 日期跨度，大于等于 0 且不超过 31，默认：0   |
    | avg  |          | int  | 是否平均，可选值为：0，1，默认：0（不平均） |

    ## 响应示例

    ```json
    {
        "00:00:00": 56.1315,
        "01:00:00": 62.468,
        "02:00:00": 56.9487,
        "03:00:00": 57.4583,
        "04:00:00": 60.775,
        "05:00:00": 47.3529,
        "06:00:00": 65.9285,
        "07:00:00": 50.9,
        "08:00:00": 68.1395,
        "09:00:00": 62.9729,
        "10:00:00": 56.4444,
        "11:00:00": 67.3142,
        "12:00:00": 59.5555,
        "13:00:00": 56.98,
        "14:00:00": 64.2285,
        "15:00:00": 59.4473,
        "16:00:00": 70.4637,
        "17:00:00": 68.2409,
        "18:00:00": 72.3069,
        "19:00:00": 71.7127,
        "20:00:00": 70.8105,
        "21:00:00": 62.7558,
        "22:00:00": 68.2166,
        "23:00:00": 0.0
    }
    ```

    ```json
    {
        "2020-09-02": 0.0,
        "2020-09-03": 0.0,
        "2020-09-04": 0.0,
        "2020-09-05": 0.0,
        "2020-09-06": 61.5780,
        "2020-09-07": 59.8980
    }
    ```

    ```json
    {
        "avg": 20.246
    }
    ```

    @@@
    """
    cr = CountRecorder('all')
    cr.read(osp.join(TEMP_DIR, 'state_log/count'))
    sr = SumRecorder('all')
    sr.read(osp.join(TEMP_DIR, 'state_log/sum'))
    payload = (sr / cr).payload

    date_str = request.args.get('date', None, type=str)
    day_span = request.args.get('span', 0, type=int)
    is_avg = request.args.get('avg', 0, type=int)

    dt = None
    if date_str:
        dt = date_str_2_dt(date_str)
    if dt is None:
        dt = day_start(datetime.now())

    result_json = {}
    if day_span == 0:
        result_json = payload.get(dt.strftime(DATE_FORMATTER)) or {}
    elif day_span - 0:
        day_span = 31 if day_span - 31 > 0 else day_span
        for i in range(day_span):
            key = dt.strftime(DATE_FORMATTER)
            result_json[key] = keep_decimals(sum((payload.get(key) or {}).values()) / (len(payload.get(key) or {}) or 1))
            dt -= timedelta(days=1)
    else:
        return bad_request(_l('%(obj)s is invalid!', obj=day_span))

    if is_avg:
        result_json = {'avg': keep_decimals(sum(result_json.values()) / (len(result_json) or 1))}
    return jsonify(result_json)
