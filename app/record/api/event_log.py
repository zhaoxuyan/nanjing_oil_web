# -*- coding: utf-8 -*-

import os.path as osp
from datetime import datetime, timedelta

from flask import request, jsonify
from flask_babel import lazy_gettext as _l

from ... import read_required
from ...api import token_auth, bad_request, not_found
from ...beans import Scheduler
from ...utils import read_json, date_str_2_dt, day_start, DATE_FORMATTER
from .. import TEMP_DIR, RECODER_MODELS
from . import bp, category_codes, event_codes, levels


@bp.route('/event_log/<string:model>', methods=['GET'])
@token_auth.login_required
@read_required()
def get_event_log(model):
    """获取事件日志统计

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 record_api 模块有 R 权限。

    ## 查询字段

    | 字段          | 是否必须 | 类型 | 说明                                        |
    | ------------- | -------- | ---- | ------------------------------------------- |
    | model         |          | str  | 可选值：`count`, `sum`                      |
    | category_code |          | int  | 参考 `/bean/api/category` 接口              |
    | channel_id    |          | int  | 参考 `/config/api/scheduler` 接口           |
    | event_code    |          | int  | 参考 `/bean/api/event` 接口                 |
    | level         |          | str  | 取值可选：`0`, `1`, `2`, `3`, `1_2_3`       |
    | date          |          | str  | 形如：`YYYY-mm-dd`，默认：当天              |
    | span          |          | str  | 日期跨度，大于等于 0 且不超过 31，默认：0   |
    | sum           |          | int  | 是否求和，可选值为：0，1，默认：0（不求和） |

    - /record/api/event_log/count
    - /record/api/event_log/count?category_code=
    - /record/api/event_log/count?category_code=&event_code=
    - /record/api/event_log/count?category_code=&level=
    - /record/api/event_log/count?channel_id=
    - /record/api/event_log/count?channel_id=&event_code=
    - /record/api/event_log/count?channel_id=&level=
    - /record/api/event_log/count?event_code=
    - /record/api/event_log/count?level=
    - /record/api/event_log/sum
    - /record/api/event_log/sum?category_code=
    - /record/api/event_log/sum?category_code=&event_code=
    - /record/api/event_log/sum?category_code=&level=
    - /record/api/event_log/sum?channel_id=
    - /record/api/event_log/sum?channel_id=&event_code=
    - /record/api/event_log/sum?channel_id=&level=
    - /record/api/event_log/sum?event_code=
    - /record/api/event_log/sum?level=

    ## 响应示例

    ```json
    {
        "00:00:00": 0,
        "01:00:00": 0,
        "02:00:00": 0,
        "03:00:00": 0,
        "04:00:00": 0,
        "05:00:00": 0,
        "06:00:00": 0,
        "07:00:00": 0,
        "08:00:00": 0,
        "09:00:00": 0,
        "10:00:00": 0,
        "11:00:00": 0,
        "12:00:00": 0,
        "13:00:00": 0,
        "14:00:00": 0,
        "15:00:00": 0,
        "16:00:00": 0,
        "17:00:00": 61,
        "18:00:00": 200,
        "19:00:00": 128,
        "20:00:00": 158,
        "21:00:00": 19,
        "22:00:00": 5,
        "23:00:00": 7
    }
    ```

    ```json
    {
        "2020-08-22": 0,
        "2020-08-23": 0,
        "2020-08-24": 578,
        "2020-08-25": 76,
        "2020-08-26": 22,
        "2020-08-27": 28,
        "2020-08-28": 24
    }
    ```

    ```json
    {
        "sum": 728
    }
    ```

    @@@
    """
    if model not in RECODER_MODELS:
        return bad_request(_l('%(obj)s is invalid!', obj=model))
    filename = osp.join(TEMP_DIR, 'event_log', model)

    category_code = request.args.get('category_code', None, type=int)
    channel_id = request.args.get('channel_id', None, type=int)
    event_code = request.args.get('event_code', None, type=int)
    level = request.args.get('level', None, type=str)

    if category_code:
        if category_code not in category_codes:
            return not_found(_l('%(obj)s does not exists!', obj=category_code))
    if channel_id:
        channel_ids = [s.id for s in Scheduler.query.all()]
        if channel_id not in channel_ids:
            return not_found(_l('%(obj)s does not exists!', obj=channel_id))
    if event_code:
        if event_code not in event_codes:
            return not_found(_l('%(obj)s does not exists!', obj=event_code))
    if level:
        if level not in levels:
            return not_found(_l('%(obj)s does not exists!', obj=level))

    if category_code:
        if category_code not in category_codes:
            return not_found(_l('%(obj)s does not exists!', obj=category_code))
        if event_code:
            filename = osp.join(filename, 'category_code', str(category_code))
            filename = osp.join(filename, 'event_code', '.'.join([str(event_code), 'json']))
        elif level:
            filename = osp.join(filename, 'category_code', str(category_code))
            filename = osp.join(filename, 'level', '.'.join([str(level), 'json']))
        else:
            filename = osp.join(filename, 'category_code', '.'.join([str(category_code), 'json']))
    elif channel_id:
        if event_code:
            filename = osp.join(filename, 'channel_id', str(channel_id))
            filename = osp.join(filename, 'event_code', '.'.join([str(event_code), 'json']))
        elif level:
            filename = osp.join(filename, 'channel_id', str(channel_id))
            filename = osp.join(filename, 'level', '.'.join([str(level), 'json']))
        else:
            filename = osp.join(filename, 'channel_id', '.'.join([str(channel_id), 'json']))
    elif event_code:
        filename = osp.join(filename, 'event_code', '.'.join([str(event_code), 'json']))
    elif level:
        filename = osp.join(filename, 'level', '.'.join([str(level), 'json']))
    else:
        filename = osp.join(filename, '.'.join(['all', 'json']))
    payload = read_json(filename)

    date_str = request.args.get('date', None, type=str)
    day_span = request.args.get('span', 0, type=int)
    is_sum = request.args.get('sum', 0, type=int)

    dt = None
    if date_str:
        dt = date_str_2_dt(date_str)
    if dt is None:
        dt = day_start(datetime.now())

    result_json = {}
    if day_span == 0:
        result_json = payload.get(dt.strftime(DATE_FORMATTER)) or {}
    elif day_span - 0:
        day_span = 31 if day_span - 31 > 0 else day_span
        for i in range(day_span):
            key = dt.strftime(DATE_FORMATTER)
            result_json[key] = sum((payload.get(key) or {}).values())
            dt -= timedelta(days=1)
    else:
        return bad_request(_l('%(obj)s is invalid!', obj=day_span))

    if is_sum:
        result_json = {'sum': sum(result_json.values())}
    return jsonify(result_json)
