# -*- coding: utf-8 -*-

from flask import Blueprint
bp = Blueprint('record_api', __name__)

from ...beans import LEVELS  # NOQA
levels = [str(i) for i in LEVELS] + ['1_2_3']

from ...utils import read_json  # NOQA
payload = read_json('data/beans/metadata.json')
category_codes = [item.get('code') for item in payload.get('categories')]
event_codes = [item.get('code') for item in payload.get('events')]
state_codes = [item.get('code') for item in payload.get('states')]
crash_codes = [item.get('code') for item in payload.get('crashes')]

from . import event_log  # NOQA
from . import state_log  # NOQA
from . import crash_log  # NOQA
from . import service_ratio  # NOQA
