# -*- coding: utf-8 -*-

import atexit
import fcntl


def init_apscheduler(app, scheduler):
    """解决多进程中APScheduler重复运行的问题"""
    f = open('data/scheduler.lock', 'wb')
    try:
        fcntl.flock(f, fcntl.LOCK_EX | fcntl.LOCK_NB)
        scheduler.init_app(app)
        scheduler.start()
    except:
        pass

    def unlock():
        fcntl.flock(f, fcntl.LOCK_UN)
        f.close()
    atexit.register(unlock)
