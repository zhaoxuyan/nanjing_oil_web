# -*- coding: utf-8 -*-

from flask import Blueprint
bp = Blueprint('query', __name__)

from ..beans import LEVELS  # NOQA
levels = [i for i in LEVELS]

# 不查询的事件
EXCLUDED_EVENTS = [
    100015
]

from . import event_log  # NOQA
from . import state_log  # NOQA
from . import crash_log  # NOQA
