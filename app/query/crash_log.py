# -*- coding: utf-8 -*-

from flask import request, render_template, flash, redirect, url_for
from flask_babel import lazy_gettext as _l
from flask_login import login_required

from .. import read_required, RECORDS_PER_PAGE, write_required, db
from ..beans import CrashLog, Scheduler, Crash
from ..utils import date_str_2_dt, DATE_FORMATTER
from . import bp


@bp.route('/crash_log', methods=['GET'])
@login_required
@read_required()
def list_crash_logs():
    """异常日志列表"""
    log_id = request.args.get('log_id', None, type=int)
    channel_id = request.args.get('channel_id', None, type=int)
    crash_code = request.args.get('crash_code', None, type=int)
    start = date_str_2_dt(request.args.get('start', None, type=str))
    end = date_str_2_dt(request.args.get('end', None, type=str))
    page = request.args.get('page', 1, type=int)
    custom_query = CrashLog.query
    if log_id is not None:
        custom_query = custom_query.filter(CrashLog.id == log_id)
    if channel_id is not None:
        custom_query = custom_query.filter(CrashLog.channel_id == channel_id)
    if crash_code is not None:
        custom_query = custom_query.filter(CrashLog.crash_code == crash_code)
    if start is not None:
        custom_query = custom_query.filter(CrashLog.timestamp >= start)
    if end is not None:
        custom_query = custom_query.filter(CrashLog.timestamp <= end)
    custom_query = custom_query.order_by(CrashLog.id.desc())
    return render_template('query/crash_list.jinja2', title=_l('CrashLog List'),
        res=custom_query.paginate(page, RECORDS_PER_PAGE, False),
        schedulers=Scheduler.query.all(),
        crashes=Crash.query.all(),
        keywords={
            "channel_id": channel_id if channel_id else '',
            "crash_code": crash_code if crash_code else '',
            "start": start.strftime(DATE_FORMATTER) if start else '',
            "end": end.strftime(DATE_FORMATTER) if end else ''
        })  # NOQA


@bp.route('/crash_log/delete/<int:log_id>', methods=['GET'])
@login_required
@write_required()
def delete_crash(log_id):
    """删除异常日志"""
    res = CrashLog.query.filter(CrashLog.id == log_id).first()
    if res is None:
        flash(_l('%(obj)s not found!', obj=log_id))
        return redirect(url_for('query.list_crash_logs'))
    db.session.delete(res)
    db.session.commit()
    flash(_l('%(obj)s has been deleted.', obj=log_id))

    log_id = request.args.get('log_id', None, type=int)
    channel_id = request.args.get('channel_id', None, type=int)
    crash_code = request.args.get('crash_code', None, type=int)
    start = date_str_2_dt(request.args.get('start', None, type=str))
    end = date_str_2_dt(request.args.get('end', None, type=str))
    page = request.args.get('page', 1, type=int)
    return redirect(url_for('query.list_crash_logs', log_id=log_id,
        channel_id=channel_id, crash_code=crash_code,
        start=start, end=end, page=page))  # NOQA
