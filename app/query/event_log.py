# -*- coding: utf-8 -*-

from flask import request, render_template, flash, redirect, url_for
from flask_babel import lazy_gettext as _l
from flask_login import login_required

from .. import read_required, RECORDS_PER_PAGE, write_required, db
from ..beans import EventLog, Scheduler, Category, Event
from ..utils import date_str_2_dt, DATE_FORMATTER
from . import bp, EXCLUDED_EVENTS, levels


@bp.route('/event_log', methods=['GET'])
@login_required
@read_required()
def list_event_logs():
    """事件日志列表"""
    log_id = request.args.get('log_id', None, type=int)
    channel_id = request.args.get('channel_id', None, type=int)
    category_code = request.args.get('category_code', None, type=int)
    event_code = request.args.get('event_code', None, type=int)
    level = request.args.get('level', None, type=int)
    start = date_str_2_dt(request.args.get('start', None, type=str))
    end = date_str_2_dt(request.args.get('end', None, type=str))
    page = request.args.get('page', 1, type=int)
    custom_query = EventLog.query
    for e in EXCLUDED_EVENTS:
        custom_query = custom_query.filter(EventLog.event_code != e)
    if log_id is not None:
        custom_query = custom_query.filter(EventLog.id == log_id)
    if channel_id is not None:
        custom_query = custom_query.filter(EventLog.channel_id == channel_id)
    if category_code is not None:
        custom_query = custom_query.filter(EventLog.category_code == category_code)
    if event_code is not None:
        custom_query = custom_query.filter(EventLog.event_code == event_code)
    if level is not None:
        custom_query = custom_query.filter(EventLog.level == level)
    if start is not None:
        custom_query = custom_query.filter(EventLog.timestamp >= start)
    if end is not None:
        custom_query = custom_query.filter(EventLog.timestamp <= end)
    custom_query = custom_query.order_by(EventLog.id.desc())
    return render_template('query/event_list.jinja2', title=_l('EventLog List'),
        res=custom_query.paginate(page, RECORDS_PER_PAGE, False),
        schedulers=Scheduler.query.all(),
        categories=Category.query.all(),
        category_dict={c.code: c.name_cn for c in Category.query.all()},
        events=Event.query.all(),
        levels=levels,
        keywords={
            "channel_id": channel_id if channel_id else '',
            "category_code": category_code if category_code else '',
            "event_code": event_code if event_code else '',
            "level": level if level else '',
            "start": start.strftime(DATE_FORMATTER) if start else '',
            "end": end.strftime(DATE_FORMATTER) if end else ''
        })  # NOQA


@bp.route('/event_log/delete/<int:log_id>', methods=['GET'])
@login_required
@write_required()
def delete_event(log_id):
    """删除事件日志"""
    res = EventLog.query.filter(EventLog.id == log_id).first()
    if res is None:
        flash(_l('%(obj)s not found!', obj=log_id))
        return redirect(url_for('query.list_event_logs'))
    db.session.delete(res)
    db.session.commit()
    flash(_l('%(obj)s has been deleted.', obj=log_id))

    log_id = request.args.get('log_id', None, type=int)
    channel_id = request.args.get('channel_id', None, type=int)
    category_code = request.args.get('category_code', None, type=int)
    event_code = request.args.get('event_code', None, type=int)
    level = request.args.get('level', None, type=int)
    start = date_str_2_dt(request.args.get('start', None, type=str))
    end = date_str_2_dt(request.args.get('end', None, type=str))
    page = request.args.get('page', 1, type=int)
    return redirect(url_for('query.list_event_logs', log_id=log_id,
        channel_id=channel_id, category_code=category_code, event_code=event_code,
        level=level, start=start, end=end, page=page))  # NOQA
