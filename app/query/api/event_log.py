# -*- coding: utf-8 -*-

from flask import request, jsonify
from flask_babel import lazy_gettext as _l

from ... import db, read_required, write_required, RECORDS_PER_PAGE, RECORDS_MAX_PER_PAGE
from ...api import token_auth, not_found
from ...beans import EventLog
from ...utils import date_str_2_dt
from .. import EXCLUDED_EVENTS
from . import bp


@bp.route('/event_log', methods=['GET'])
@token_auth.login_required
@read_required()
def list_event_logs():
    """获取事件日志

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 query_api 模块有 R 权限。

    ## 查询字段

    | 字段          | 是否必须 | 类型 | 说明                              |
    | ------------- | -------- | ---- | --------------------------------- |
    | log_id        |          | int  |                                   |
    | channel_id    |          | int  | 参考 `/config/api/scheduler` 接口 |
    | category_code |          | int  | 参考 `/bean/api/category` 接口    |
    | event_code    |          | int  | 参考 `/bean/api/event` 接口       |
    | level         |          | int  | 参考 `/bean/api/level` 接口       |
    | start         |          | str  | 形如：'YYYY-mm-dd'                |
    | end           |          | str  | 形如：'YYYY-mm-dd'                |
    | page          |          | int  | 默认：1                           |
    | per_page      |          | int  | 默认：25                          |

    ## 响应示例

    ```json
    {
        "_links": {
            "next": "/api/event_log?page=2&per_page=25",
            "prev": null,
            "self": "/api/event_log?page=1&per_page=25"
        },
        "_meta": {
            "page": 1,
            "per_page": 25,
            "total_items": 76,
            "total_pages": 4
        },
        "items": [
            {
                "category_code": 1,
                "channel_id": 2,
                "event_code": 100005,
                "image_path": "/image/xiwan/unload/20200821163351_卸油作业期间工作人员离开时间过长_2.jpg",
                "level": 2,
                "log_id": 117,
                "timestamp": "2020-08-21 16:33:51",
                "video_path": null
            },
            {
                "category_code": 3,
                "channel_id": 10,
                "event_code": 100009,
                "image_path": "/image/xiwan/refuel_side/20200821_161939_烟火_10.jpg",
                "level": 3,
                "log_id": 116,
                "timestamp": "2020-08-21 16:19:39",
                "video_path": null
            }
        ]
    }
    ```

    @@@
    """
    log_id = request.args.get('log_id', None, type=int)
    channel_id = request.args.get('channel_id', None, type=int)
    category_code = request.args.get('category_code', None, type=int)
    event_code = request.args.get('event_code', None, type=int)
    level = request.args.get('level', None, type=int)
    start = date_str_2_dt(request.args.get('start', None, type=str))
    end = date_str_2_dt(request.args.get('end', None, type=str))
    page = request.args.get('page', 1, type=int)
    per_page = request.args.get('per_page', RECORDS_PER_PAGE, type=int)
    per_page = min(per_page, RECORDS_MAX_PER_PAGE)
    custom_query = EventLog.query
    for e in EXCLUDED_EVENTS:
        custom_query = custom_query.filter(EventLog.event_code != e)
    if log_id is not None:
        custom_query = custom_query.filter(EventLog.id == log_id)
    if channel_id is not None:
        custom_query = custom_query.filter(EventLog.channel_id == channel_id)
    if category_code is not None:
        custom_query = custom_query.filter(EventLog.category_code == category_code)
    if event_code is not None:
        custom_query = custom_query.filter(EventLog.event_code == event_code)
    if level is not None:
        custom_query = custom_query.filter(EventLog.level == level)
    if start is not None:
        custom_query = custom_query.filter(EventLog.timestamp >= start)
    if end is not None:
        custom_query = custom_query.filter(EventLog.timestamp <= end)
    custom_query = custom_query.order_by(EventLog.id.desc())
    payload = EventLog.to_collection_dict(custom_query, page, per_page, 'query_api.list_event_logs',
        log_id=log_id, channel_id=channel_id, category_code=category_code, event_code=event_code, level=level, start=start, end=end)  # NOQA
    return jsonify(payload)


@bp.route('/event_log/<int:log_id>', methods=['DELETE'])
@token_auth.login_required
@write_required()
def delete_event_log(log_id):
    """删除事件日志

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 query_api 模块有 W 权限。

    ## 响应示例

    ```json
    {
        "category_code": 1,
        "channel_id": 2,
        "event_code": 100005,
        "image_path": "/image/xiwan/unload/20200821163351_卸油作业期间工作人员离开时间过长_2.jpg",
        "level": 2,
        "log_id": 117,
        "timestamp": "2020-08-21 16:33:51",
        "video_path": null
    }

    ```
    @@@
    """
    res = EventLog.query.filter(EventLog.id == log_id).first()
    if res is None:
        return not_found(_l('%(obj)s does not exists!', obj=log_id))
    db.session.delete(res)
    db.session.commit()
    return jsonify(res.to_dict())
