# -*- coding: utf-8 -*-

from flask import request, jsonify
from flask_babel import lazy_gettext as _l

from ... import db, read_required, write_required, RECORDS_PER_PAGE, RECORDS_MAX_PER_PAGE
from ...api import token_auth, not_found
from ...beans import StateLog
from ...utils import date_str_2_dt
from . import bp


@bp.route('/state_log', methods=['GET'])
@token_auth.login_required
@read_required()
def list_state_logs():
    """获取状态日志

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 query_api 模块有 R 权限。

    ## 查询字段

    | 字段          | 是否必须 | 类型 | 说明                              |
    | ------------- | -------- | ---- | --------------------------------- |
    | log_id        |          | int  |                                   |
    | channel_id    |          | int  | 参考 `/config/api/scheduler` 接口 |
    | category_code |          | int  | 参考 `/bean/api/category` 接口    |
    | state_code    |          | int  | 参考 `/bean/api/state` 接口       |
    | start         |          | str  | 形如：'YYYY-mm-dd'                |
    | end           |          | str  | 形如：'YYYY-mm-dd'                |
    | page          |          | int  | 默认：1                           |
    | per_page      |          | int  | 默认：25                          |

    ## 响应示例

    ```json
    {
        "_links": {
            "next": "/api/state_log?page=2&per_page=25",
            "prev": null,
            "self": "/api/state_log?page=1&per_page=25"
        },
        "_meta": {
            "page": 1,
            "per_page": 25,
            "total_items": 2489,
            "total_pages": 100
        },
        "items": [
            {
                "category_code": 2,
                "channel_id": 4,
                "end": "2020-08-21 17:11:04",
                "log_id": 2754,
                "seconds": 189,
                "start": "2020-08-21 17:07:55",
                "state_code": 200001
            },
            {
                "category_code": 2,
                "channel_id": 4,
                "end": "2020-08-21 17:09:44",
                "log_id": 2753,
                "seconds": 57,
                "start": "2020-08-21 17:08:47",
                "state_code": 200001
            }
        ]
    }
    ```

    @@@
    """
    log_id = request.args.get('log_id', None, type=int)
    channel_id = request.args.get('channel_id', None, type=int)
    category_code = request.args.get('category_code', None, type=int)
    state_code = request.args.get('state_code', None, type=int)
    start = date_str_2_dt(request.args.get('start', None, type=str))
    end = date_str_2_dt(request.args.get('end', None, type=str))
    page = request.args.get('page', 1, type=int)
    per_page = request.args.get('per_page', RECORDS_PER_PAGE, type=int)
    per_page = min(per_page, RECORDS_MAX_PER_PAGE)
    custom_query = StateLog.query
    if log_id is not None:
        custom_query = custom_query.filter(StateLog.id == log_id)
    if channel_id is not None:
        custom_query = custom_query.filter(StateLog.channel_id == channel_id)
    if category_code is not None:
        custom_query = custom_query.filter(StateLog.category_code == category_code)
    if state_code is not None:
        custom_query = custom_query.filter(StateLog.state_code == state_code)
    if start is not None:
        custom_query = custom_query.filter(StateLog.end >= start)
    if end is not None:
        custom_query = custom_query.filter(StateLog.end <= end)
    custom_query = custom_query.order_by(StateLog.id.desc())
    payload = StateLog.to_collection_dict(custom_query, page, per_page, 'query_api.list_state_logs',
        log_id=log_id, channel_id=channel_id, state_code=state_code, start=start, end=end)  # NOQA
    return jsonify(payload)


@bp.route('/state_log/<int:log_id>', methods=['DELETE'])
@token_auth.login_required
@write_required()
def delete_state_log(log_id):
    """删除状态日志

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 query_api 模块有 W 权限。

    ## 响应示例

    ```json
    {
        "category_code": 2,
        "channel_id": 4,
        "end": "2020-08-21 17:11:04",
        "log_id": 2754,
        "seconds": 189,
        "start": "2020-08-21 17:07:55",
        "state_code": 200001
    }
    ```

    @@@
    """
    res = StateLog.query.filter(StateLog.id == log_id).first()
    if res is None:
        return not_found(_l('%(obj)s does not exists!', obj=log_id))
    db.session.delete(res)
    db.session.commit()
    return jsonify(res.to_dict())
