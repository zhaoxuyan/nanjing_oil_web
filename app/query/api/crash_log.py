# -*- coding: utf-8 -*-

from flask import request, jsonify
from flask_babel import lazy_gettext as _l

from ... import db, read_required, write_required, RECORDS_PER_PAGE, RECORDS_MAX_PER_PAGE
from ...api import token_auth, not_found
from ...beans import CrashLog
from ...utils import date_str_2_dt
from . import bp


@bp.route('/crash_log', methods=['GET'])
@token_auth.login_required
@read_required()
def list_crash_logs():
    """获取异常日志

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 query_api 模块有 R 权限。

    ## 查询字段

    | 字段       | 是否必须 | 类型 | 说明                              |
    | ---------- | -------- | ---- | --------------------------------- |
    | log_id     |          | int  |                                   |
    | channel_id |          | int  | 参考 `/config/api/scheduler` 接口 |
    | crash_code |          | int  | 参考 `/bean/api/crash` 接口       |
    | start      |          | str  | 形如：'YYYY-mm-dd'                |
    | end        |          | str  | 形如：'YYYY-mm-dd'                |
    | page       |          | int  | 默认：1                           |
    | per_page   |          | int  | 默认：25                          |

    ## 响应示例

    ```json
    {
        "_links": {
            "next": "/api/crash_log?page=2&per_page=25",
            "prev": null,
            "self": "/api/crash_log?page=1&per_page=25"
        },
        "_meta": {
            "page": 1,
            "per_page": 25,
            "total_items": 31,
            "total_pages": 2
        },
        "items": [
            {
                "channel_id": 5,
                "crash_code": 300004,
                "image_path": "/image/xiwan/refuel_side/20200821172854_视频画面中出现遮挡物_5.jpg",
                "log_id": 41,
                "timestamp": "2020-08-21 17:28:54",
                "video_path": null
            },
            {
                "channel_id": 5,
                "crash_code": 300002,
                "image_path": "/image/xiwan/refuel_side/20200821172854_视频画面巨幅改变_5.jpg",
                "log_id": 40,
                "timestamp": "2020-08-21 17:28:54",
                "video_path": null
            }
        ]
    }
    ```

    @@@
    """
    log_id = request.args.get('log_id', None, type=int)
    channel_id = request.args.get('channel_id', None, type=int)
    crash_code = request.args.get('crash_code', None, type=int)
    start = date_str_2_dt(request.args.get('start', None, type=str))
    end = date_str_2_dt(request.args.get('end', None, type=str))
    page = request.args.get('page', 1, type=int)
    per_page = request.args.get('per_page', RECORDS_PER_PAGE, type=int)
    per_page = min(per_page, RECORDS_MAX_PER_PAGE)
    custom_query = CrashLog.query
    if log_id is not None:
        custom_query = custom_query.filter(CrashLog.id == log_id)
    if channel_id is not None:
        custom_query = custom_query.filter(CrashLog.channel_id == channel_id)
    if crash_code is not None:
        custom_query = custom_query.filter(CrashLog.crash_code == crash_code)
    if start is not None:
        custom_query = custom_query.filter(CrashLog.timestamp >= start)
    if end is not None:
        custom_query = custom_query.filter(CrashLog.timestamp <= end)
    custom_query = custom_query.order_by(CrashLog.id.desc())
    payload = CrashLog.to_collection_dict(custom_query, page, per_page, 'query_api.list_crash_logs',
        log_id=log_id, channel_id=channel_id, crash_code=crash_code, start=start, end=end)  # NOQA
    return jsonify(payload)


@bp.route('/crash_log/<int:log_id>', methods=['DELETE'])
@token_auth.login_required
@write_required()
def delete_crash_log(log_id):
    """删除异常日志

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 query_api 模块有 W 权限。

    ## 响应示例

    ```json
    {
        "channel_id": 5,
        "crash_code": 300004,
        "image_path": "/image/xiwan/refuel_side/20200821172854_视频画面中出现遮挡物_5.jpg",
        "log_id": 41,
        "timestamp": "2020-08-21 17:28:54",
        "video_path": null
    }
    ```

    @@@
    """
    res = CrashLog.query.filter(CrashLog.id == log_id).first()
    if res is None:
        return not_found(_l('%(obj)s does not exists!', obj=log_id))
    db.session.delete(res)
    db.session.commit()
    return jsonify(res.to_dict())
