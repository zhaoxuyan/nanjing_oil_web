# -*- coding: utf-8 -*-

from . import scheduler
from .record import table
from .event import crash


def do_jobs():
    scheduler.remove_all_jobs()
    scheduler.add_job('update_event_log', table.update_event_log, args=(), trigger='cron', minute=0)
    scheduler.add_job('update_state_log', table.update_state_log, args=(), trigger='cron', minute=0)
    scheduler.add_job('update_crash_log', table.update_crash_log, args=(), trigger='cron', minute=0)
    scheduler.add_job('check_300001', crash.check_300001, args=(), trigger='interval', minutes=10)
