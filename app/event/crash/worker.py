# -*- coding: utf-8 -*-

import os
from datetime import datetime

from flask import current_app

from ... import scheduler, db
from ...beans import Scheduler, CrashLog
from ...utils import save_screenshot, extract_ip, ping_ip
from .. import TEMP_DIR


def check_300001():
    with scheduler.app.app_context():
        schedulers = Scheduler.query.all()
        items_ready_add = []
        for s in schedulers:
            image_path = None
            if (extract_ip(s.url) and ping_ip(extract_ip(s.url))) or os.path.isfile(s.url):
                image_path = save_screenshot(s.url, image_dir=os.path.join(TEMP_DIR, 'crash'))
            if image_path:
                os.remove(image_path)
                current_app.logger.info('channel_id {}: {} is connected properly.'.format(s.id, s.url))
            else:
                crash_log = CrashLog.from_dict({
                    'channel_id': s.id,
                    'crash_code': 300001
                })
                items_ready_add.append(crash_log)
                current_app.logger.warning('channel_id {}: {} cannot be connected!'.format(s.id, s.url))
        if items_ready_add:
            db.session.add_all(items_ready_add)
            db.session.commit()
