# -*- coding: utf-8 -*-

from ..utils import extract_ip


def rtmp_to_http(rtmp_url, http_flv_port=8090):
    """rtmp转http格式

    Args:
        rtmp_url (str): rtmp地址，形如：'rtmp://server_ip:port1/live/xiwan_8'
        http_flv_port (int, optional): http推流端口. Defaults to 8090.

    Returns:
        str: http地址，形如：'http://server_ip:port2/live?app=live&stream=xiwan_8'
    """    
    ip = extract_ip(rtmp_url)
    stream_tag = str.split(rtmp_url, '/')[-1]
    if ip and stream_tag:
        return 'http://{}:{}/live?app=live&stream={}'.format(ip, http_flv_port, stream_tag)
    return None
