module Streamer{

    struct StreamInfo{
        string station;
        string category;
        string channelId;
        string streamUrl;
        string pushing;
        string previewPath;
    }

    sequence<StreamInfo> StreamInfos;

    interface stream
    {
        /**
         * 查询sivas.config配置文件,返回所有流的信息
         */
        StreamInfos queryStream();

        /**
         * 开启一路推流，并返回这一路流的相关信息
         */
        StreamInfo pullStream(string channelId);
    }
}
