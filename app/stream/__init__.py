# -*- coding: utf-8 -*-

from flask import Blueprint
bp = Blueprint('stream', __name__)

# ICE 配置
ICE_IP = '127.0.0.1'
ICE_PORT = 20100
STRING_IDENTITY = 'Encoder'
# http 推流端口
HTTP_FLV_PORT = 8090

from . import routes  # NOQA
