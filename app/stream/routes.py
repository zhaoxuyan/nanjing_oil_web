# -*- coding: utf-8 -*-

import sys

import Ice
from flask import jsonify, current_app
from flask_babel import lazy_gettext as _l

from .. import read_required, write_required
from ..api import token_auth, bad_request
from . import bp, ICE_IP, ICE_PORT, STRING_IDENTITY, HTTP_FLV_PORT
from .utils import rtmp_to_http


# 这里要加上搜索路径
sys.path.append('./app/stream')  # NOQA
import Streamer  # NOQA


@bp.route('/query', methods=['GET'])
@token_auth.login_required
@read_required()
def query_streams():
    """获取视频流信息

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 stream 模块有 R 权限。

    ## 响应示例

    ```json
    {
        "code": 6000,
        "data": [
            {
                "category": "gate",
                "channel": "9",
                "httpflvUrl": "http://10.181.128.93:8090/live?app=live&stream=jinshajiang_09",
                "preview": "/image//preview/jinshajiang_09.jpg",
                "pushing": false,
                "station": "jinshajiang",
                "streamUrl": "rtmp://10.181.128.93/live/jinshajiang_09"
            }
        ]
    }
    ```

    @@@
    """
    try:
        communicator = Ice.initialize()
        base = communicator.stringToProxy('{}:default -h {} -p {} -t 1000'.format(STRING_IDENTITY, ICE_IP, ICE_PORT))
        stream_pusher = Streamer.streamPrx.checkedCast(base)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify({'code': 9000, 'data': []})

    if not stream_pusher:
        return jsonify({'code': 9000, 'data': []})

    try:
        stream_infos = stream_pusher.queryStream()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify({'code': 9000, 'data': []})

    try:
        communicator.destroy()
    except Exception as e:
        current_app.logger.error(e)

    return_result = []
    for info in stream_infos:
        stream_info = {
            'station': info.station,
            'category': info.category,
            'channel': info.channelId,
            'streamUrl': info.streamUrl,
            'httpflvUrl': rtmp_to_http(info.streamUrl),
            'pushing': True if info.pushing == '1' else False,
            'preview': '/image/{}'.format(info.previewPath)
        }
        return_result.append(stream_info)

    sort_keys = ['unload', 'checkout', 'gate', 'refuel_side']
    return_result.sort(key=lambda item: [sort_keys.index(item.get('category')), int(item.get('channel'))])

    return jsonify({'code': 6000, 'data': return_result})


@bp.route('/pull/<string:channel_id>', methods=['GET'])
@token_auth.login_required
@write_required()
def pull_stream(channel_id):
    """拉取一路视频流

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 stream 模块有 W 权限。

    ## 响应示例

    ```json
    {
        "code": 6000,
        "data": {
            "category": "gate",
            "channel": "9",
            "httpflvUrl": "http://10.181.128.93:8090/live?app=live&stream=jinshajiang_09",
            "preview": "/image//preview/jinshajiang_09.jpg",
            "pushing": false,
            "station": "jinshajiang",
            "streamUrl": "rtmp://10.181.128.93/live/jinshajiang_09"
        }
    }
    ```

    @@@
    """
    try:
        communicator = Ice.initialize()
        base = communicator.stringToProxy('{}:default -h {} -p {} -t 1000'.format(STRING_IDENTITY, ICE_IP, ICE_PORT))
        stream_pusher = Streamer.streamPrx.checkedCast(base)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify({'code': 9000, 'data': {}})

    if not stream_pusher:
        return jsonify({'code': 9000, 'data': {}})

    try:
        stream_infos = stream_pusher.queryStream()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify({'code': 9000, 'data': {}})

    if channel_id not in [str(info.channelId) for info in stream_infos]:
        return bad_request(_l('%(obj)s is invalid!', obj=channel_id))

    try:
        info = stream_pusher.pullStream(channel_id)
    except Exception as _:
        return jsonify({'code': 9000, 'data': {}})

    try:
        communicator.destroy()
    except Exception as e:
        current_app.logger.error(e)

    stream_info = {
        'station': info.station,
        'category': info.category,
        'channel': info.channelId,
        'streamUrl': info.streamUrl,
        'httpflvUrl': rtmp_to_http(info.streamUrl),
        'pushing': True if info.pushing == '1' else False,
        'preview': '/image/{}'.format(info.previewPath)
    }
    return jsonify({'code': 6000, 'data': stream_info})
