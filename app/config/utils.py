# -*- coding: utf-8 -*-


def schedulers_to_config(schedulers):
    """[object,...]转{'scheduler': [...]}

    Args:
        schedulers (list): 调度程序列表

    Returns:
        dict: 配置文件内容
    """
    payload = {'scheduler': []}
    for s in schedulers:
        item = {}
        item['channel_id'] = s.id
        item['channel_name'] = s.name
        item['category'] = s.category_name
        item['start'] = s.start
        item['batch_predict_size'] = s.batch
        item['input_video_streamer'] = {
            'in_url': s.url,
            'mat_height': s.height,
            'mat_width': s.width,
            'buffer_length': s.buffer
        }
        item.update(services_to_config(s.services))
        item.update(masks_to_config(s.masks))
        payload['scheduler'].append(item)
    return payload


def config_to_schedulers(payload):
    """{'scheduler': [...]}转[object,...]

    Args:
        payload (dict): 配置文件内容

    Returns:
        list: 转换结果
    """
    import os.path as osp
    from app.beans import Scheduler
    schedulers = []
    for item in payload.get('scheduler'):
        s = Scheduler()
        s.id = item.get('channel_id', 0)
        s.name = item.get('channel_name', '')
        s.category_name = item.get('category', '')[:20]
        s.start = item.get('start', 0)
        s.batch = item.get('batch_predict_size', 4)
        s.url = item.get('input_video_streamer', {}).get('in_url', '')[:200]
        s.height = item.get('input_video_streamer', {}).get('mat_height', 720)
        s.width = item.get('input_video_streamer', {}).get('mat_width', 1280)
        s.buffer = item.get('input_video_streamer', {}).get('buffer_length', 30)
        image_path = s.capture_screenshot()
        if not image_path:
            continue
        s.image_path = osp.join('scheduler', osp.split(image_path)[1])
        schedulers.append(s)
    return schedulers


def config_to_cameras(payload):
    """{'scheduler': [...]}转[object,...]

    Args:
        payload (dict): 配置文件内容

    Returns:
        list: 转换结果
    """
    from app.beans import Camera
    cameras = []
    urls = []
    for item in payload.get('scheduler'):
        c = Camera()
        c.url = item.get('input_video_streamer', {}).get('in_url', '')[:200]
        if c.url in urls:
            continue
        urls.append(c.url)
        image_path = c.capture_screenshot()
        if not image_path:
            continue
        cameras.append(c)
    return cameras


def services_to_config(services):
    """[object,...]转{'scheduler': [...]}

    Args:
        schedulers (list): 模型服务列表

    Returns:
        dict: 配置文件内容
    """
    payload = {'model_services': []}
    for s in services:
        item = {}
        item['type'] = s.name
        item['ip'] = s.ip
        item['port'] = s.port
        payload['model_services'].append(item)
    return payload


def config_to_services(payload):
    """{'scheduler': [...]}转[object,...]

    Args:
        payload (dict): 配置文件内容

    Returns:
        list: 转换结果
    """
    from app.beans import Service
    services = []
    for item in payload.get('scheduler'):
        channel_id = item.get('channel_id')
        for i in item.get('model_services'):
            s = Service()
            s.channel_id = channel_id
            s.name = i.get('type', '')[:20]
            s.ip = i.get('ip', '0.0.0.0')[:50]
            s.port = i.get('port', -1)
            services.append(s)
    return services


def masks_to_config(masks):
    """[object,...]转{'scheduler': [...]}

    Args:
        schedulers (list): 检测区域列表

    Returns:
        dict: 配置文件内容
    """
    payload = {}

    def parse(_payload, mask, mask_name):
        if mask_name.find('.') != -1:
            prefix = mask_name.split('.')[0]
            not_prefix = mask_name[len(prefix)+1:]
            if prefix not in _payload:
                _payload.setdefault(prefix, {})
            parse(_payload[prefix], mask, not_prefix)
        else:
            if mask_name not in _payload:
                _payload.setdefault(mask_name, [])
            area = {}
            area['x_min'] = mask.x_min
            area['y_min'] = mask.y_min
            area['x_max'] = mask.x_max
            area['y_max'] = mask.y_max
            _payload[mask_name].append(area)
    for m in masks:
        parse(payload, m, m.name)
    return payload


def config_to_masks(payload):
    """{'scheduler': [...]}转[object,...]

    Args:
        payload (dict): 配置文件内容

    Returns:
        list: 转换结果
    """
    from app.beans import Mask

    def parse(_masks, _item, prefix=None):
        for k in _item:
            if isinstance(_item.get(k), dict):
                parse(_masks, _item.get(k), (prefix + '.' + k) if prefix else k)
            elif isinstance(_item.get(k), list):
                for area in _item.get(k):
                    if 'x_min' in area and 'y_min' in area and 'x_max' in area and 'y_max' in area:
                        mask = Mask()
                        mask.channel_id = channel_id
                        mask.name = (prefix + '.' + k) if prefix else k
                        mask.x_min = area.get('x_min', 0)
                        mask.y_min = area.get('y_min', 0)
                        mask.x_max = area.get('x_max', 0)
                        mask.y_max = area.get('y_max', 0)
                        _masks.append(mask)
    masks = []
    for item in payload.get('scheduler'):
        channel_id = item.get('channel_id')
        item.pop('channel_id')
        item.pop('channel_name')
        item.pop('category')
        item.pop('start')
        item.pop('batch_predict_size')
        item.pop('input_video_streamer')
        item.pop('model_services')
        parse(masks, item)
    return masks
