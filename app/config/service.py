# -*- coding: utf-8 -*-

from flask import request, render_template, flash, redirect, url_for
from flask_babel import lazy_gettext as _l
from flask_login import login_required

from .. import read_required, RECORDS_PER_PAGE, write_required, db
from ..beans import Service, Scheduler, SERVICES
from . import bp
from .forms import ServiceEditForm


@bp.route('/service', methods=['GET'])
@login_required
@read_required()
def list_services():
    """模型服务列表"""
    service_id = request.args.get('service_id', None, type=int)
    channel_id = request.args.get('channel_id', None, type=int)
    name = request.args.get('name', None, type=str)
    page = request.args.get('page', 1, type=int)
    custom_query = Service.query
    if service_id is not None:
        custom_query = custom_query.filter(Service.id == service_id)
    if channel_id is not None:
        custom_query = custom_query.filter(Service.channel_id == channel_id)
    if name is not None:
        custom_query = custom_query.filter(Service.name == name)
    custom_query = custom_query.order_by(Service.id.asc())
    return render_template('config/service_list.jinja2', title=_l('Service List'),
        res=custom_query.paginate(page, RECORDS_PER_PAGE, False),
        services=Service.query.all(),
        schedulers=Scheduler.query.all(),
        names=list(SERVICES.keys()),
        keywords={
            "service_id": service_id if service_id else '',
            "channel_id": channel_id if channel_id else '',
            "name": name if name else ''
        })  # NOQA


@bp.route('/service/add', methods=['GET', 'POST'])
@login_required
@write_required()
def add_service():
    """新增模型服务"""
    form = ServiceEditForm()
    if form.validate_on_submit():
        service = Service.from_dict(form.data)
        db.session.add(service)
        db.session.commit()
        flash(_l('%(obj)s has been added.', obj=service.name))
        return redirect(url_for('config.list_services'))
    channel_id = request.args.get('channel_id', None, type=int)
    if channel_id:
        res = Scheduler.query.filter(Scheduler.id == channel_id).first()
        form.channel_id.choices = [(str(res.id), str(res.id))]
        form.channel_id.data = res.id
    return render_template('edit.jinja2', title=_l('Add Service'),
        form=form)  # NOQA


@bp.route('/service/<int:service_id>', methods=['GET', 'POST'])
@login_required
@write_required()
def update_service(service_id):
    """更新模型服务"""
    res = Service.query.filter(Service.id == service_id).first()
    if res is None:
        flash(_l('%(obj)s not found!', obj=service_id))
        return redirect(url_for('config.list_services'))
    form = ServiceEditForm(res)
    if form.validate_on_submit():
        res = Service.from_dict(form.data, res)
        db.session.commit()
        flash(_l('%(obj)s has been updated.', obj=service_id))
        return redirect(url_for('config.list_services'))
    form.channel_id.choices = [(str(res.channel_id), str(res.channel_id))]
    form.name.choices = [(res.name, res.name)]
    return render_template('edit.jinja2', title=_l('Update Service'),
        form=form)  # NOQA


@bp.route('/service/delete/<int:service_id>', methods=['GET'])
@login_required
@write_required()
def delete_service(service_id):
    """删除模型服务"""
    res = Service.query.filter(Service.id == service_id).first()
    if res is None:
        flash(_l('%(obj)s not found!', obj=service_id))
        return redirect(url_for('config.list_services'))
    db.session.delete(res)
    db.session.commit()
    flash(_l('%(obj)s has been deleted.', obj=service_id))
    return redirect(url_for('config.list_services'))
