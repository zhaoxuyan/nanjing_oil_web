# -*- coding: utf-8 -*-

from datetime import datetime

from flask import render_template, flash, redirect, url_for
from flask_babel import lazy_gettext as _l
from flask_login import login_required

from .. import write_required, db
from ..beans import Camera
from ..utils import RTSP_PORT, scan_rtsp, write_json, read_json, DATETIME_FORMATTER
from . import bp, CAMERA_SCAN_RESULT
from .forms import CameraScanForm


@bp.route('/camera/scan', methods=['GET', 'POST'])
@login_required
@write_required()
def scan_camera():
    """扫描摄像头"""
    form = CameraScanForm()
    if form.validate_on_submit():
        ip_address = form.ip_address.data
        user = form.user.data
        pwd = form.pwd.data
        port = form.port.data if form.port.data != RTSP_PORT else None
        results = []
        results = scan_rtsp(ip_address, user, pwd, port)
        for item in results:
            res = Camera.query.filter(Camera.url == item.get('rtsp_url')).first()
            item['in_database'] = 1 if res else 0
        # 为了防止反复扫描摄像头消耗Web后台资源，把每次摄像头扫描结果存到文件中
        payload = {
            'cameras': results,
            'timestamp': datetime.strftime(datetime.utcnow(), DATETIME_FORMATTER)
        }
        write_json(payload, CAMERA_SCAN_RESULT, new_thread=True)
        return redirect(url_for('config.scan_camera_history'))
    payload = read_json(CAMERA_SCAN_RESULT)
    return render_template('config/camera_scan.jinja2', title=_l('Scan Camera'),
        form=form, scan_result=payload)  # NOQA


@bp.route('/camera/scan_history', methods=['GET'])
@login_required
@write_required()
def scan_camera_history():
    """查看摄像头扫描历史"""
    payload = read_json(CAMERA_SCAN_RESULT)
    if not payload:
        flash(_l('last scan results not found!'))
        return redirect(url_for('config.scan_camera'))
    for item in payload.get('cameras'):
        res = Camera.query.filter(Camera.url == item.get('rtsp_url')).first()
        item['in_database'] = 1 if res else 0
    write_json(payload, CAMERA_SCAN_RESULT, new_thread=True)
    return render_template('config/camera_scan_result.jinja2', title=_l('Scan Result'),
        scan_result=payload.get('cameras'),  # NOQA
        scan_timestamp=datetime.strptime(payload.get('timestamp'), DATETIME_FORMATTER))  # NOQA


@bp.route('/camera/add_history', methods=['GET'])
@login_required
@write_required()
def add_camera_history():
    """新增摄像头扫描结果"""
    payload = read_json(CAMERA_SCAN_RESULT)
    if not payload:
        flash(_l('last scan results not found!'))
        return redirect(url_for('config.scan_camera'))
    ready_add_items = []
    for item in payload.get('cameras'):
        if Camera.query.filter(Camera.url == item.get('rtsp_url')).count() == 0:
            camera = Camera(url=item.get('rtsp_url'))
            ready_add_items.append(camera)
    db.session.add_all(ready_add_items)
    db.session.commit()
    flash(_l('%(size)d records from %(path)s have been added.', size=len(ready_add_items), path=CAMERA_SCAN_RESULT))
    return redirect(url_for('config.list_cameras'))
