# -*- coding: utf-8 -*-

import os.path as osp

from flask import flash, redirect, url_for
from flask_babel import lazy_gettext as _l
from flask_login import login_required

from .. import write_required, db
from ..beans import Scheduler, Camera
from ..utils import read_json, write_json
from . import bp, CONFIG_METADATA, SCHEDULERS_CONFIG
from .utils import schedulers_to_config, config_to_schedulers, config_to_cameras, config_to_services, config_to_masks


@bp.route('/scheduler/export', methods=['GET', 'POST'])
@login_required
@write_required()
def export_schedulers():
    """导出调度程序"""
    payload = schedulers_to_config(Scheduler.query.all())
    config_dir = read_json(CONFIG_METADATA).get('write_config_dir', 'tmp')
    config_path = osp.join(config_dir, SCHEDULERS_CONFIG)
    # fixme: 此处写文件时会遇到没有权限，由于在另一个线程操作，报错不会提示到界面
    write_json(payload, config_path, new_thread=True)
    flash(_l('scheduler has been exported to %(path)s.', path=config_path))
    return redirect(url_for('config.list_schedulers'))


@bp.route('/scheduler/import', methods=['GET', 'POST'])
@login_required
@write_required()
def import_schedulers():
    """导入调度程序"""
    config_dir = read_json(CONFIG_METADATA).get('read_config_dir', 'data/config')
    config_path = osp.join(config_dir, SCHEDULERS_CONFIG)
    payload = read_json(config_path)
    # 先新增摄像头
    cameras = config_to_cameras(payload)
    ready_add_items = []
    for c in cameras:
        if Camera.query.filter(Camera.url == c.url).count() == 0:
            ready_add_items.append(c)
    db.session.add_all(ready_add_items)
    db.session.commit()
    # 新增调度调度程序
    schedulers = config_to_schedulers(payload)
    ready_add_items = []
    for s in schedulers:
        if Scheduler.query.filter(Scheduler.id == s.id).count() == 0:
            ready_add_items.append(s)
    db.session.add_all(ready_add_items)
    db.session.commit()
    flash(_l('%(size)d records from %(path)s has been imported.', size=len(ready_add_items), path=config_path))
    return redirect(url_for('config.list_schedulers'))


@bp.route('/service/import', methods=['GET', 'POST'])
@login_required
@write_required()
def import_services():
    """导入模型服务"""
    config_dir = read_json(CONFIG_METADATA).get('read_config_dir', 'data/config')
    config_path = osp.join(config_dir, SCHEDULERS_CONFIG)
    payload = read_json(config_path)
    # 先新增摄像头
    cameras = config_to_cameras(payload)
    ready_add_items = []
    for c in cameras:
        if Camera.query.filter(Camera.url == c.url).count() == 0:
            ready_add_items.append(c)
    db.session.add_all(ready_add_items)
    db.session.commit()
    # 先新增调度程序
    schedulers = config_to_schedulers(payload)
    ready_add_items = []
    for s in schedulers:
        if Scheduler.query.filter(Scheduler.id == s.id).count() == 0:
            ready_add_items.append(s)
    db.session.add_all(ready_add_items)
    db.session.commit()
    # 新增模型服务
    ready_add_items = config_to_services(payload)
    db.session.add_all(ready_add_items)
    db.session.commit()
    flash(_l('%(size)d records from %(path)s has been imported.', size=len(ready_add_items), path=config_path))
    return redirect(url_for('config.list_services'))


@bp.route('/mask/import', methods=['GET', 'POST'])
@login_required
@write_required()
def import_masks():
    """导入检测区域"""
    config_dir = read_json(CONFIG_METADATA).get('read_config_dir', 'data/config')
    config_path = osp.join(config_dir, SCHEDULERS_CONFIG)
    payload = read_json(config_path)
    # 先新增摄像头
    cameras = config_to_cameras(payload)
    ready_add_items = []
    for c in cameras:
        if Camera.query.filter(Camera.url == c.url).count() == 0:
            ready_add_items.append(c)
    db.session.add_all(ready_add_items)
    db.session.commit()
    # 先新增调度程序
    schedulers = config_to_schedulers(payload)
    ready_add_schedulers = []
    for s in schedulers:
        if Scheduler.query.filter(Scheduler.id == s.id).count() == 0:
            ready_add_schedulers.append(s)
    db.session.add_all(ready_add_schedulers)
    db.session.commit()
    # 新增检测区域
    ready_add_items = config_to_masks(payload)
    db.session.add_all(ready_add_items)
    db.session.commit()
    flash(_l('%(size)d records from %(path)s has been imported.', size=len(ready_add_items), path=config_path))
    return redirect(url_for('config.list_masks'))
