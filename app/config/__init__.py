# -*- coding: utf-8 -*-

from flask import Blueprint
bp = Blueprint('config', __name__)

# 临时文件夹
TEMP_DIR = 'tmp/config'

# 扫描摄像头
import os.path as osp  # NOQA
CAMERA_SCAN_CONFIG = 'data/config/scan_camera_config.json'
CAMERA_SCAN_RESULT = osp.join(TEMP_DIR, 'scan_camera_result.json')

# 配置文件路径
CONFIG_METADATA = 'data/config/metadata.json'
SCHEDULERS_CONFIG = 'sivas.config'

from . import camera  # NOQA
from . import camera_scan  # NOQA
from . import scheduler  # NOQA
from . import service  # NOQA
from . import mask  # NOQA
from . import import_and_export  # NOQA
