# -*- coding: utf-8 -*-

from flask import request, render_template, flash, redirect, url_for
from flask_babel import lazy_gettext as _l
from flask_login import login_required

from .. import read_required, RECORDS_PER_PAGE, write_required, db
from ..beans import Mask, Scheduler
from . import bp
from .forms import MaskEditForm


@bp.route('/mask', methods=['GET'])
@login_required
@read_required()
def list_masks():
    """检测区域列表"""
    mask_id = request.args.get('mask_id', None, type=int)
    channel_id = request.args.get('channel_id', None, type=int)
    name = request.args.get('name', None, type=str)
    page = request.args.get('page', 1, type=int)
    custom_query = Mask.query
    if mask_id is not None:
        custom_query = custom_query.filter(Mask.id == mask_id)
    if channel_id is not None:
        custom_query = custom_query.filter(Mask.channel_id == channel_id)
    if name is not None:
        custom_query = custom_query.filter(Mask.name.like('%' + name + '%'))
    custom_query = custom_query.order_by(Mask.id.asc())
    return render_template('config/mask_list.jinja2', title=_l('Mask List'),
        res=custom_query.paginate(page, RECORDS_PER_PAGE, False),
        masks=Mask.query.all(),
        schedulers=Scheduler.query.all(),
        keywords={
            "mask_id": mask_id if mask_id else '',
            "channel_id": channel_id if channel_id else '',
            "name": name if name else ''
        })  # NOQA


@bp.route('/mask/add', methods=['GET', 'POST'])
@login_required
@write_required()
def add_mask():
    """新增检测区域"""
    form = MaskEditForm()
    if form.validate_on_submit():
        mask = Mask.from_dict(form.data)
        db.session.add(mask)
        db.session.commit()
        flash(_l('%(obj)s has been added.', obj=mask.name))
        return redirect(url_for('config.list_masks'))
    channel_id = request.args.get('channel_id', None, type=int)
    if channel_id:
        res = Scheduler.query.filter(Scheduler.id == channel_id).first()
        form.channel_id.choices = [(str(res.id), str(res.id))]
        form.channel_id.data = res.id
    return render_template('edit.jinja2', title=_l('Add Mask'),
        form=form)  # NOQA


@bp.route('/mask/<int:mask_id>', methods=['GET', 'POST'])
@login_required
@write_required()
def update_mask(mask_id):
    """更新检测区域"""
    res = Mask.query.filter(Mask.id == mask_id).first()
    if res is None:
        flash(_l('%(obj)s not found!', obj=mask_id))
        return redirect(url_for('config.list_masks'))
    form = MaskEditForm(res)
    if form.validate_on_submit():
        res = Mask.from_dict(form.data, res)
        db.session.commit()
        flash(_l('%(obj)s has been updated.', obj=mask_id))
        return redirect(url_for('config.list_masks'))
    form.channel_id.choices = [(str(res.channel_id), str(res.channel_id))]
    form.name.choices = [(res.name, res.name)]
    return render_template('edit.jinja2', title=_l('Update Mask'),
        form=form)  # NOQA


@bp.route('/mask/delete/<int:mask_id>', methods=['GET'])
@login_required
@write_required()
def delete_mask(mask_id):
    """删除检测区域"""
    res = Mask.query.filter(Mask.id == mask_id).first()
    if res is None:
        flash(_l('%(obj)s not found!', obj=mask_id))
        return redirect(url_for('config.list_masks'))
    db.session.delete(res)
    db.session.commit()
    flash(_l('%(obj)s has been deleted.', obj=mask_id))
    return redirect(url_for('config.list_masks'))
