# -*- coding: utf-8 -*-

import os.path as osp

from flask import request, render_template, flash, redirect, url_for
from flask_babel import lazy_gettext as _l
from flask_login import login_required

from .. import read_required, RECORDS_PER_PAGE, write_required, db
from ..beans import Scheduler, Camera, Category
from . import bp
from .forms import SchedulerEditForm


@bp.route('/scheduler', methods=['GET'])
@login_required
@read_required()
def list_schedulers():
    """调度程序列表"""
    channel_id = request.args.get('channel_id', None, type=int)
    name = request.args.get('name', None, type=str)
    category_name = request.args.get('category_name', None, type=str)
    url = request.args.get('url', None, type=str)
    page = request.args.get('page', 1, type=int)
    custom_query = Scheduler.query
    if channel_id is not None:
        custom_query = custom_query.filter(Scheduler.id == channel_id)
    if name is not None:
        custom_query = custom_query.filter(Scheduler.name.like('%' + name + '%'))
    if category_name is not None:
        custom_query = custom_query.filter(Scheduler.category_name == category_name)
    if url is not None:
        custom_query = custom_query.filter(Scheduler.url.like('%' + url + '%'))
    custom_query = custom_query.order_by(Scheduler.id.asc())
    return render_template('config/scheduler_list.jinja2', title=_l('Scheduler List'),
        res=custom_query.paginate(page, RECORDS_PER_PAGE, False),
        schedulers=Scheduler.query.all(),
        categories=Category.query.all(),
        keywords={
            "channel_id": channel_id if channel_id else '',
            "name": name if name else '',
            "category_name": category_name if category_name else '',
            "url": url if url else ''
        })  # NOQA


@bp.route('/scheduler/add', methods=['GET', 'POST'])
@login_required
@write_required()
def add_scheduler():
    """新增调度程序"""
    form = SchedulerEditForm()
    if form.validate_on_submit():
        scheduler = Scheduler.from_dict(form.data)
        if Scheduler.query.filter(Scheduler.name == scheduler.name).count() > 0:
            flash(_l('%(obj)s already exists!', obj=scheduler.name))
            return redirect(url_for('config.list_schedulers'))
        scheduler.image_path = osp.join('scheduler', osp.split(scheduler.capture_screenshot())[1])
        db.session.add(scheduler)
        db.session.commit()
        flash(_l('%(obj)s has been added.', obj=scheduler.url))
        return redirect(url_for('config.list_schedulers'))
    camera_id = request.args.get('camera_id', None, type=int)
    if camera_id:
        res = Camera.query.filter(Camera.id == camera_id).first()
        form.url.choices = [(res.url, res.url)]
        form.url.data = res.url
    return render_template('edit.jinja2', title=_l('Add Scheduler'),
        form=form)  # NOQA


@bp.route('/scheduler/<int:channel_id>', methods=['GET', 'POST'])
@login_required
@write_required()
def update_scheduler(channel_id):
    """更新调度程序"""
    res = Scheduler.query.filter(Scheduler.id == channel_id).first()
    if res is None:
        flash(_l('%(obj)s not found!', obj=channel_id))
        return redirect(url_for('config.list_schedulers'))
    form = SchedulerEditForm(res)
    if form.validate_on_submit():
        res = Scheduler.from_dict(form.data, res)
        res.image_path = osp.join('scheduler', osp.split(res.capture_screenshot())[1])
        db.session.commit()
        flash(_l('%(obj)s has been updated.', obj=channel_id))
        return redirect(url_for('config.list_schedulers'))
    form.category_name.choices = [(res.category_name, res.category_name)]
    form.url.choices = [(res.url, res.url)]
    return render_template('edit.jinja2', title=_l('Update Scheduler'),
        form=form)  # NOQA


@bp.route('/scheduler/delete/<int:channel_id>', methods=['GET'])
@login_required
@write_required()
def delete_scheduler(channel_id):
    """删除调度程序"""
    res = Scheduler.query.filter(Scheduler.id == channel_id).first()
    if res is None:
        flash(_l('%(obj)s not found!', obj=channel_id))
        return redirect(url_for('config.list_schedulers'))
    if res.services.count() > 0 or res.masks.count() > 0 or res.event_logs.count() > 0 or \
            res.state_logs.count() > 0 or res.crash_logs.count() > 0:
        flash(_l('%(obj)s in used!', obj=channel_id))
        return redirect(url_for('config.list_schedulers'))
    db.session.delete(res)
    db.session.commit()
    flash(_l('%(obj)s has been deleted.', obj=channel_id))
    return redirect(url_for('config.list_schedulers'))
