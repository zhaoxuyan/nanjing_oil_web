# -*- coding: utf-8 -*-

from flask import request, jsonify
from flask_babel import lazy_gettext as _l

from ... import db, read_required, write_required, RECORDS_PER_PAGE, RECORDS_MAX_PER_PAGE
from ...api import token_auth, bad_request, not_found
from ...beans import Service, Scheduler, SERVICES
from . import bp


@bp.route('/service', methods=['GET'])
@token_auth.login_required
@read_required()
def list_services():
    """获取检测区域

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 config_api 模块有 R 权限。

    ## 查询字段

    | 字段       | 是否必须 | 类型 | 说明                              |
    | ---------- | -------- | ---- | --------------------------------- |
    | service_id |          | int  |                                   |
    | channel_id |          | int  | 参考 `/config/api/scheduler` 接口 |
    | name       |          | str  |                                   |
    | page       |          | int  | 默认：1                           |
    | per_page   |          | int  | 默认：25                          |

    ## 响应示例

    ```json
    {
        "_links": {
            "next": null,
            "prev": null,
            "self": "/api/service?page=1&per_page=25"
        },
        "_meta": {
            "page": 1,
            "per_page": 25,
            "total_items": 12,
            "total_pages": 1
        },
        "items": [
            {
                "channel_id": 1,
                "ip": "0.0.0.0",
                "name": "detect",
                "port": 20081,
                "service_id": 1
            },
            {
                "channel_id": 2,
                "ip": "0.0.0.0",
                "name": "detect",
                "port": 20081,
                "service_id": 2
            },
            {
                "channel_id": 3,
                "ip": "0.0.0.0",
                "name": "detect",
                "port": 20082,
                "service_id": 3
            },
            {
                "channel_id": 4,
                "ip": "0.0.0.0",
                "name": "detect",
                "port": 20082,
                "service_id": 4
            },
            {
                "channel_id": 5,
                "ip": "0.0.0.0",
                "name": "detect",
                "port": 20083,
                "service_id": 5
            },
            {
                "channel_id": 5,
                "ip": "0.0.0.0",
                "name": "smoke",
                "port": -1,
                "service_id": 6
            },
            {
                "channel_id": 6,
                "ip": "0.0.0.0",
                "name": "detect",
                "port": 20083,
                "service_id": 7
            },
            {
                "channel_id": 6,
                "ip": "0.0.0.0",
                "name": "smoke",
                "port": -1,
                "service_id": 8
            },
            {
                "channel_id": 7,
                "ip": "0.0.0.0",
                "name": "detect",
                "port": 20083,
                "service_id": 9
            },
            {
                "channel_id": 7,
                "ip": "0.0.0.0",
                "name": "smoke",
                "port": -1,
                "service_id": 10
            },
            {
                "channel_id": 8,
                "ip": "0.0.0.0",
                "name": "detect",
                "port": 20083,
                "service_id": 11
            },
            {
                "channel_id": 8,
                "ip": "0.0.0.0",
                "name": "smoke",
                "port": -1,
                "service_id": 12
            }
        ]
    }
    ```

    @@@
    """
    service_id = request.args.get('service_id', None, type=int)
    channel_id = request.args.get('channel_id', None, type=int)
    name = request.args.get('name', None, type=str)
    page = request.args.get('page', 1, type=int)
    per_page = request.args.get('per_page', RECORDS_PER_PAGE, type=int)
    per_page = min(per_page, RECORDS_MAX_PER_PAGE)
    custom_query = Service.query
    if service_id is not None:
        custom_query = custom_query.filter(Service.id == service_id)
    if channel_id is not None:
        custom_query = custom_query.filter(Service.channel_id == channel_id)
    if name is not None:
        custom_query = custom_query.filter(Service.name == name)
    custom_query = custom_query.order_by(Service.id.asc())
    payload = Service.to_collection_dict(custom_query, page, per_page, 'config_api.list_services',
        service_id=service_id, channel_id=channel_id, name=name)  # NOQA
    return jsonify(payload)


@bp.route('/service/add', methods=['POST'])
@token_auth.login_required
@write_required()
def add_service():
    """新增检测区域

    @@@

    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 config_api 模块有 W 权限。

    ## 表单字段

    | 字段       | 是否必须 | 类型 | 说明                                    |
    | ---------- | -------- | ---- | --------------------------------------- |
    | channel_id | 是       | int  | 参考 `/bean/api/scheduler` 接口         |
    | name       | 是       | str  | 模型服务的名称，可以为：detect 或 smoke |
    | ip         |          | str  | 默认：0.0.0.0                           |
    | port       |          | int  | 默认：-1                                |

    ## 响应示例

    ```json
    {
        "channel_id": 9,
        "ip": "0.0.0.0",
        "name": "smoke",
        "port": -1,
        "service_id": 13
    }
    ```

    @@@
    """
    form_data = request.form.to_dict()
    service = Service.from_dict(form_data)
    if service is None:
        return bad_request(_l('missing required field!'))
    if str(service.channel_id) not in [str(c.id) for c in Scheduler.query.all()]:
        return bad_request(_l('%(obj)s is invalid!', obj=service.channel_id))
    if service.name not in list(SERVICES.keys()):
        return bad_request(_l('%(obj)s is invalid!', obj=service.name))
    db.session.add(service)
    db.session.commit()
    return jsonify(service.to_dict())


@bp.route('/service/<int:service_id>', methods=['PATCH'])
@token_auth.login_required
@write_required()
def update_service(service_id):
    """更新检测区域

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 config_api 模块有 W 权限。

    ## 表单字段

    | 字段 | 是否必须 | 类型 | 说明          |
    | ---- | -------- | ---- | ------------- |
    | ip   |          | str  | 默认：0.0.0.0 |
    | port |          | int  | 默认：-1      |

    ## 响应示例

    ```json
    {
        "channel_id": 9,
        "ip": "0.0.0.0",
        "name": "smoke",
        "port": 20001,
        "service_id": 13
    }
    ```

    @@@
    """
    form_data = request.form.to_dict()
    res = Service.query.filter(Service.id == service_id).first()
    if res is None:
        return not_found(_l('%(obj)s not found!', obj=service_id))
    res = Service.from_dict(form_data, res)
    db.session.commit()
    return jsonify(res.to_dict())


@bp.route('/service/<int:service_id>', methods=['DELETE'])
@token_auth.login_required
@write_required()
def delete_service(service_id):
    """删除检测区域

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 config_api 模块有 W 权限。

    ## 响应示例

    ```json
    {
        "channel_id": 9,
        "ip": "0.0.0.0",
        "name": "smoke",
        "port": 20001,
        "service_id": 13
    }
    ```

    @@@
    """
    res = Service.query.filter(Service.id == service_id).first()
    if res is None:
        return not_found(_l('%(obj)s not found!', obj=service_id))
    db.session.delete(res)
    db.session.commit()
    return jsonify(res.to_dict())
