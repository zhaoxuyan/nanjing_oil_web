# -*- coding: utf-8 -*-

import os.path as osp

from flask import request, jsonify
from flask_babel import lazy_gettext as _l


from ... import write_required, db
from ...api import token_auth
from ...beans import Scheduler, Camera
from ...utils import read_json, write_json
from .. import CONFIG_METADATA, SCHEDULERS_CONFIG
from ..utils import schedulers_to_config, config_to_schedulers, config_to_cameras, config_to_services, config_to_masks
from . import bp


@bp.route('/scheduler/export', methods=['POST'])
@token_auth.login_required
@write_required()
def export_schedulers():
    """导出调度程序

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。\n

    ## 权限

    - 访问用户对 config_api 模块有 W 权限。\n

    ## 响应示例

    ```json
    {
        "scheduler": [
            {
                "batch_predict_size": 12,
                "category": "unload",
                "channel_id": 1,
                "channel_name": "unload_1",
                "input_video_streamer": {
                    "buffer_length": 30,
                    "in_url": "/DATACENTER1/test_video/nanjing_unload_1.264",
                    "mat_height": 720,
                    "mat_width": 1280
                },
                "model_services": [
                    {
                        "ip": "0.0.0.0",
                        "port": 20081,
                        "type": "detect"
                    }
                ],
                "start": 1,
                "unload_event": {
                    "unload_area": [
                        {
                            "x_max": 890,
                            "x_min": 337,
                            "y_max": 563,
                            "y_min": 425
                        },
                        {
                            "x_max": 885,
                            "x_min": 361,
                            "y_max": 422,
                            "y_min": 174
                        },
                        {
                            "x_max": 890,
                            "x_min": 337,
                            "y_max": 563,
                            "y_min": 425
                        },
                        {
                            "x_max": 885,
                            "x_min": 361,
                            "y_max": 422,
                            "y_min": 174
                        }
                    ]
                }
            },
            {
                "batch_predict_size": 12,
                "category": "unload",
                "channel_id": 2,
                "channel_name": "unload_2",
                "input_video_streamer": {
                    "buffer_length": 30,
                    "in_url": "/DATACENTER1/test_video/nanjing_unload_2.mp4",
                    "mat_height": 720,
                    "mat_width": 1280
                },
                "model_services": [
                    {
                        "ip": "0.0.0.0",
                        "port": 20081,
                        "type": "detect"
                    }
                ],
                "start": 1,
                "unload_event": {
                    "unload_area": [
                        {
                            "x_max": 890,
                            "x_min": 337,
                            "y_max": 563,
                            "y_min": 425
                        },
                        {
                            "x_max": 885,
                            "x_min": 361,
                            "y_max": 422,
                            "y_min": 174
                        },
                        {
                            "x_max": 890,
                            "x_min": 337,
                            "y_max": 563,
                            "y_min": 425
                        },
                        {
                            "x_max": 885,
                            "x_min": 361,
                            "y_max": 422,
                            "y_min": 174
                        }
                    ]
                }
            },
            {
                "batch_predict_size": 8,
                "category": "checkout",
                "channel_id": 3,
                "channel_name": "checkout_1",
                "checkout_event": {
                    "checkout_area": [
                        {
                            "x_max": 1280,
                            "x_min": 3,
                            "y_max": 717,
                            "y_min": 559
                        },
                        {
                            "x_max": 1280,
                            "x_min": 3,
                            "y_max": 717,
                            "y_min": 559
                        }
                    ]
                },
                "input_video_streamer": {
                    "buffer_length": 30,
                    "in_url": "/DATACENTER1/test_video/nanjing_checkout_1.mp4",
                    "mat_height": 720,
                    "mat_width": 1280
                },
                "model_services": [
                    {
                        "ip": "0.0.0.0",
                        "port": 20082,
                        "type": "detect"
                    }
                ],
                "start": 1
            },
            {
                "batch_predict_size": 8,
                "category": "checkout",
                "channel_id": 4,
                "channel_name": "checkout_2",
                "checkout_event": {
                    "checkout_area": [
                        {
                            "x_max": 448,
                            "x_min": 1,
                            "y_max": 720,
                            "y_min": 1
                        },
                        {
                            "x_max": 698,
                            "x_min": 360,
                            "y_max": 720,
                            "y_min": 193
                        },
                        {
                            "x_max": 448,
                            "x_min": 1,
                            "y_max": 720,
                            "y_min": 1
                        },
                        {
                            "x_max": 698,
                            "x_min": 360,
                            "y_max": 720,
                            "y_min": 193
                        }
                    ]
                },
                "input_video_streamer": {
                    "buffer_length": 30,
                    "in_url": "/DATACENTER1/test_video/nanjing_checkout_2.mp4",
                    "mat_height": 720,
                    "mat_width": 1280
                },
                "model_services": [
                    {
                        "ip": "0.0.0.0",
                        "port": 20082,
                        "type": "detect"
                    }
                ],
                "start": 1
            },
            {
                "batch_predict_size": 12,
                "category": "refuel_side",
                "channel_id": 5,
                "channel_name": "refuel_side_1",
                "input_video_streamer": {
                    "buffer_length": 30,
                    "in_url": "/DATACENTER1/test_video/nanjing_refuel_1.mp4",
                    "mat_height": 720,
                    "mat_width": 1280
                },
                "model_services": [
                    {
                        "ip": "0.0.0.0",
                        "port": 20083,
                        "type": "detect"
                    },
                    {
                        "ip": "0.0.0.0",
                        "port": -1,
                        "type": "smoke"
                    }
                ],
                "refuel_event": {
                    "refuel_area": [
                        {
                            "x_max": 372,
                            "x_min": 169,
                            "y_max": 185,
                            "y_min": 37
                        },
                        {
                            "x_max": 358,
                            "x_min": 61,
                            "y_max": 427,
                            "y_min": 199
                        },
                        {
                            "x_max": 297,
                            "x_min": 3,
                            "y_max": 720,
                            "y_min": 444
                        },
                        {
                            "x_max": 1001,
                            "x_min": 629,
                            "y_max": 375,
                            "y_min": 178
                        },
                        {
                            "x_max": 1261,
                            "x_min": 701,
                            "y_max": 718,
                            "y_min": 399
                        },
                        {
                            "x_max": 893,
                            "x_min": 589,
                            "y_max": 169,
                            "y_min": 57
                        },
                        {
                            "x_max": 372,
                            "x_min": 169,
                            "y_max": 185,
                            "y_min": 37
                        },
                        {
                            "x_max": 358,
                            "x_min": 61,
                            "y_max": 427,
                            "y_min": 199
                        },
                        {
                            "x_max": 297,
                            "x_min": 3,
                            "y_max": 720,
                            "y_min": 444
                        },
                        {
                            "x_max": 1001,
                            "x_min": 629,
                            "y_max": 375,
                            "y_min": 178
                        },
                        {
                            "x_max": 1261,
                            "x_min": 701,
                            "y_max": 718,
                            "y_min": 399
                        },
                        {
                            "x_max": 893,
                            "x_min": 589,
                            "y_max": 169,
                            "y_min": 57
                        }
                    ]
                },
                "start": 1
            },
            {
                "batch_predict_size": 12,
                "category": "refuel_side",
                "channel_id": 6,
                "channel_name": "refuel_side_2",
                "input_video_streamer": {
                    "buffer_length": 30,
                    "in_url": "/DATACENTER1/test_video/nanjing_refuel_2.mp4",
                    "mat_height": 720,
                    "mat_width": 1280
                },
                "model_services": [
                    {
                        "ip": "0.0.0.0",
                        "port": 20083,
                        "type": "detect"
                    },
                    {
                        "ip": "0.0.0.0",
                        "port": -1,
                        "type": "smoke"
                    }
                ],
                "refuel_event": {
                    "refuel_area": [
                        {
                            "x_max": 529,
                            "x_min": 279,
                            "y_max": 375,
                            "y_min": 224
                        },
                        {
                            "x_max": 492,
                            "x_min": 102,
                            "y_max": 720,
                            "y_min": 389
                        },
                        {
                            "x_max": 998,
                            "x_min": 703,
                            "y_max": 367,
                            "y_min": 201
                        },
                        {
                            "x_max": 1201,
                            "x_min": 742,
                            "y_max": 712,
                            "y_min": 373
                        },
                        {
                            "x_max": 529,
                            "x_min": 279,
                            "y_max": 375,
                            "y_min": 224
                        },
                        {
                            "x_max": 492,
                            "x_min": 102,
                            "y_max": 720,
                            "y_min": 389
                        },
                        {
                            "x_max": 998,
                            "x_min": 703,
                            "y_max": 367,
                            "y_min": 201
                        },
                        {
                            "x_max": 1201,
                            "x_min": 742,
                            "y_max": 712,
                            "y_min": 373
                        }
                    ]
                },
                "start": 1
            },
            {
                "batch_predict_size": 12,
                "category": "refuel_side",
                "channel_id": 7,
                "channel_name": "refuel_side_3",
                "input_video_streamer": {
                    "buffer_length": 30,
                    "in_url": "/DATACENTER1/test_video/nanjing_refuel_3.mp4",
                    "mat_height": 720,
                    "mat_width": 1280
                },
                "model_services": [
                    {
                        "ip": "0.0.0.0",
                        "port": 20083,
                        "type": "detect"
                    },
                    {
                        "ip": "0.0.0.0",
                        "port": -1,
                        "type": "smoke"
                    }
                ],
                "refuel_event": {
                    "refuel_area": [
                        {
                            "x_max": 388,
                            "x_min": 158,
                            "y_max": 186,
                            "y_min": 93
                        },
                        {
                            "x_max": 846,
                            "x_min": 540,
                            "y_max": 172,
                            "y_min": 61
                        },
                        {
                            "x_max": 1008,
                            "x_min": 622,
                            "y_max": 381,
                            "y_min": 176
                        },
                        {
                            "x_max": 1271,
                            "x_min": 696,
                            "y_max": 712,
                            "y_min": 401
                        },
                        {
                            "x_max": 304,
                            "x_min": 11,
                            "y_max": 720,
                            "y_min": 442
                        },
                        {
                            "x_max": 393,
                            "x_min": 47,
                            "y_max": 446,
                            "y_min": 191
                        },
                        {
                            "x_max": 388,
                            "x_min": 158,
                            "y_max": 186,
                            "y_min": 93
                        },
                        {
                            "x_max": 846,
                            "x_min": 540,
                            "y_max": 172,
                            "y_min": 61
                        },
                        {
                            "x_max": 1008,
                            "x_min": 622,
                            "y_max": 381,
                            "y_min": 176
                        },
                        {
                            "x_max": 1271,
                            "x_min": 696,
                            "y_max": 712,
                            "y_min": 401
                        },
                        {
                            "x_max": 304,
                            "x_min": 11,
                            "y_max": 720,
                            "y_min": 442
                        },
                        {
                            "x_max": 393,
                            "x_min": 47,
                            "y_max": 446,
                            "y_min": 191
                        }
                    ]
                },
                "start": 1
            },
            {
                "batch_predict_size": 12,
                "category": "refuel_side",
                "channel_id": 8,
                "channel_name": "refuel_side_4",
                "input_video_streamer": {
                    "buffer_length": 30,
                    "in_url": "/DATACENTER1/test_video/nanjing_refuel_4.mp4",
                    "mat_height": 720,
                    "mat_width": 1280
                },
                "model_services": [
                    {
                        "ip": "0.0.0.0",
                        "port": 20083,
                        "type": "detect"
                    },
                    {
                        "ip": "0.0.0.0",
                        "port": -1,
                        "type": "smoke"
                    }
                ],
                "refuel_event": {
                    "refuel_area": [
                        {
                            "x_max": 578,
                            "x_min": 420,
                            "y_max": 325,
                            "y_min": 240
                        },
                        {
                            "x_max": 885,
                            "x_min": 668,
                            "y_max": 306,
                            "y_min": 219
                        },
                        {
                            "x_max": 572,
                            "x_min": 331,
                            "y_max": 468,
                            "y_min": 325
                        },
                        {
                            "x_max": 512,
                            "x_min": 204,
                            "y_max": 720,
                            "y_min": 486
                        },
                        {
                            "x_max": 1133,
                            "x_min": 777,
                            "y_max": 716,
                            "y_min": 451
                        },
                        {
                            "x_max": 992,
                            "x_min": 729,
                            "y_max": 457,
                            "y_min": 302
                        },
                        {
                            "x_max": 578,
                            "x_min": 420,
                            "y_max": 325,
                            "y_min": 240
                        },
                        {
                            "x_max": 885,
                            "x_min": 668,
                            "y_max": 306,
                            "y_min": 219
                        },
                        {
                            "x_max": 572,
                            "x_min": 331,
                            "y_max": 468,
                            "y_min": 325
                        },
                        {
                            "x_max": 512,
                            "x_min": 204,
                            "y_max": 720,
                            "y_min": 486
                        },
                        {
                            "x_max": 1133,
                            "x_min": 777,
                            "y_max": 716,
                            "y_min": 451
                        },
                        {
                            "x_max": 992,
                            "x_min": 729,
                            "y_max": 457,
                            "y_min": 302
                        }
                    ]
                },
                "start": 1
            }
        ]
    }

    ```
    @@@
    """
    payload = schedulers_to_config(Scheduler.query.all())
    config_dir = read_json(CONFIG_METADATA).get('write_config_dir', 'tmp')
    config_path = osp.join(config_dir, SCHEDULERS_CONFIG)
    # fixme: 此处写文件时会遇到没有权限，由于在另一个线程操作，报错不会提示到界面
    write_json(payload, config_path, new_thread=True)
    return jsonify(payload)


@bp.route('/scheduler/import', methods=['POST'])
@token_auth.login_required
@write_required()
def import_schedulers():
    """导入调度程序

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。\n

    ## 权限

    - 访问用户对 config_api 模块有 W 权限。\n

    ## 响应示例

    ```json
    {
        "added_items": [
            {
                "batch": 12,
                "buffer": 30,
                "category_name": "unload",
                "channel_id": 1,
                "height": 720,
                "image_path": "scheduler/screenshot_20200823_134834_635785.jpg",
                "name": "unload_1",
                "start": 1,
                "url": "/DATACENTER1/test_video/nanjing_unload_1.264",
                "width": 1280
            },
            {
                "batch": 12,
                "buffer": 30,
                "category_name": "unload",
                "channel_id": 2,
                "height": 720,
                "image_path": "scheduler/screenshot_20200823_134834_788585.jpg",
                "name": "unload_2",
                "start": 1,
                "url": "/DATACENTER1/test_video/nanjing_unload_2.mp4",
                "width": 1280
            },
            {
                "batch": 8,
                "buffer": 30,
                "category_name": "checkout",
                "channel_id": 3,
                "height": 720,
                "image_path": "scheduler/screenshot_20200823_134834_935873.jpg",
                "name": "checkout_1",
                "start": 1,
                "url": "/DATACENTER1/test_video/nanjing_checkout_1.mp4",
                "width": 1280
            },
            {
                "batch": 8,
                "buffer": 30,
                "category_name": "checkout",
                "channel_id": 4,
                "height": 720,
                "image_path": "scheduler/screenshot_20200823_134835_084294.jpg",
                "name": "checkout_2",
                "start": 1,
                "url": "/DATACENTER1/test_video/nanjing_checkout_2.mp4",
                "width": 1280
            },
            {
                "batch": 12,
                "buffer": 30,
                "category_name": "refuel_side",
                "channel_id": 5,
                "height": 720,
                "image_path": "scheduler/screenshot_20200823_134835_225931.jpg",
                "name": "refuel_side_1",
                "start": 1,
                "url": "/DATACENTER1/test_video/nanjing_refuel_1.mp4",
                "width": 1280
            },
            {
                "batch": 12,
                "buffer": 30,
                "category_name": "refuel_side",
                "channel_id": 6,
                "height": 720,
                "image_path": "scheduler/screenshot_20200823_134835_400288.jpg",
                "name": "refuel_side_2",
                "start": 1,
                "url": "/DATACENTER1/test_video/nanjing_refuel_2.mp4",
                "width": 1280
            },
            {
                "batch": 12,
                "buffer": 30,
                "category_name": "refuel_side",
                "channel_id": 7,
                "height": 720,
                "image_path": "scheduler/screenshot_20200823_134835_531008.jpg",
                "name": "refuel_side_3",
                "start": 1,
                "url": "/DATACENTER1/test_video/nanjing_refuel_3.mp4",
                "width": 1280
            },
            {
                "batch": 12,
                "buffer": 30,
                "category_name": "refuel_side",
                "channel_id": 8,
                "height": 720,
                "image_path": "scheduler/screenshot_20200823_134835_706416.jpg",
                "name": "refuel_side_4",
                "start": 1,
                "url": "/DATACENTER1/test_video/nanjing_refuel_4.mp4",
                "width": 1280
            }
        ]
    }

    ```
    @@@
    """
    config_dir = read_json(CONFIG_METADATA).get('read_config_dir', 'data/config')
    config_path = osp.join(config_dir, SCHEDULERS_CONFIG)
    payload = read_json(config_path)
    # 先新增摄像头
    cameras = config_to_cameras(payload)
    ready_add_items = []
    for c in cameras:
        if Camera.query.filter(Camera.url == c.url).count() == 0:
            ready_add_items.append(c)
    db.session.add_all(ready_add_items)
    db.session.commit()
    # 新增调度调度程序
    schedulers = config_to_schedulers(payload)
    ready_add_items = []
    for s in schedulers:
        if Scheduler.query.filter(Scheduler.id == s.id).count() == 0:
            ready_add_items.append(s)
    db.session.add_all(ready_add_items)
    db.session.commit()
    return jsonify({'added_items': [item.to_dict() for item in ready_add_items]})


@bp.route('/service/import', methods=['POST'])
@token_auth.login_required
@write_required()
def import_services():
    """导入模型服务

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。\n

    ## 权限

    - 访问用户对 config_api 模块有 W 权限。\n

    ## 响应示例

    ```json
    {
        "added_items": [
            {
                "channel_id": 1,
                "ip": "0.0.0.0",
                "name": "detect",
                "port": 20081,
                "service_id": 1
            },
            {
                "channel_id": 2,
                "ip": "0.0.0.0",
                "name": "detect",
                "port": 20081,
                "service_id": 2
            },
            {
                "channel_id": 3,
                "ip": "0.0.0.0",
                "name": "detect",
                "port": 20082,
                "service_id": 3
            },
            {
                "channel_id": 4,
                "ip": "0.0.0.0",
                "name": "detect",
                "port": 20082,
                "service_id": 4
            },
            {
                "channel_id": 5,
                "ip": "0.0.0.0",
                "name": "detect",
                "port": 20083,
                "service_id": 5
            },
            {
                "channel_id": 5,
                "ip": "0.0.0.0",
                "name": "smoke",
                "port": -1,
                "service_id": 6
            },
            {
                "channel_id": 6,
                "ip": "0.0.0.0",
                "name": "detect",
                "port": 20083,
                "service_id": 7
            },
            {
                "channel_id": 6,
                "ip": "0.0.0.0",
                "name": "smoke",
                "port": -1,
                "service_id": 8
            },
            {
                "channel_id": 7,
                "ip": "0.0.0.0",
                "name": "detect",
                "port": 20083,
                "service_id": 9
            },
            {
                "channel_id": 7,
                "ip": "0.0.0.0",
                "name": "smoke",
                "port": -1,
                "service_id": 10
            },
            {
                "channel_id": 8,
                "ip": "0.0.0.0",
                "name": "detect",
                "port": 20083,
                "service_id": 11
            },
            {
                "channel_id": 8,
                "ip": "0.0.0.0",
                "name": "smoke",
                "port": -1,
                "service_id": 12
            }
        ]
    }

    ```
    @@@
    """
    config_dir = read_json(CONFIG_METADATA).get('read_config_dir', 'data/config')
    config_path = osp.join(config_dir, SCHEDULERS_CONFIG)
    payload = read_json(config_path)
    # 先新增摄像头
    cameras = config_to_cameras(payload)
    ready_add_items = []
    for c in cameras:
        if Camera.query.filter(Camera.url == c.url).count() == 0:
            ready_add_items.append(c)
    db.session.add_all(ready_add_items)
    db.session.commit()
    # 先新增调度程序
    schedulers = config_to_schedulers(payload)
    ready_add_items = []
    for s in schedulers:
        if Scheduler.query.filter(Scheduler.id == s.id).count() == 0:
            ready_add_items.append(s)
    db.session.add_all(ready_add_items)
    db.session.commit()
    # 新增模型服务
    ready_add_items = config_to_services(payload)
    db.session.add_all(ready_add_items)
    db.session.commit()
    return jsonify({'added_items': [item.to_dict() for item in ready_add_items]})


@bp.route('/mask/import', methods=['POST'])
@token_auth.login_required
@write_required()
def import_masks():
    """导入检测区域

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。\n

    ## 权限

    - 访问用户对 config_api 模块有 W 权限。\n

    ## 响应示例

    ```json

    {
        "added_items": [
            {
                "channel_id": 1,
                "mask_id": 1,
                "name": "unload_event.unload_area",
                "x_max": 890,
                "x_min": 337,
                "y_max": 563,
                "y_min": 425
            },
            {
                "channel_id": 1,
                "mask_id": 2,
                "name": "unload_event.unload_area",
                "x_max": 885,
                "x_min": 361,
                "y_max": 422,
                "y_min": 174
            },
            {
                "channel_id": 2,
                "mask_id": 3,
                "name": "unload_event.unload_area",
                "x_max": 890,
                "x_min": 337,
                "y_max": 563,
                "y_min": 425
            },
            {
                "channel_id": 2,
                "mask_id": 4,
                "name": "unload_event.unload_area",
                "x_max": 885,
                "x_min": 361,
                "y_max": 422,
                "y_min": 174
            },
            {
                "channel_id": 3,
                "mask_id": 5,
                "name": "checkout_event.checkout_area",
                "x_max": 1280,
                "x_min": 3,
                "y_max": 717,
                "y_min": 559
            },
            {
                "channel_id": 4,
                "mask_id": 6,
                "name": "checkout_event.checkout_area",
                "x_max": 448,
                "x_min": 1,
                "y_max": 720,
                "y_min": 1
            },
            {
                "channel_id": 4,
                "mask_id": 7,
                "name": "checkout_event.checkout_area",
                "x_max": 698,
                "x_min": 360,
                "y_max": 720,
                "y_min": 193
            },
            {
                "channel_id": 5,
                "mask_id": 8,
                "name": "refuel_event.refuel_area",
                "x_max": 372,
                "x_min": 169,
                "y_max": 185,
                "y_min": 37
            },
            {
                "channel_id": 5,
                "mask_id": 9,
                "name": "refuel_event.refuel_area",
                "x_max": 358,
                "x_min": 61,
                "y_max": 427,
                "y_min": 199
            },
            {
                "channel_id": 5,
                "mask_id": 10,
                "name": "refuel_event.refuel_area",
                "x_max": 297,
                "x_min": 3,
                "y_max": 720,
                "y_min": 444
            },
            {
                "channel_id": 5,
                "mask_id": 11,
                "name": "refuel_event.refuel_area",
                "x_max": 1001,
                "x_min": 629,
                "y_max": 375,
                "y_min": 178
            },
            {
                "channel_id": 5,
                "mask_id": 12,
                "name": "refuel_event.refuel_area",
                "x_max": 1261,
                "x_min": 701,
                "y_max": 718,
                "y_min": 399
            },
            {
                "channel_id": 5,
                "mask_id": 13,
                "name": "refuel_event.refuel_area",
                "x_max": 893,
                "x_min": 589,
                "y_max": 169,
                "y_min": 57
            },
            {
                "channel_id": 6,
                "mask_id": 14,
                "name": "refuel_event.refuel_area",
                "x_max": 529,
                "x_min": 279,
                "y_max": 375,
                "y_min": 224
            },
            {
                "channel_id": 6,
                "mask_id": 15,
                "name": "refuel_event.refuel_area",
                "x_max": 492,
                "x_min": 102,
                "y_max": 720,
                "y_min": 389
            },
            {
                "channel_id": 6,
                "mask_id": 16,
                "name": "refuel_event.refuel_area",
                "x_max": 998,
                "x_min": 703,
                "y_max": 367,
                "y_min": 201
            },
            {
                "channel_id": 6,
                "mask_id": 17,
                "name": "refuel_event.refuel_area",
                "x_max": 1201,
                "x_min": 742,
                "y_max": 712,
                "y_min": 373
            },
            {
                "channel_id": 7,
                "mask_id": 18,
                "name": "refuel_event.refuel_area",
                "x_max": 388,
                "x_min": 158,
                "y_max": 186,
                "y_min": 93
            },
            {
                "channel_id": 7,
                "mask_id": 19,
                "name": "refuel_event.refuel_area",
                "x_max": 846,
                "x_min": 540,
                "y_max": 172,
                "y_min": 61
            },
            {
                "channel_id": 7,
                "mask_id": 20,
                "name": "refuel_event.refuel_area",
                "x_max": 1008,
                "x_min": 622,
                "y_max": 381,
                "y_min": 176
            },
            {
                "channel_id": 7,
                "mask_id": 21,
                "name": "refuel_event.refuel_area",
                "x_max": 1271,
                "x_min": 696,
                "y_max": 712,
                "y_min": 401
            },
            {
                "channel_id": 7,
                "mask_id": 22,
                "name": "refuel_event.refuel_area",
                "x_max": 304,
                "x_min": 11,
                "y_max": 720,
                "y_min": 442
            },
            {
                "channel_id": 7,
                "mask_id": 23,
                "name": "refuel_event.refuel_area",
                "x_max": 393,
                "x_min": 47,
                "y_max": 446,
                "y_min": 191
            },
            {
                "channel_id": 8,
                "mask_id": 24,
                "name": "refuel_event.refuel_area",
                "x_max": 578,
                "x_min": 420,
                "y_max": 325,
                "y_min": 240
            },
            {
                "channel_id": 8,
                "mask_id": 25,
                "name": "refuel_event.refuel_area",
                "x_max": 885,
                "x_min": 668,
                "y_max": 306,
                "y_min": 219
            },
            {
                "channel_id": 8,
                "mask_id": 26,
                "name": "refuel_event.refuel_area",
                "x_max": 572,
                "x_min": 331,
                "y_max": 468,
                "y_min": 325
            },
            {
                "channel_id": 8,
                "mask_id": 27,
                "name": "refuel_event.refuel_area",
                "x_max": 512,
                "x_min": 204,
                "y_max": 720,
                "y_min": 486
            },
            {
                "channel_id": 8,
                "mask_id": 28,
                "name": "refuel_event.refuel_area",
                "x_max": 1133,
                "x_min": 777,
                "y_max": 716,
                "y_min": 451
            },
            {
                "channel_id": 8,
                "mask_id": 29,
                "name": "refuel_event.refuel_area",
                "x_max": 992,
                "x_min": 729,
                "y_max": 457,
                "y_min": 302
            }
        ]
    }

    ```
    @@@
    """
    config_dir = read_json(CONFIG_METADATA).get('read_config_dir', 'data/config')
    config_path = osp.join(config_dir, SCHEDULERS_CONFIG)
    payload = read_json(config_path)
    # 先新增摄像头
    cameras = config_to_cameras(payload)
    ready_add_items = []
    for c in cameras:
        if Camera.query.filter(Camera.url == c.url).count() == 0:
            ready_add_items.append(c)
    db.session.add_all(ready_add_items)
    db.session.commit()
    # 先新增调度程序
    schedulers = config_to_schedulers(payload)
    ready_add_schedulers = []
    for s in schedulers:
        if Scheduler.query.filter(Scheduler.id == s.id).count() == 0:
            ready_add_schedulers.append(s)
    db.session.add_all(ready_add_schedulers)
    db.session.commit()
    # 新增检测区域
    ready_add_items = config_to_masks(payload)
    db.session.add_all(ready_add_items)
    db.session.commit()
    return jsonify({'added_items': [item.to_dict() for item in ready_add_items]})
