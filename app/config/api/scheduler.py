# -*- coding: utf-8 -*-

import os.path as osp

from flask import request, jsonify
from flask_babel import lazy_gettext as _l

from ... import db, read_required, write_required, RECORDS_PER_PAGE, RECORDS_MAX_PER_PAGE
from ...api import token_auth, bad_request, not_found
from ...beans import Scheduler, Category, Camera
from . import bp


@bp.route('/scheduler', methods=['GET'])
@token_auth.login_required
@read_required()
def list_schedulers():
    """获取调度程序

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 config_api 模块有 R 权限。

    ## 查询字段

    | 字段          | 是否必须 | 类型 | 说明                           |
    | ------------- | -------- | ---- | ------------------------------ |
    | channel_id    |          | int  |                                |
    | name          |          | str  | 模糊查询                       |
    | category_name |          | str  | 参考 `/bean/api/category` 接口 |
    | url           |          | str  | 模糊查询                       |
    | page          |          | int  | 默认：1                        |

    ## 响应示例

    ```json
    {
        "_links": {
            "next": null,
            "prev": null,
            "self": "/api/scheduler?page=1&per_page=25"
        },
        "_meta": {
            "page": 1,
            "per_page": 25,
            "total_items": 8,
            "total_pages": 1
        },
        "items": [
            {
                "batch": 12,
                "buffer": 30,
                "category_name": "unload",
                "channel_id": 1,
                "height": 720,
                "image_path": "scheduler/screenshot_20200823_140727_282246.jpg",
                "name": "unload_1",
                "start": 1,
                "url": "/DATACENTER1/test_video/nanjing_unload_1.264",
                "width": 1280
            },
            {
                "batch": 12,
                "buffer": 30,
                "category_name": "unload",
                "channel_id": 2,
                "height": 720,
                "image_path": "scheduler/screenshot_20200823_140727_451891.jpg",
                "name": "unload_2",
                "start": 1,
                "url": "/DATACENTER1/test_video/nanjing_unload_2.mp4",
                "width": 1280
            },
            {
                "batch": 8,
                "buffer": 30,
                "category_name": "checkout",
                "channel_id": 3,
                "height": 720,
                "image_path": "scheduler/screenshot_20200823_140727_629238.jpg",
                "name": "checkout_1",
                "start": 1,
                "url": "/DATACENTER1/test_video/nanjing_checkout_1.mp4",
                "width": 1280
            },
            {
                "batch": 8,
                "buffer": 30,
                "category_name": "checkout",
                "channel_id": 4,
                "height": 720,
                "image_path": "scheduler/screenshot_20200823_140727_754576.jpg",
                "name": "checkout_2",
                "start": 1,
                "url": "/DATACENTER1/test_video/nanjing_checkout_2.mp4",
                "width": 1280
            },
            {
                "batch": 12,
                "buffer": 30,
                "category_name": "refuel_side",
                "channel_id": 5,
                "height": 720,
                "image_path": "scheduler/screenshot_20200823_140727_924992.jpg",
                "name": "refuel_side_1",
                "start": 1,
                "url": "/DATACENTER1/test_video/nanjing_refuel_1.mp4",
                "width": 1280
            },
            {
                "batch": 12,
                "buffer": 30,
                "category_name": "refuel_side",
                "channel_id": 6,
                "height": 720,
                "image_path": "scheduler/screenshot_20200823_140728_081785.jpg",
                "name": "refuel_side_2",
                "start": 1,
                "url": "/DATACENTER1/test_video/nanjing_refuel_2.mp4",
                "width": 1280
            },
            {
                "batch": 12,
                "buffer": 30,
                "category_name": "refuel_side",
                "channel_id": 7,
                "height": 720,
                "image_path": "scheduler/screenshot_20200823_140728_238593.jpg",
                "name": "refuel_side_3",
                "start": 1,
                "url": "/DATACENTER1/test_video/nanjing_refuel_3.mp4",
                "width": 1280
            },
            {
                "batch": 12,
                "buffer": 30,
                "category_name": "refuel_side",
                "channel_id": 8,
                "height": 720,
                "image_path": "scheduler/screenshot_20200823_140728_403088.jpg",
                "name": "refuel_side_4",
                "start": 1,
                "url": "/DATACENTER1/test_video/nanjing_refuel_4.mp4",
                "width": 1280
            }
        ]
    }
    ```

    @@@
    """
    channel_id = request.args.get('channel_id', None, type=int)
    name = request.args.get('name', None, type=str)
    category_name = request.args.get('category_name', None, type=str)
    url = request.args.get('url', None, type=str)
    page = request.args.get('page', 1, type=int)
    per_page = request.args.get('per_page', RECORDS_PER_PAGE, type=int)
    per_page = min(per_page, RECORDS_MAX_PER_PAGE)
    custom_query = Scheduler.query
    if channel_id is not None:
        custom_query = custom_query.filter(Scheduler.id == channel_id)
    if name is not None:
        custom_query = custom_query.filter(Scheduler.name.like('%' + name + '%'))
    if category_name is not None:
        custom_query = custom_query.filter(Scheduler.category_name == category_name)
    if url is not None:
        custom_query = custom_query.filter(Scheduler.url.like('%' + url + '%'))
    custom_query = custom_query.order_by(Scheduler.id.asc())
    payload = Scheduler.to_collection_dict(custom_query, page, per_page, 'config_api.list_schedulers',
        channel_id=channel_id, name=name, category_name=category_name, url=url)  # NOQA
    return jsonify(payload)


@bp.route('/scheduler/add', methods=['POST'])
@token_auth.login_required
@write_required()
def add_scheduler():
    """新增调度程序

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 config_api 模块有 W 权限。

    ## 表单字段

    | 字段          | 是否必须 | 类型 | 说明                                       |
    | ------------- | -------- | ---- | ------------------------------------------ |
    | name          | 是       | str  | 名称                                       |
    | category_name | 是       | str  | 场景，参考 `/bean/api/category` 接口       |
    | url           | 是       | str  | 摄像头地址，参考 `/config/api/camera` 接口 |
    | start         |          | int  | 是否启动，默认：0                          |
    | batch         |          | int  | 跳检帧数，默认：4                          |
    | height        |          | int  | 高，默认：720                              |
    | width         |          | int  | 宽，默认：1280                             |
    | buffer        |          | int  | 缓冲，默认：30                             |

    ## 响应示例

    ```json
    {
        "batch": 4,
        "buffer": 30,
        "category_name": "unload",
        "channel_id": 9,
        "height": 720,
        "image_path": "scheduler/screenshot_20200823_142758_613717.jpg",
        "name": "unload_3",
        "start": 0,
        "url": "rtsp://admin:swjtu9422@192.168.9.32",
        "width": 1280
    }
    ```

    @@@
    """
    form_data = request.form.to_dict()
    scheduler = Scheduler.from_dict(form_data)
    if scheduler is None:
        return bad_request(_l('missing required field!'))
    if Scheduler.query.filter(Scheduler.name == scheduler.name).count() - 0:
        bad_request(_l('%(obj)s already exists!', obj=scheduler.name))
    if scheduler.category_name not in [c.name for c in Category.query.all()]:
        return bad_request(_l('%(obj)s is invalid!', obj=scheduler.category_name))
    if scheduler.url not in [c.url for c in Camera.query.all()]:
        return bad_request(_l('%(obj)s is invalid!', obj=scheduler.url))
    scheduler.image_path = osp.join('scheduler', osp.split(scheduler.capture_screenshot())[1])
    db.session.add(scheduler)
    db.session.commit()
    return jsonify(scheduler.to_dict())


@bp.route('/scheduler/<int:channel_id>', methods=['PATCH'])
@token_auth.login_required
@write_required()
def update_scheduler(channel_id):
    """更新调度程序

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 config_api 模块有 W 权限。

    ## 表单字段

    | 字段   | 是否必须 | 类型 | 说明              |
    | ------ | -------- | ---- | ----------------- |
    | start  |          | int  | 是否启动，默认：0 |
    | batch  |          | int  | 跳检帧数，默认：4 |
    | height |          | int  | 高，默认：720     |
    | width  |          | int  | 宽，默认：1280    |
    | buffer |          | int  | 缓冲，默认：30    |

    ## 响应示例

    ```json
    {
        "batch": 4,
        "buffer": 30,
        "category_name": "unload",
        "channel_id": 9,
        "height": 720,
        "image_path": "scheduler/screenshot_20200823_142944_844601.jpg",
        "name": "unload_3",
        "start": 1,
        "url": "rtsp://admin:swjtu9422@192.168.9.32",
        "width": 1280
    }
    ```

    @@@
    """
    form_data = request.form.to_dict()
    res = Scheduler.query.filter(Scheduler.id == channel_id).first()
    if res is None:
        return not_found(_l('%(obj)s not found!', obj=channel_id))
    res = Scheduler.from_dict(form_data, res)
    res.image_path = osp.join('scheduler', osp.split(res.capture_screenshot())[1])
    db.session.commit()
    return jsonify(res.to_dict())


@bp.route('/scheduler/<int:channel_id>', methods=['DELETE'])
@token_auth.login_required
@write_required()
def delete_scheduler(channel_id):
    """删除调度程序

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 config_api 模块有 W 权限。

    ## 响应示例

    ```json
    {
        "batch": 4,
        "buffer": 30,
        "category_name": "unload",
        "channel_id": 9,
        "height": 720,
        "image_path": "scheduler/screenshot_20200823_142944_844601.jpg",
        "name": "unload_3",
        "start": 1,
        "url": "rtsp://admin:swjtu9422@192.168.9.32",
        "width": 1280
    }
    ```

    @@@
    """
    res = Scheduler.query.filter(Scheduler.id == channel_id).first()
    if res is None:
        return not_found(_l('%(obj)s not found!', obj=channel_id))
    if res.services.count() - 0 or res.masks.count() - 0 or res.event_logs.count() - 0 or \
            res.state_logs.count() - 0 or res.crash_logs.count() - 0:
        return bad_request(_l('%(obj)s in used!', obj=channel_id))
    db.session.delete(res)
    db.session.commit()
    return jsonify(res.to_dict())
