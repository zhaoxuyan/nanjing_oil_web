# -*- coding: utf-8 -*-

import os.path as osp

from flask import request, jsonify
from flask_babel import lazy_gettext as _l

from ... import db, read_required, write_required, RECORDS_PER_PAGE, RECORDS_MAX_PER_PAGE
from ...api import token_auth, bad_request, not_found
from ...beans import Camera
from ...utils import check_rtsp
from . import bp


@bp.route('/camera', methods=['GET'])
@token_auth.login_required
@read_required()
def list_cameras():
    """获取摄像头

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 config_api 模块有 R 权限。

    ## 查询字段

    | 字段      | 是否必须 | 类型 | 说明     |
    | --------- | -------- | ---- | -------- |
    | camera_id |          | int  |          |
    | url       |          | str  | 模糊查询 |
    | page      |          | int  | 默认：1  |
    | per_page  |          | int  | 默认：25 |

    ## 响应示例

    ```json
        {
        "_links": {
            "next": null,
            "prev": null,
            "self": "/api/camera?page=1&per_page=25"
        },
        "_meta": {
            "page": 1,
            "per_page": 25,
            "total_items": 8,
            "total_pages": 1
        },
        "items": [
            {
                "id": 1,
                "url": "/DATACENTER1/test_video/nanjing_unload_1.264"
            },
            {
                "id": 2,
                "url": "/DATACENTER1/test_video/nanjing_unload_2.mp4"
            },
            {
                "id": 3,
                "url": "/DATACENTER1/test_video/nanjing_checkout_1.mp4"
            },
            {
                "id": 4,
                "url": "/DATACENTER1/test_video/nanjing_checkout_2.mp4"
            },
            {
                "id": 5,
                "url": "/DATACENTER1/test_video/nanjing_refuel_1.mp4"
            },
            {
                "id": 6,
                "url": "/DATACENTER1/test_video/nanjing_refuel_2.mp4"
            },
            {
                "id": 7,
                "url": "/DATACENTER1/test_video/nanjing_refuel_3.mp4"
            },
            {
                "id": 8,
                "url": "/DATACENTER1/test_video/nanjing_refuel_4.mp4"
            }
        ]
    }
    ```

    @@@
    """
    camera_id = request.args.get('camera_id', None, type=int)
    url = request.args.get('url', None, type=str)
    page = request.args.get('page', 1, type=int)
    per_page = request.args.get('per_page', RECORDS_PER_PAGE, type=int)
    per_page = min(per_page, RECORDS_MAX_PER_PAGE)
    custom_query = Camera.query
    if camera_id is not None:
        custom_query = custom_query.filter(Camera.id == camera_id)
    if url is not None:
        custom_query = custom_query.filter(Camera.url.like('%' + url + '%'))
    custom_query = custom_query.order_by(Camera.id.asc())
    payload = Camera.to_collection_dict(custom_query, page, per_page, 'config_api.list_cameras',
        camera_id=camera_id, url=url)  # NOQA
    return jsonify(payload)


@bp.route('/camera/add', methods=['POST'])
@token_auth.login_required
@write_required()
def add_camera():
    """新增摄像头

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 config_api 模块有 W 权限。

    ## 表单字段

    | 字段 | 是否必须 | 类型 | 说明       |
    | ---- | -------- | ---- | ---------- |
    | url  | 是       | str  | 摄像头地址 |

    ## 响应示例

    ```json
    {
        "id": 9,
        "url": "app/static/logo/bg.png"
    }

    ```
    @@@
    """
    form_data = request.form.to_dict()
    camera = Camera.from_dict(form_data)
    if camera is None:
        return bad_request(_l('missing required field!'))
    if not (check_rtsp(camera.url) or osp.isfile(camera.url)):
        return bad_request(_l('%(obj)s is invalid!', obj=camera.url))
    if Camera.query.filter(Camera.url == camera.url).count() - 0:
        return bad_request(_l('%(obj)s already exists!', obj=camera.url))
    db.session.add(camera)
    db.session.commit()
    return jsonify(camera.to_dict())


@bp.route('/camera/<int:camera_id>', methods=['DELETE'])
@token_auth.login_required
@write_required()
def delete_camera(camera_id):
    """删除摄像头

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 config_api 模块有 W 权限。

    ## 响应示例

    ```json
    {
        "id": 9,
        "url": "app/static/logo/bg.png"
    }
    ```

    @@@
    """
    res = Camera.query.filter(Camera.id == camera_id).first()
    if res is None:
        return not_found(_l('%(obj)s not found!', obj=camera_id))
    if res.schedulers.count() - 0:
        return bad_request(_l('%(obj)s in used!', obj=camera_id))
    db.session.delete(res)
    db.session.commit()
    return jsonify(res.to_dict())
