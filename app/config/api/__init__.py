# -*- coding: utf-8 -*-

from flask import Blueprint
bp = Blueprint('config_api', __name__)

from . import camera  # NOQA
from . import camera_scan  # NOQA
from . import scheduler  # NOQA
from . import service  # NOQA
from . import mask  # NOQA
from . import import_and_export  # NOQA
