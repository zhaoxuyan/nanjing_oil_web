# -*- coding: utf-8 -*-

from datetime import datetime

from flask import request, jsonify
from flask_babel import lazy_gettext as _l

from ... import db, write_required, read_required
from ...api import token_auth, bad_request, not_found
from ...beans import Camera
from ...utils import RTSP_PORT, scan_rtsp, write_json, read_json, DATETIME_FORMATTER
from .. import CAMERA_SCAN_RESULT
from . import bp


@bp.route('/camera/scan', methods=['POST'])
@token_auth.login_required
@write_required()
def scan_camera():
    """扫描摄像头

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。\n

    ## 权限

    - 访问用户对 config_api 模块有 W 权限。\n

    ## 表单字段

    | 字段       | 是否必须 | 类型 | 说明                      |
    | ---------- | -------- | ---- | ------------------------- |
    | ip_address | 是       | str  | ip 地址，用于获取扫描网段 |
    | user       |          | str  | 用户名                    |
    | pwd        |          | str  | 密码                      |
    | port       |          | str  | 端口，默认：554           |

    ## 响应示例

    ```json
    {
        "cameras": [
            {
                "image_path": "tmp/screenshot_20200823_141331_676995.jpg",
                "in_database": 0,
                "rtsp_url": "rtsp://admin:swjtu9422@192.168.9.32"
            },
            {
                "image_path": "tmp/screenshot_20200823_141330_490186.jpg",
                "in_database": 0,
                "rtsp_url": "rtsp://admin:swjtu9422@192.168.9.35"
            }
        ],
        "timestamp": "2020-08-23 14:16:31"
    }
    ```

    @@@
    """
    ip_address = request.form.get('ip_address')
    user = request.form.get('user')
    pwd = request.form.get('pwd')
    port = request.form.get('port') if request.form.get('port') != RTSP_PORT else None
    if not ip_address:
        return bad_request(_l('missing required field!'))
    results = []
    results = scan_rtsp(ip_address, user, pwd, port)
    for item in results:
        res = Camera.query.filter(Camera.url == item.get('rtsp_url')).first()
        item['in_database'] = 1 if res else 0
    # 为了防止反复扫描摄像头消耗Web后台资源，把每次摄像头扫描结果存到文件中
    payload = {
        'cameras': results,
        'timestamp': datetime.strftime(datetime.utcnow(), DATETIME_FORMATTER)
    }
    write_json(payload, CAMERA_SCAN_RESULT, new_thread=True)
    return jsonify(payload)


@bp.route('/camera/scan_history', methods=['GET'])
@token_auth.login_required
@read_required()
def scan_camera_history():
    """获取摄像头扫描历史

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。\n

    ## 权限

    - 访问用户对 config_api 模块有 R 权限。\n

    ## 响应示例

    ```json
    {
        "cameras": [
            {
                "image_path": "tmp/screenshot_20200823_141331_676995.jpg",
                "in_database": 1,
                "rtsp_url": "rtsp://admin:swjtu9422@192.168.9.32"
            },
            {
                "image_path": "tmp/screenshot_20200823_141330_490186.jpg",
                "in_database": 1,
                "rtsp_url": "rtsp://admin:swjtu9422@192.168.9.35"
            }
        ],
        "timestamp": "2020-08-23 14:16:31"
    }
    ```

    @@@
    """
    payload = read_json(CAMERA_SCAN_RESULT)
    if not payload:
        return not_found(_l('last scan results not found!'))
    for item in payload.get('cameras'):
        res = Camera.query.filter(Camera.url == item.get('rtsp_url')).first()
        item['in_database'] = 1 if res else 0
    write_json(payload, CAMERA_SCAN_RESULT, new_thread=True)
    return jsonify(payload)


@bp.route('/camera/add_history', methods=['POST'])
@token_auth.login_required
@write_required()
def add_camera_history():
    """新增摄像头扫描历史

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。\n

    ## 权限

    - 访问用户对 config_api 模块有 W 权限。\n

    ## 响应示例

    ```json
    {
        "added_items": [
            {
                "id": 9,
                "url": "rtsp://admin:swjtu9422@192.168.9.32"
            },
            {
                "id": 10,
                "url": "rtsp://admin:swjtu9422@192.168.9.35"
            }
        ]
    }
    ```

    @@@
    """
    payload = read_json(CAMERA_SCAN_RESULT)
    if not payload:
        return not_found(_l('last scan results not found!'))
    ready_add_items = []
    for item in payload.get('cameras'):
        if Camera.query.filter(Camera.url == item.get('rtsp_url')).count() == 0:
            camera = Camera(url=item.get('rtsp_url'))
            ready_add_items.append(camera)
    db.session.add_all(ready_add_items)
    db.session.commit()
    payload = {
        'cameras': [c.to_dict() for c in ready_add_items],
        'timestamp': datetime.strftime(datetime.utcnow(), DATETIME_FORMATTER)
    }
    return jsonify({'added_items': [item.to_dict() for item in ready_add_items]})
