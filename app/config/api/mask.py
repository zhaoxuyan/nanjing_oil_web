# -*- coding: utf-8 -*-

from flask import request, jsonify
from flask_babel import lazy_gettext as _l

from ... import db, read_required, write_required, RECORDS_PER_PAGE, RECORDS_MAX_PER_PAGE
from ...api import token_auth, bad_request, not_found
from ...beans import Mask, Scheduler
from . import bp


@bp.route('/mask', methods=['GET'])
@token_auth.login_required
@read_required()
def list_masks():
    """获取检测区域

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 config_api 模块有 R 权限。


    ## 查询字段

    | 字段       | 是否必须 | 类型 | 说明                              |
    | ---------- | -------- | ---- | --------------------------------- |
    | mask_id    |          | int  |                                   |
    | channel_id |          | int  | 参考 `/config/api/scheduler` 接口 |
    | name       |          | str  | 模糊查询                          |
    | page       |          | int  | 默认：1                           |
    | per_page   |          | int  | 默认：25                          |

    ## 响应示例

    ```json
    {
        "_links": {
            "next": "/api/mask?page=2&per_page=25",
            "prev": null,
            "self": "/api/mask?page=1&per_page=25"
        },
        "_meta": {
            "page": 1,
            "per_page": 25,
            "total_items": 29,
            "total_pages": 2
        },
        "items": [
            {
                "channel_id": 1,
                "mask_id": 1,
                "name": "unload_event.unload_area",
                "x_max": 890,
                "x_min": 337,
                "y_max": 563,
                "y_min": 425
            },
            {
                "channel_id": 1,
                "mask_id": 2,
                "name": "unload_event.unload_area",
                "x_max": 885,
                "x_min": 361,
                "y_max": 422,
                "y_min": 174
            },
            {
                "channel_id": 2,
                "mask_id": 3,
                "name": "unload_event.unload_area",
                "x_max": 890,
                "x_min": 337,
                "y_max": 563,
                "y_min": 425
            },
            {
                "channel_id": 2,
                "mask_id": 4,
                "name": "unload_event.unload_area",
                "x_max": 885,
                "x_min": 361,
                "y_max": 422,
                "y_min": 174
            },
            {
                "channel_id": 3,
                "mask_id": 5,
                "name": "checkout_event.checkout_area",
                "x_max": 1280,
                "x_min": 3,
                "y_max": 717,
                "y_min": 559
            },
            {
                "channel_id": 4,
                "mask_id": 6,
                "name": "checkout_event.checkout_area",
                "x_max": 448,
                "x_min": 1,
                "y_max": 720,
                "y_min": 1
            },
            {
                "channel_id": 4,
                "mask_id": 7,
                "name": "checkout_event.checkout_area",
                "x_max": 698,
                "x_min": 360,
                "y_max": 720,
                "y_min": 193
            },
            {
                "channel_id": 5,
                "mask_id": 8,
                "name": "refuel_event.refuel_area",
                "x_max": 372,
                "x_min": 169,
                "y_max": 185,
                "y_min": 37
            },
            {
                "channel_id": 5,
                "mask_id": 9,
                "name": "refuel_event.refuel_area",
                "x_max": 358,
                "x_min": 61,
                "y_max": 427,
                "y_min": 199
            },
            {
                "channel_id": 5,
                "mask_id": 10,
                "name": "refuel_event.refuel_area",
                "x_max": 297,
                "x_min": 3,
                "y_max": 720,
                "y_min": 444
            },
            {
                "channel_id": 5,
                "mask_id": 11,
                "name": "refuel_event.refuel_area",
                "x_max": 1001,
                "x_min": 629,
                "y_max": 375,
                "y_min": 178
            },
            {
                "channel_id": 5,
                "mask_id": 12,
                "name": "refuel_event.refuel_area",
                "x_max": 1261,
                "x_min": 701,
                "y_max": 718,
                "y_min": 399
            },
            {
                "channel_id": 5,
                "mask_id": 13,
                "name": "refuel_event.refuel_area",
                "x_max": 893,
                "x_min": 589,
                "y_max": 169,
                "y_min": 57
            },
            {
                "channel_id": 6,
                "mask_id": 14,
                "name": "refuel_event.refuel_area",
                "x_max": 529,
                "x_min": 279,
                "y_max": 375,
                "y_min": 224
            },
            {
                "channel_id": 6,
                "mask_id": 15,
                "name": "refuel_event.refuel_area",
                "x_max": 492,
                "x_min": 102,
                "y_max": 720,
                "y_min": 389
            },
            {
                "channel_id": 6,
                "mask_id": 16,
                "name": "refuel_event.refuel_area",
                "x_max": 998,
                "x_min": 703,
                "y_max": 367,
                "y_min": 201
            },
            {
                "channel_id": 6,
                "mask_id": 17,
                "name": "refuel_event.refuel_area",
                "x_max": 1201,
                "x_min": 742,
                "y_max": 712,
                "y_min": 373
            },
            {
                "channel_id": 7,
                "mask_id": 18,
                "name": "refuel_event.refuel_area",
                "x_max": 388,
                "x_min": 158,
                "y_max": 186,
                "y_min": 93
            },
            {
                "channel_id": 7,
                "mask_id": 19,
                "name": "refuel_event.refuel_area",
                "x_max": 846,
                "x_min": 540,
                "y_max": 172,
                "y_min": 61
            },
            {
                "channel_id": 7,
                "mask_id": 20,
                "name": "refuel_event.refuel_area",
                "x_max": 1008,
                "x_min": 622,
                "y_max": 381,
                "y_min": 176
            },
            {
                "channel_id": 7,
                "mask_id": 21,
                "name": "refuel_event.refuel_area",
                "x_max": 1271,
                "x_min": 696,
                "y_max": 712,
                "y_min": 401
            },
            {
                "channel_id": 7,
                "mask_id": 22,
                "name": "refuel_event.refuel_area",
                "x_max": 304,
                "x_min": 11,
                "y_max": 720,
                "y_min": 442
            },
            {
                "channel_id": 7,
                "mask_id": 23,
                "name": "refuel_event.refuel_area",
                "x_max": 393,
                "x_min": 47,
                "y_max": 446,
                "y_min": 191
            },
            {
                "channel_id": 8,
                "mask_id": 24,
                "name": "refuel_event.refuel_area",
                "x_max": 578,
                "x_min": 420,
                "y_max": 325,
                "y_min": 240
            },
            {
                "channel_id": 8,
                "mask_id": 25,
                "name": "refuel_event.refuel_area",
                "x_max": 885,
                "x_min": 668,
                "y_max": 306,
                "y_min": 219
            }
        ]
    }

    ```
    @@@
    """
    mask_id = request.args.get('mask_id', None, type=int)
    channel_id = request.args.get('channel_id', None, type=int)
    name = request.args.get('name', None, type=str)
    page = request.args.get('page', 1, type=int)
    per_page = request.args.get('per_page', RECORDS_PER_PAGE, type=int)
    per_page = min(per_page, RECORDS_MAX_PER_PAGE)
    custom_query = Mask.query
    if mask_id is not None:
        custom_query = custom_query.filter(Mask.id == mask_id)
    if channel_id is not None:
        custom_query = custom_query.filter(Mask.channel_id == channel_id)
    if name is not None:
        custom_query = custom_query.filter(Mask.name.like('%' + name + '%'))
    custom_query = custom_query.order_by(Mask.id.asc())
    payload = Mask.to_collection_dict(custom_query, page, per_page, 'config_api.list_masks',
        mask_id=mask_id, channel_id=channel_id, name=name)  # NOQA
    return jsonify(payload)


@bp.route('/mask/add', methods=['POST'])
@token_auth.login_required
@write_required()
def add_mask():
    """新增检测区域

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 config_api 模块有 W 权限。

    ## 表单字段

    | 字段       | 是否必须 | 类型 | 说明                               |
    | ---------- | -------- | ---- | ---------------------------------- |
    | channel_id | 是       | int  | 参考 `/config/api/scheduler` 接口  |
    | name       | 是       | str  | mask 的键名，父子键之间用 `.` 分隔 |
    | x_min      |          | int  | 默认：-1                           |
    | y_min      |          | int  | 默认：-1                           |
    | x_max      |          | int  | 默认：-1                           |
    | y_max      |          | int  | 默认：-1                           |

    ## 响应示例

    ```json
    {
        "channel_id": 9,
        "mask_id": 30,
        "name": "aaa.bbb",
        "x_max": 44,
        "x_min": 22,
        "y_max": 55,
        "y_min": 33
    }

    ```
    @@@
    """
    form_data = request.form.to_dict()
    mask = Mask.from_dict(form_data)
    if mask is None:
        return bad_request(_l('missing required field!'))
    if str(mask.channel_id) not in [str(c.id) for c in Scheduler.query.all()]:
        return bad_request(_l('%(obj)s is invalid!', obj=mask.channel_id))
    if mask.x_min - mask.x_max:
        return bad_request(_l('Invalid x_min or x_max!'))
    if mask.y_min - mask.y_max:
        return bad_request(_l('Invalid y_min or y_max!'))
    db.session.add(mask)
    db.session.commit()
    return jsonify(mask.to_dict())


@bp.route('/mask/<int:mask_id>', methods=['PATCH'])
@token_auth.login_required
@write_required()
def update_mask(mask_id):
    """更新检测区域

    @@@

    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限


    - 访问用户对 config_api 模块有 W 权限。

    ## 表单字段

    | 字段  | 是否必须 | 类型 | 说明     |
    | ----- | -------- | ---- | -------- |
    | x_min |          | int  | 默认：-1 |
    | y_min |          | int  | 默认：-1 |
    | x_max |          | int  | 默认：-1 |
    | y_max |          | int  | 默认：-1 |

    ## 响应示例

    ```json
    {
        "channel_id": 9,
        "mask_id": 30,
        "name": "aaa.bbb",
        "x_max": 33,
        "x_min": 11,
        "y_max": 44,
        "y_min": 22
    }

    ```
    @@@
    """
    form_data = request.form.to_dict()
    res = Mask.query.filter(Mask.id == mask_id).first()
    if res is None:
        return not_found(_l('%(obj)s not found!', obj=mask_id))
    res = Mask.from_dict(form_data, res)
    db.session.commit()
    return jsonify(res.to_dict())


@bp.route('/mask/<int:mask_id>', methods=['DELETE'])
@token_auth.login_required
@write_required()
def delete_mask(mask_id):
    """删除检测区域

    @@@
    ## 请求头

    - 携带 `Authorization` 字段。

    ## 权限

    - 访问用户对 config_api 模块有 W 权限。

    ## 响应示例

    ```json
    {
        "channel_id": 9,
        "mask_id": 30,
        "name": "aaa.bbb",
        "x_max": 33,
        "x_min": 11,
        "y_max": 44,
        "y_min": 22
    }
    ```

    @@@
    """
    res = Mask.query.filter(Mask.id == mask_id).first()
    if res is None:
        return not_found(_l('%(obj)s not found!', obj=mask_id))
    db.session.delete(res)
    db.session.commit()
    return jsonify(res.to_dict())
