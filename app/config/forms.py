# -*- coding: utf-8 -*-

import os.path as osp

from flask_babel import lazy_gettext as _l
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, SelectField, IntegerField
from wtforms.validators import DataRequired, ValidationError, NumberRange, IPAddress

# from . import CAMERA_SCAN_CONFIG
from ..beans import Category, Camera, Scheduler, SERVICES
from ..utils import check_rtsp, read_json, RTSP_PORT


class CameraAddForm(FlaskForm):
    """新增摄像头表单"""
    url = StringField(_l('CameraUrl'), validators=[DataRequired()],
        render_kw={'placeholder': _l('RTSP url or File')})  # NOQA
    submit = SubmitField(_l('Add'))

    def __init__(self, *args, **kwargs):
        super(CameraAddForm, self).__init__(*args, **kwargs)

    def validate_url(self, url):
        if not (check_rtsp(url.data) or osp.isfile(url.data)):
            raise ValidationError(_l('Invalid url!'))


class CameraScanForm(FlaskForm):
    """扫描摄像头表单"""    
    ip_address = StringField(_l('IP Address'), validators=[DataRequired()], render_kw={'placeholder': _l('Required')})
    user = StringField(_l('Username'), validators=[], render_kw={'placeholder': _l('Optional')})
    pwd = StringField(_l('Password'), validators=[], render_kw={'placeholder': _l('Optional')})
    port = IntegerField(_l('Port'), validators=[NumberRange(min=0, max=65535)])
    submit = SubmitField(_l('Scan'))

    def __init__(self, *args, **kwargs):
        super(CameraScanForm, self).__init__(*args, **kwargs)
        # payload = read_json(CAMERA_SCAN_CONFIG)
        # self.user.choices = [(u, u) for u in payload.get('user_choices')]
        # self.pwd.choices = [(p, p) for p in payload.get('pwd_choices')]
        if not self.is_submitted():
            self.port.data = RTSP_PORT


class SchedulerEditForm(FlaskForm):
    """新增调度程序表单"""    
    name = StringField(_l('Name'), validators=[DataRequired()], render_kw={'placeholder': 'channel_name'})
    category_name = SelectField(_l('Category'), validators=[DataRequired()])
    url = SelectField(_l('Url'), validators=[DataRequired()])
    start = SelectField(_l('Startup'), validators=[DataRequired()])
    batch = IntegerField(_l('Batch'), validators=[NumberRange(min=0)])
    height = IntegerField(_l('Height'), validators=[NumberRange(min=0)])
    width = IntegerField(_l('Width'), validators=[NumberRange(min=0)])
    buffer = IntegerField(_l('Buffer'), validators=[NumberRange(min=0)])
    submit = SubmitField(_l('Save'))

    def __init__(self, original=None, *args, **kwargs):
        super(SchedulerEditForm, self).__init__(*args, **kwargs)
        self.category_name.choices = [(c.name, c.name) for c in Category.query.all()]
        self.url.choices = [(c.url, c.url) for c in Camera.query.all()]
        self.start.choices = [(c, c) for c in ['0', '1']]
        if not self.is_submitted():
            if original:
                self.name.data = original.name
                self.category_name.data = original.category_name
                self.start.data = original.start
                self.batch.data = original.batch
                self.url.data = original.url
                self.height.data = original.height
                self.width.data = original.width
                self.buffer.data = original.buffer
            else:
                self.start.data = 0
                self.batch.data = 4
                self.height.data = 720
                self.width.data = 1280
                self.buffer.data = 30


class ServiceEditForm(FlaskForm):
    """新增模型服务表单"""    
    channel_id = SelectField(_l('ChannelId'), validators=[DataRequired()])
    name = SelectField(_l('Name'), validators=[DataRequired()])
    ip = StringField(_l('IP'), validators=[IPAddress()])
    port = IntegerField(_l('Port'), validators=[NumberRange(min=-1)])
    submit = SubmitField(_l('Save'))

    def __init__(self, original=None, *args, **kwargs):
        super(ServiceEditForm, self).__init__(*args, **kwargs)
        self.channel_id.choices = [(str(c.id), str(c.name)) for c in Scheduler.query.all()]
        self.name.choices = [(c, c) for c in list(SERVICES.keys())]
        if not self.is_submitted():
            if original:
                self.name.data = original.name
                self.ip.data = original.ip
                self.port.data = original.port
            else:
                self.name.data = 'detect'
                self.ip.data = '0.0.0.0'
                self.port.data = -1

    def validate_name(self, name):
        if self.name.data not in list(SERVICES.keys()):
            raise ValidationError(_l('Invalid name!'))


class MaskEditForm(FlaskForm):
    """新增检测区域表单"""    
    channel_id = SelectField(_l('ChannelId'), validators=[DataRequired()])
    name = StringField(_l('name'), validators=[DataRequired()])
    x_min = IntegerField(_l('x_min'), validators=[NumberRange(min=0)])
    y_min = IntegerField(_l('y_min'), validators=[NumberRange(min=0)])
    x_max = IntegerField(_l('x_max'), validators=[NumberRange(min=0)])
    y_max = IntegerField(_l('y_max'), validators=[NumberRange(min=0)])
    submit = SubmitField(_l('Save'))

    def __init__(self, original=None, *args, **kwargs):
        super(MaskEditForm, self).__init__(*args, **kwargs)
        self.channel_id.choices = [(str(c.id), str(c.name)) for c in Scheduler.query.all()]
        if not self.is_submitted():
            if original:
                self.channel_id.data = original.channel_id
                self.name.data = original.name
                self.x_min.data = original.x_min
                self.y_min.data = original.y_min
                self.x_max.data = original.x_max
                self.y_max.data = original.y_max
            else:
                self.x_min.data = 0
                self.y_min.data = 0
                self.x_max.data = 0
                self.y_max.data = 0

    def validate_x_max(self, x_max):
        if self.x_min.data > self.x_max.data:
            raise ValidationError(_l('Invalid x_min or x_max!'))

    def validate_y_max(self, y_max):
        if self.y_min.data > self.y_max.data:
            raise ValidationError(_l('Invalid y_min or y_max!'))
