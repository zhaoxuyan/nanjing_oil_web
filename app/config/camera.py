# -*- coding: utf-8 -*-

from flask import request, render_template, flash, redirect, url_for
from flask_babel import lazy_gettext as _l
from flask_login import login_required

from .. import read_required, RECORDS_PER_PAGE, write_required, db
from ..beans import Camera
from . import bp
from .forms import CameraAddForm


@bp.route('/camera', methods=['GET'])
@login_required
@read_required()
def list_cameras():
    """摄像头列表"""
    camera_id = request.args.get('camera_id', None, type=int)
    url = request.args.get('url', None, type=str)
    page = request.args.get('page', 1, type=int)
    custom_query = Camera.query
    if camera_id is not None:
        custom_query = custom_query.filter(Camera.id == camera_id)
    if url is not None:
        custom_query = custom_query.filter(Camera.url.like('%' + url + '%'))
    custom_query = custom_query.order_by(Camera.id.asc())
    return render_template('config/camera_list.jinja2', title=_l('Camera List'),
        res=custom_query.paginate(page, RECORDS_PER_PAGE, False),
        cameras=Camera.query.all(),
        keywords={
            "camera_id": camera_id if camera_id else '',
            "url": url if url else ''
        })  # NOQA


@bp.route('/camera/add', methods=['GET', 'POST'])
@login_required
@write_required()
def add_camera():
    """新增摄像头"""
    form = CameraAddForm()
    if form.validate_on_submit():
        camera_url = form.url.data
        if Camera.query.filter(Camera.url == camera_url).count() > 0:
            flash(_l('%(obj)s already exists!', obj=camera_url))
            return redirect(url_for('config.list_cameras'))
        camera = Camera.from_dict(form.data)
        db.session.add(camera)
        db.session.commit()
        flash(_l('%(obj)s has been added.', obj=camera_url))
        return redirect(url_for('config.list_cameras'))
    camera_url = request.args.get('camera_url', None, type=str)
    if camera_url:
        form.url.data = camera_url
    return render_template('edit.jinja2', title=_l('Add Camera'),
        form=form)  # NOQA


@bp.route('/camera/delete/<int:camera_id>', methods=['GET'])
@login_required
@write_required()
def delete_camera(camera_id):
    """删除摄像头"""
    res = Camera.query.filter(Camera.id == camera_id).first()
    if res is None:
        flash(_l('%(obj)s not found!', obj=camera_id))
        return redirect(url_for('config.list_cameras'))
    if res.schedulers.count() > 0:
        flash(_l('%(obj)s in used!', obj=camera_id))
        return redirect(url_for('config.list_cameras'))
    db.session.delete(res)
    db.session.commit()
    flash(_l('%(obj)s has been deleted.', obj=camera_id))
    return redirect(url_for('config.list_cameras'))
